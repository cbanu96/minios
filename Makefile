CC := i686-elf-gcc
UCC := i686-minios-gcc
HOSTCC := gcc
AS := i686-elf-as
QEMU := qemu-system-i386 

KCFLAGS := -Wall -Wextra
UCFLAGS := -s -O3

SRCDIR := src
OBJDIR := obj
BINDIR := bin

KSRCDIR := src/kernel
KOBJDIR := obj/kernel
KBINDIR := bin/kernel

KINCLUDE := -Isrc -Isrc/include

USRCDIR := src/userspace
UOBJDIR := obj/userspace
UBINDIR := bin/userspace

C_KSRCFILES := $(shell find $(KSRCDIR) -type f -name \*.c)
C_KOBJFILES := $(patsubst $(KSRCDIR)/%.c,$(KOBJDIR)/%.o,$(C_KSRCFILES))
S_KSRCFILES := $(shell find $(KSRCDIR) -type f -name \*.S)
S_KOBJFILES := $(patsubst $(KSRCDIR)/%.S,$(KOBJDIR)/%.o,$(S_KSRCFILES))

UBINFILES := $(patsubst $(USRCDIR)/%.c, $(UBINDIR)/%, $(shell find $(USRCDIR) -type f -name \*.c))

KOBJFILES := $(C_KOBJFILES) $(S_KOBJFILES)

all: clean run-qemu-iso

clean:
	rm -rf isodir/boot/initrd.bin
	rm -rf initrd/bin/*
	rm -rf $(OBJDIR)
	rm -rf $(BINDIR)
	mkdir -p $(BINDIR)

kernel: $(KOBJFILES)
	mkdir -p $(KBINDIR)
	$(CC) -T linker.ld -o $(KBINDIR)/os.bin -ffreestanding -O2 -nostdlib $(KOBJFILES) -lgcc

mk-img: isodir/boot/initrd.bin kernel
	cp $(KBINDIR)/os.bin isodir/boot/os.bin
	genisoimage -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 \
		-boot-info-table -o os.iso isodir

isodir/boot/initrd.bin: initrd $(UBINFILES)
	mkdir -p initrd/bin
	cp -ar $(UBINDIR)/* initrd/bin
	$(BINDIR)/mkinitrd isodir/boot/initrd.bin initrd

$(UBINDIR)/%: $(USRCDIR)/%.c
	mkdir -p $(UBINDIR)
	$(UCC) $(UCFLAGS) $< -o $@

initrd: $(BINDIR)/mkinitrd

$(BINDIR)/mkinitrd: mkinitrd/mkinitrd.c
	$(HOSTCC) $(KCFLAGS) -o $(BINDIR)/mkinitrd mkinitrd/mkinitrd.c

run-qemu-iso: mk-img 
	$(QEMU) -cdrom os.iso

$(KOBJDIR)/%.o: $(KSRCDIR)/%.c
	mkdir -p $(shell dirname $@)
	$(CC) $(KCFLAGS) -c $< -o $@ -ffreestanding -O2 -nostdlib -lgcc -I- $(KINCLUDE)

$(KSRCDIR)/%.S: $(KSRCDIR)/%.h

$(KSRCDIR)/%.s: $(KSRCDIR)/%.S
	$(CC) -E -I- $(KINCLUDE) $< > $@

$(KOBJDIR)/%.o: $(KSRCDIR)/%.s
	mkdir -p $(shell dirname $@)
	$(AS) $< -o $@

.PHONY: clean mk-img

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <stdint.h>

#define INITRD_MAX_ENTRIES 256
#define INITRD_MAX_DATA    1048576
#define INITRD_MAGIC       0xDEADBEEF

struct initrd_header {
	uint32_t magic;
	uint32_t entries;
	uint32_t data_begin;
};

struct initrd_entry {
	uint32_t  flag;
	char      path[64];
	uint32_t data_offset;
	uint32_t data_length;
};

struct initrd_entry entries[INITRD_MAX_ENTRIES];
int entry_ptr = 0;
int initrd_data_offset = 0;
char initrd_data_buffer[INITRD_MAX_DATA];

void print_usage() {
    printf(
        "Usage: ./mkinitrd <output> <directory>\n"
    );
}

void add_initrd_entry(int flag, const char * path, int offset, int length) {
    if (entry_ptr == INITRD_MAX_ENTRIES) {
        fprintf(stderr, "Max number of entries reached.\n");
        exit(1);
    }
    entries[entry_ptr].flag = flag;
    strcpy(entries[entry_ptr].path, path);
    entries[entry_ptr].data_offset = offset;
    entries[entry_ptr].data_length = length;
    entry_ptr++;
}

void file_read(const char * filename, const char * virt_path) {
    printf("FIL %s\n", filename);

    int offset = initrd_data_offset;
    int fd = open(filename, O_RDONLY);
    if (fd < 0) {
        perror("open");
        exit(1);
    }

    char tmpbuf[INITRD_MAX_DATA];

    int n = read(fd, tmpbuf, INITRD_MAX_DATA);

    if (close(fd) < 0) {
        perror("close");
        exit(1);
    }

    if (initrd_data_offset + n > INITRD_MAX_DATA) {
        fprintf(stderr, "Data too large, exceeding the limit of %d bytes.\n",
            INITRD_MAX_DATA);
        exit(1);
    }

    add_initrd_entry(S_IFREG, virt_path, offset, n);
    memcpy(initrd_data_buffer + initrd_data_offset, tmpbuf, n);
    initrd_data_offset += n;
}

void recursive_read(const char * filename, const char * virt_path) {
    printf("DIR %s\n", filename);

    struct dirent * ent;
    DIR * dir = opendir(filename);

    if (dir == NULL) {
        perror("opendir");
        exit(1);
    }

    while ((ent = readdir(dir))) {
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;

        char * next_fn = (char *) malloc(sizeof(char) * 256);
        strcpy(next_fn, filename);
        strcat(next_fn, "/");
        strcat(next_fn, ent->d_name);

        char * next_vn = (char *) malloc(sizeof(char) * 64);
        strcpy(next_vn, virt_path);
        strcat(next_vn, "/");
        strcat(next_vn, ent->d_name);

        if (ent->d_type == DT_DIR) {
            add_initrd_entry(S_IFDIR, next_vn, 0, 0);
            recursive_read(next_fn, next_vn);
        }
        if (ent->d_type == DT_REG) {
            file_read(next_fn, next_vn);
        }

        free(next_fn);
        free(next_vn);
    }

    if (closedir(dir) == -1) {
        perror("closedir");
        exit(0);
    }
}

int do_read(const char * filename) {
    recursive_read(filename, "");

    return 0;
}

int output(const char * filename) {
    int fd = open(filename, O_CREAT | O_WRONLY, S_IFREG | 0664);
    if (fd < 0) {
        perror("open");
        exit(1);
    }

    struct initrd_header initrd_hdr;

    initrd_hdr.magic = INITRD_MAGIC;
    initrd_hdr.entries = entry_ptr;
    initrd_hdr.data_begin  = entry_ptr * sizeof(struct initrd_entry) + sizeof(initrd_hdr);

    int written;

    written = write(fd, &initrd_hdr, sizeof(struct initrd_header));
    if (written < 0) {
        perror("write");
        exit(1);
    }

    written = write(fd, &entries, sizeof(struct initrd_entry) * entry_ptr);
    if (written < 0) {
        perror("write");
        exit(1);
    }

    written = write(fd, &initrd_data_buffer, initrd_data_offset);

    if (close(fd) < 0) {
        perror("close");
        exit(1);
    }

    return 0;
}

int main (int argc, char ** argv) {
    if (argc != 3) {
        print_usage();
        exit(1);
    }

    if (do_read(argv[2]) < 0) {
        exit(1);
    }

    if (output(argv[1]) < 0) {
        exit(1);
    }


    exit(0);
}

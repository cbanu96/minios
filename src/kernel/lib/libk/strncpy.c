#include <sys/types.h>

char * strncpy(char * dest, const char * src, size_t num) {
    char * saved = dest;

    while (num--) {
        *dest++ = *src++;
    }

    return saved;
}

#include <sys/types.h>

void * memcpy(void * dest, const void * src, size_t len) {
    void * save = dest;
    char * destc = (char *) dest;
    char * srcc = (char *) src;

    while(len--) {
        *destc++ = *srcc++;
    }

    return save;
}

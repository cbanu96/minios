char * strcpy(char * dest, const char * src) {
    char * saved = dest;

    while ((*dest++ = *src++));

    return saved;
}

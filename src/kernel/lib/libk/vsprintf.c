#include <sys/types.h>
#include <stdarg.h>

extern char * itoa(unsigned int value, char *str, int base);

/* my own implementation of vsnprintf,
 * don't need much, hex, decimal and string printing.
 * will add features later if anything else is required. */
static size_t vsnprintf_int(char *s, size_t n, const char *fmt, va_list ap) {

    bool stop_n = true;
    uint8_t read_percent = false;
    char buffer[20];
    char *strptr;
    size_t printed = 0;

    if (n == 0) stop_n = false; /* Don't mind n */

    n--; /* have to account for null-terminating the string  */
    while (*fmt != '\0' && ((!stop_n) || (stop_n && n))) {
        if (read_percent) {
            read_percent = false;

            switch (*fmt) {
                case '%': /* %% */
                    *s++ = '%';
                    n--;
                    break;
                case 'd':
                    strptr = itoa(va_arg(ap, unsigned int), buffer, 10);
                    goto print_str;
                    break;
                case 'x':
                    strptr = itoa(va_arg(ap, unsigned int), buffer, 16);
                    goto print_str;
                    break;
                case 's':
                    strptr = va_arg(ap, char*);
                    goto print_str;
                    break;
                print_str:
                    while (*strptr != '\0' && ((!stop_n) || (stop_n && n)))
                        *s++ = *strptr++, n--, printed++;
                    break;
            }
        } else {
            if (*fmt == '%')
                read_percent = true;
            else
                *s++ = *fmt, n--, printed++;
        }

        fmt++;
    }

    *s = '\0';

    return printed;
}

size_t vsnprintf(char *s, size_t n, const char *fmt, va_list ap) {
    if (n == 0)
        return 0;

    return vsnprintf_int(s, n, fmt, ap);
}

size_t vsprintf(char *s, const char *fmt, va_list ap) {
    return vsnprintf_int(s, 0, fmt, ap);
}

size_t sprintf(char *s, const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    size_t printed = vsnprintf_int(s, 0, fmt, ap);
    va_end(ap);

    return printed;
}

size_t snprintf(char *s, size_t n, const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    size_t printed = vsnprintf_int(s, n, fmt, ap);
    va_end(ap);

    return printed;
}

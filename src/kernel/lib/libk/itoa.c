#include <sys/types.h>

char * itoa(unsigned int value, char *str, int base) {
    if (base > 36)
        return str;
    const char *digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    char *begin, *end, *save, tmp;
    char digit;
    begin = end = save = str;

    begin = end;

    if (!value) {
        *end++ = digits[0];
    }

    /* Put the digits into the buffer */
    while (value) {
        digit = value % base;
        value /= base;

        *end++ = digits[(int)digit];
    }

    *end = '\0';
    end--;

    /* Reverse the digits' order */
    while (begin < end) {
        tmp = *begin;
        *begin = *end;
        *end = tmp;

        begin++, end--;
    }


    return str;
}

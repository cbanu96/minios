#include <sys/types.h>

int strncmp(const char *str1, const char *str2, size_t n) {
    while ((*str1 || *str2) && n) {
        if (*str1 != *str2) {
            return (*str1 - *str2) > 0 ? 1 : -1;
        }

        str1++, str2++;
        n--;
    }

    return 0;
}

int strcmp(const char *str1, const char *str2) {
    while (*str1 || *str2) {
        if (*str1 != *str2) {
            return (*str1 - *str2) > 0 ? 1 : -1;
        }

        str1++, str2++;
    }

    return 0;
}

#include <sys/types.h>

void * memset(void * ptr, int value, size_t num) {
    /* Rudimentary version; will implement duff's device later
     * if the performance difference is noticeable */

    void * saved = ptr;

    while (num--) {
        *(uint8_t *)ptr++ = value;
    }

    return saved;
}

#include <sys/types.h>

extern char * strcpy(char *, const char *);
extern size_t strlen(const char *);

char * strcat(char * dest, const char * src) {
    strcpy(dest + strlen(dest), src);

    return dest;
}

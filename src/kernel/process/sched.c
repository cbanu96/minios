#include <minios/kernel.h>
#include <minios/mm.h>
#include <minios/process.h>
#include <minios/fs/vfs.h>
#include <minios/x86/dt.h>
#include <minios/x86/pit.h>
#include <minios/log.h>
#include <minios/assert.h>

#include <ds/list.h>

#include <sys/types.h>
#include <sys/io.h>
#include <string.h>

extern struct process * current_process;
extern struct process * init;
extern struct process * idle;
extern list_t *         process_queue;
extern list_t *         process_over;

static void _spawn_idle();
static void _spawn_init();

addr_t                  kernel_cr3;

void schedule(struct interrupt_registers * registers, bool reschedule);

void timer_tick_handler(struct interrupt_registers * registers) {
    static int ticks = 0;
    if (ticks++ == 10)
    {
        ticks = 0;
        schedule(registers, true);
    }

    /* Send EOI */
    irq_send_eoi(IRQ(0));
}

void schedinit() {
    cli();

    /* Save the kernel cr3, we're going to need it later */
    kernel_cr3 = read_cr3();

    process_queue = list_create();
    process_over  = list_create();

    /* Initialize process table */
    ptable_init();

    /* Initialize idle and init processes */
    _spawn_init();

    _spawn_idle();

    /* Initialize PIT, set IRQ0 handler */
    pit_init(100); /* 100 Hz frequency = 0.01 timeslice */
    interrupt_set_handler(IRQ(0), &timer_tick_handler);

    /* Start multitasking by process_start()ing init */

    /* Init is not pushed initially in the process queue because init is the first process to run, and it will be pushed
     * when its timeslice is over */
    process_start(init);
}

static void __idle(void) {
    /* Infinite loop, what did you expect? */
    for (;;);
}

static void _spawn_idle() {
    idle = process_create(NULL);

    idle->p_regs.eip = (addr_t) &__idle;

    /* Kernel space process, the only one */
    idle->p_regs.cs  = 0x08;
    idle->p_regs.ss  = 0x10;
    idle->p_regs.ds  = 0x10;
    idle->p_regs.es  = 0x10;
    idle->p_regs.fs  = 0x10;
    idle->p_regs.gs  = 0x10;

    ptable_insert(idle->p_pid, idle);
}

/* Spawns init in CPL0 */
static void _spawn_init() {
    init = process_create(NULL);

    init->p_regs.cs = 0x1B;
    init->p_regs.ss = 0x23;
    init->p_regs.ds = 0x23;
    init->p_regs.es = 0x23;
    init->p_regs.fs = 0x23;
    init->p_regs.gs = 0x23;

    /* Everything else will be settled by process_loading /bin/init. */

    /* Now, load "/bin/init" into init */
    char * argv[] = {
        "/bin/init",
        NULL
    };

    char * envp[] = {
        NULL
    };

    int err = process_load(init, "/bin/init", argv, 1, envp);
    if (err < 0)
        PANIC("Error while loading /bin/init: %d\n", -err);

    ptable_insert(init->p_pid, init);
}

static void _do_process_cleanup() {
    while (process_over->length) {
        struct process * p = (struct process *) list_pop_tail(process_over);

        /* If this process is over and has children,
         * clearly it is not waitpid()ing any of them.
         * So we can safely assume that the children will
         * be reparented to the init process without any
         * complications.
         */
        list_node_t * it;
        list_foreach(it, p->p_children) {
            struct process * child = (struct process *)(it->value);

            if (process_reparent(child)) {
                /* p is corrupt!!! */
                PANIC("p 0x%x or child 0x% corrupt, couldn't reparent child!\n", p, child);
            }
        }

        process_destroy(p);
    }
}

void schedule(struct interrupt_registers * registers, bool reschedule) {
    /* Everything we do in the scheduler must be done mainly in the kernel page directory.
     * Only process image manipulation will be done in the respective process's cr3.
     */
    write_cr3(kernel_cr3);

    if (!current_process) /* wait for process_start() */
        return;

    _do_process_cleanup();

    if (current_process->p_state == PROC_STATE_ACTIVE && reschedule) {
        current_process->p_state = PROC_STATE_READY;
        /* If it wasn't READY, it might be OVER, so don't push it on the process queue */
        process_push(current_process);
    }

    struct process * next = process_pop();

    /* Copy registers into kernel space */
    memcpy(&current_process->p_regs, registers, sizeof(struct interrupt_registers));

    /* Copy next process's registers onto the stack */
    memcpy(registers, &next->p_regs, sizeof(struct interrupt_registers));

    /* Update TSS */
    set_tss_esp(next->p_kesp);

    /* Update CR3 */
    write_cr3(next->p_cr3);

    /* Set current process */
    current_process = next;
    current_process->p_state = PROC_STATE_ACTIVE;

    /* IRET */
    return;
}

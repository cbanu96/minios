#include <minios/kernel.h>
#include <minios/mm.h>
#include <minios/process.h>
#include <minios/spinlock.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/assert.h>

#include <ds/list.h>

#include <sys/types.h>
#include <string.h>

struct ptable ptable;

/* Process Table Lock */
spinlock_t ptable_lock;

/* Initialize the process table. */
void ptable_init() {
    ptable.entries = (struct ptable_entry *) kmalloc(sizeof(struct ptable_entry) * PROC_INIT_MAX);
    if (!ptable.entries) {
        PANIC("Couldn't allocate memory for the process table.\n");
        return;
    }

    ptable.length = PROC_INIT_MAX;
    ptable.used = 0;
}

/* Insert a (pid, process) pair into the table. */
void ptable_insert(pid_t pid, struct process * p) {
    ACQUIRE_LOCK(ptable_lock);

    struct ptable_entry entry;
    entry.e_pid = pid;
    entry.e_proc = p;

    if (ptable.length == ptable.used) {
        uint32_t new_length = ptable.length * 2;
        struct ptable_entry * new_entries = (struct ptable_entry *) kmalloc(sizeof(struct ptable_entry) * new_length);

        memcpy(new_entries, ptable.entries, sizeof(struct ptable_entry) * ptable.length);
        kfree(ptable.entries);

        ptable.entries = new_entries;
        ptable.length = new_length;
    }

    ptable.entries[ptable.used++] = entry;

    RELEASE_LOCK(ptable_lock);
}

/* Delete from the process table using the pid as a key. */
void ptable_delete(pid_t pid) {
    ACQUIRE_LOCK(ptable_lock);
    size_t i;

    for (i = 0; i < ptable.used; i++) {
        if (ptable.entries[i].e_pid == pid) {
            RELEASE_LOCK(ptable_lock);
            ptable.entries[i] = ptable.entries[--ptable.used];
            return;
        }
    }

    RELEASE_LOCK(ptable_lock);
}

/* Find process by pid. */
struct process * ptable_find(pid_t pid) {
    ACQUIRE_LOCK(ptable_lock);
    
    size_t i;

    for (i = 0; i < ptable.used; i++) {
        if (ptable.entries[i].e_pid == pid) {
            RELEASE_LOCK(ptable_lock);
            return ptable.entries[i].e_proc;
        }
    }

    RELEASE_LOCK(ptable_lock);
    return NULL;
}

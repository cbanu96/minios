#include <minios/kernel.h>
#include <minios/mm.h>
#include <minios/process.h>
#include <minios/spinlock.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/assert.h>

#include <ds/list.h>

#include <sys/types.h>
#include <sys/io.h>
#include <string.h>

/* Current / Idle / Init Process */
struct process * current_process;
struct process * init;
struct process * idle;

/* VFS Root */
extern struct vnode * vroot;

/* Process Queue */
list_t * process_queue;

list_t * process_over;

list_t * process_blocked;

spinlock_t process_queue_lock;

/* Process Tree */
spinlock_t process_tree_lock;

extern addr_t kernel_cr3;

/* Any required forward declarations */
extern void phys_mem_push(uint32_t addr);

/* Returns the next unused PID, in a linear fashion, not reusing IDs at all. */
static pid_t _get_next_pid() {
    static pid_t last_pid = 0;
    return ++last_pid;
}

/* Helper macro, automatically returning -EBADF if fd is not in range */
#define ASSERT_FD(fd, ret) \
    do { if ((fd) < 0 || (fd) > PROC_MAX_FDS) \
        return (ret); } while (0)

/* Returns the first usable file descriptor in the given process */
int get_first_usable_fd(struct process * p) {
    size_t i;
    for (i = 0; i < PROC_MAX_FDS; i++) {
        if (p->p_fds[i] == NULL)
            return i;
    }

    return -1;
}

/* Given a vnode, opens a file that points to that vnode and returns the fd */
int process_fd_open(struct process * p, struct vnode * vn, int oflags) {
    int fd = get_first_usable_fd(p);

    if (fd == -1) {
        return -EMFILE;
    }

    p->p_fds[fd] = (struct file *) kmalloc(sizeof(struct file));

    int err = vfop_open(vn, p->p_fds[fd], oflags);
    if (err) {
        kfree(p->p_fds[fd]);
        p->p_fds[fd] = NULL;
        return err;
    }

    return fd;
}

/* Makes fd[newfd] point to the same file * as fd[oldfd] */
int process_fd_dup(struct process * p, int oldfd, int newfd) {
    ASSERT_FD(oldfd, -EBADF);
    if (newfd == -1) {
        newfd = get_first_usable_fd(p);
        if (newfd == -1)
            return -EMFILE;
    }
    else {
        if (newfd < 0 || newfd > PROC_MAX_FDS)
            return -EBADF;
    }


    p->p_fds[newfd] = p->p_fds[oldfd];

    return newfd;
}

/* Close a specific file descriptor, deallocating the file ptr */
int process_fd_close(struct process * p, int fd) {
    ASSERT_FD(fd, -EBADF);

    if (p->p_fds[fd] == NULL) {
        return -EBADF;
    }

    int err = vfop_close(p->p_fds[fd]->f_vn, p->p_fds[fd]);
    if (err)
        return err;

    kfree(p->p_fds[fd]);
    p->p_fds[fd] = NULL;

    return 0;
}

/* Returns a pointer to the underlying vnode structure, given a process and a valid file descriptor */
struct vnode * process_fd_node(struct process * p, int fd) {
    ASSERT_FD(fd, NULL);
    return p->p_fds[fd]->f_vn;
}

/* Returns a pointer to the underlying file structure, given a process and a valid file descriptor */
struct file * process_fd_file(struct process * p, int fd) {
    ASSERT_FD(fd, NULL);
    return p->p_fds[fd];
}

/* Closes any open file descriptors on a specified process */
static void _close_open_fds(struct process * p) {
    size_t i;
    for (i = 0; i < PROC_MAX_FDS; i++) {
        if (p->p_fds[i] != NULL) {
            process_fd_close(p, i);
        }
    }
}

/* Clones any open file descriptors from the given process. */
static void _clone_open_fds(struct process * from, struct process * to) {
    size_t i;
    for (i = 0; i < PROC_MAX_FDS; i++) {
        if (from->p_fds[i] != NULL) {
            process_fd_open(to, from->p_fds[i]->f_vn, from->p_fds[i]->f_oflags);
        }
    }
}

/* Fractal-mapped and kernel-mapped page directory into 0FFBFF000 */
static void _update_kernel_pd(addr_t cr3) {
    uint32_t * cloned_pd = (uint32_t *)(0xFFBFF000);
    uint32_t * pd = (uint32_t *)(PDE_ADDRESS);
    int i;
    for (i = 768; i < 1022; i++) {
        cloned_pd[i] = pd[i];
    }

    cloned_pd[1022] = 0; /* unmap the new page directory from the cloned one */
    cloned_pd[1023] = cr3 | 0x3; /* fractal map again */

}

/* Updates the kernel pages, making sure that everything that got allocated in the kernel_cr3 is available in p's cr3 as well. */
void process_update_kernel_pages(struct process * p) {
    mm_alloc_page_kernel_phys(0xFFBFF000, p->p_cr3);
    _update_kernel_pd(p->p_cr3);
    mm_free_page(0xFFBFF000, false);
}

/* Creates a new page directory, fractal-mapped and with the kernel mapped */
static addr_t _clone_kernel_pd() {

    /* Request a new, unused page from the physical memory allocator
     * and place it at the PDE = 1022, PTE = 1023; right before the 
     * fractal mapping, in order to modify its value */
    addr_t pd_phys_addr;
    mm_alloc_page_kernel(0xFFBFF000, &pd_phys_addr);

    /* Initialize the first 768 entries to null (not present) */
    uint32_t * pd = (uint32_t *) (0xFFBFF000);

    int i;
    for (i = 0; i < 768; i++)
        pd[i] = 0;

    _update_kernel_pd(pd_phys_addr);

    /* free the page containing the cloned page directory from the current page directory,
       without passing it to the physical memory allocator AGAIN */
    
    mm_free_page(0xFFBFF000, false);

    return pd_phys_addr;
}

static void _clear_pd(addr_t cr3) {
    write_cr3(cr3);

    int i;
    for (i = 0; i < 768; i++)
        mm_free_pde(i, true); /* free all user pages */

    write_cr3(kernel_cr3);
}

/* Frees up the pages used by this page directory, giving the cr3 page back to the physical memory allocator */
static void _delete_pd(addr_t cr3) {
    _clear_pd(cr3);

    /* give this 4KB page back to the physical memory allocator */
    phys_mem_push(cr3);
}

/* Wrapper for _clear_pd, exposing the process pd functions out of process.c seems like
 * a bad design idea */
void process_clear_pd(struct process * p) {
    _clear_pd(p->p_cr3);
}

/* Creates a new process, or, if a parent is specified, copies that process. */
struct process * process_create(struct process * parent) {
    ACQUIRE_LOCK(process_tree_lock);

    struct process * p = (struct process *) kmalloc(sizeof(struct process));
    if (!p) {
        RELEASE_LOCK(process_tree_lock);
        return NULL;
    }

    memset(p, 0, sizeof(struct process));

    p->p_pid = _get_next_pid();

    p->p_root  = vroot;
    p->p_cwd   = vroot;
    p->p_cr3   = _clone_kernel_pd();

    p->p_regs.eflags = read_eflags() | (1 << 9); /* Fixing EFLAGS so IF is enabled when this process is run. */

    p->p_state = PROC_STATE_READY;

    p->p_children = list_create();

    if (!p->p_children) {
        /* Couldn't allocate memory for children */
        RELEASE_LOCK(process_tree_lock);
        kfree(p);
        return NULL;
    }

    /* Kernel stack */
    p->p_kesp  = (uint32_t) kmallocp(PROC_KERNEL_STACK_SIZE) + PROC_KERNEL_STACK_SIZE;

    if (!parent) {
        RELEASE_LOCK(process_tree_lock);
        return p;
    }

    p->p_uid  = parent->p_uid;
    p->p_euid = parent->p_euid;
    p->p_root = parent->p_root; p->p_cwd  = parent->p_cwd;

    p->p_parent = parent;

    list_push(parent->p_children, p);
    RELEASE_LOCK(process_tree_lock);
    return p;
}

/* Frees memory used up by a process, assuming it doesn't have any children */
void process_destroy(struct process * p) {
    ACQUIRE_LOCK(process_tree_lock);
    ASSERTF(p != init, "Tried to process_destroy(init)!\n");
    ASSERT(p->p_children->length == 0);
    list_free(p->p_children);
    _close_open_fds(p);
    _delete_pd(p->p_cr3);
    ptable_delete(p->p_pid);
    kfree(p);
    RELEASE_LOCK(process_tree_lock);

    return;
}

/* Exit the process with the given value */
void process_exit(struct process * p, int exitval) {
    p->p_state  = PROC_STATE_OVER;
    p->p_retval = exitval;

    _close_open_fds(p);
    list_push(process_over, p);

    return;
}

/* Queue Push */
void process_push(struct process * p) {
    if (p != idle)
        list_push(process_queue, p);
}

/* Queue Pop */
struct process * process_pop() {
    if (process_queue->length == 0) {
        return idle;
    }

    return list_pop_head(process_queue);
}

/* Self-explanatory name. */
static void _copy_page(addr_t from_cr3, addr_t from_begin,
                       addr_t to_cr3, addr_t to_begin) {
    addr_t to = to_begin;
    addr_t from = from_begin;

    /* Work some black magic here */
    addr_t to_phys = __get_phys_addr(to_cr3, to);
    addr_t from_phys = __get_phys_addr(from_cr3, from);

    mm_alloc_page_user_phys(0x0, to_phys);
    mm_alloc_page_user_phys(PAGE_SIZE, from_phys);

    memcpy((void *)0x0, (void *)PAGE_SIZE, PAGE_SIZE);

    mm_free_page(0x0, false);
    mm_free_page(PAGE_SIZE, false);
}

/* Copies everything in a process's first 768 PDEs */
void process_image_copy(struct process * from, struct process * to) {
    /* Keep every present page table entry's physical address in a list_t in the kernel heap.
     * That way, I have access to it from whatever page directory I'm currently using.
     * Then I just have to loop through it and let _copy_page work its magic. */
    list_t * pte_list = list_create();

    int pde, pte;

    write_cr3(from->p_cr3);

    for (pde = 0; pde < 768; pde++) {
        if (!__check_pd_present(pde))
            continue;

        for (pte = 0; pte < 1024; pte++) {
            if (!__check_pt_present(pde, pte))
                continue;

            list_push(pte_list, (void *)(pde * PAGE_TABLE_SIZE + pte * PAGE_SIZE));
        }
    }

    write_cr3(kernel_cr3);

    list_node_t * it;
    list_foreach(it, pte_list) {
        addr_t addr = (addr_t) it->value;

        process_alloc_page(to, addr);
        _copy_page(from->p_cr3, addr, to->p_cr3, addr);
    }

    list_free(pte_list);
}

/* Allocates a page at the given virtual address for the given process, if there isn't already one. */
void process_alloc_page(struct process * p, addr_t virt) {
    write_cr3(p->p_cr3);

    if (__check_addr_present(virt))
        goto out;

    mm_alloc_page_user(virt, NULL);

out:
    write_cr3(kernel_cr3);
}

/* Deallocates the page at the given virtual address */
void process_dealloc_page(struct process * p, addr_t virt) {
    write_cr3(p->p_cr3);

    if (!__check_addr_present(virt))
        goto out;

    mm_free_page(virt, true);

    out:
        write_cr3(kernel_cr3);
}

/* sbrk()s the given process */
void * process_sbrk(struct process * p, int increment) {
    addr_t addr = p->p_heap;

    addr_t tmp;

    /* I have no idea how to handle int overflows from addr + increment right now.
     * TODO
     *
     * Fatal mistake: the trust in userspace
     */

    if (increment > 0 && ((addr + increment >= PROC_IMAGE_USER_STACK_BOTTOM) && (addr + increment < PROC_IMAGE_USER_STACK_TOP)))
        return (void *) -1;

    if (increment > 0)
        for (tmp = PAGE_ALIGN_DOWN(addr); tmp < PAGE_ALIGN_UP(addr + increment); tmp += PAGE_SIZE)
            process_alloc_page(p, tmp);

    if (increment < 0)
        for (tmp = PAGE_ALIGN_DOWN(addr); tmp > PAGE_ALIGN_UP(addr + increment); tmp -= PAGE_SIZE)
            process_dealloc_page(p, tmp);

    p->p_heap += increment;

    return (void *) addr;
}

void process_memcpy(struct process * p, void * dest, void * src, size_t len) {
    write_cr3(p->p_cr3);
    memcpy(dest, src, len);
    write_cr3(kernel_cr3);
}

void process_memset(struct process * p, void * ptr, int value, size_t num) {
    write_cr3(p->p_cr3);
    memset(ptr, value, num);
    write_cr3(kernel_cr3);
}

/* Starts the given process using a fake IRET */
void process_start(struct process * p) {
    /* Update TSS kernel stack */
    set_tss_esp(p->p_kesp);

    /* Update CR3 */
    write_cr3(p->p_cr3);

    current_process = p;
    p->p_state = PROC_STATE_ACTIVE;

    /* MAGIC HAPPENS! */
    asm volatile ("push %0\n\t" /* SS */
                  "push %1\n\t" /* ESP */
                  "push %2\n\t" /* EFLAGS */
                  "push %3\n\t" /* CS */
                  "push %4\n\t" /* EIP */
                  :: "m"(p->p_regs.ss), "m"(p->p_regs.useresp), "m"(p->p_regs.eflags), "m"(p->p_regs.cs), "m"(p->p_regs.eip));

    asm volatile ("mov %0, %%ds\n\t"
                  "mov %1, %%es\n\t"
                  "mov %2, %%fs\n\t"
                  "mov %3, %%gs\n\t"
                  /* IRET */
                  "iret\n\t" ::  "m"(p->p_regs.ds), "m"(p->p_regs.es), "m"(p->p_regs.fs), "m"(p->p_regs.gs),
                  "a"(p->p_regs.eax), "b"(p->p_regs.ebx), "c"(p->p_regs.ecx), "d"(p->p_regs.edx), "S"(p->p_regs.esi), "D"(p->p_regs.edi));
}

struct process * process_clone(struct process * parent) {
    struct process * clone = process_create(parent);

    if (clone == NULL)
        return clone;

    /* Update registers */
    memcpy(&clone->p_regs, &parent->p_regs, sizeof(struct interrupt_registers));

    /* Update state */
    clone->p_state = PROC_STATE_READY;

    /* Clone file descriptors */
    _clone_open_fds(parent, clone);

    /* Copy the process image */
    process_image_copy(parent, clone);

    /* Kernel stack shouldn't be updated... It's fine just the way it is (empty).
     * Ideally there should NEVER be a situation where the kernel stack for a fork()ing or
     * fork()ed process contains anything other than the things required for fork().
     * Therefore, all signals and stuff like that will be always processed before the actual system calls.
     * Only the EAX register, for the child and parent processes should be
     * set in sys_fork().
     */
    clone->p_kesp = (uint32_t) kmallocp(PROC_KERNEL_STACK_SIZE) + PROC_KERNEL_STACK_SIZE;

    if (clone->p_kesp == (uint32_t) NULL)
        /* out of kernel memory */
        goto free;

    return clone;

free:
    /* Cleanup */
    kfree(clone);
    return NULL;
}

/* Reparent a given process:
 * Remove it and its children from its current parent node and link it to the root node,
 * which is the init process
 */
int process_reparent(struct process * p) {
    if (p->p_parent == init) {
        /* This process's parent already is init. */
        return 0;
    }

    ACQUIRE_LOCK(process_tree_lock);

    struct process * parent = p->p_parent;
    if (!parent) {
        /* Process has no parent, can't reparent since this is most likely a corrupt process */
        klog (KWARN "Process 0x%x has no parent\n", p);
        RELEASE_LOCK(process_tree_lock);
        return -1;
    }

    list_node_t * it = list_find(parent->p_children, p);

    if (!it) {
        klog (KWARN "Process 0x%x is corrupt: p->parent->p_children doesn't contain p.\n", p);
        RELEASE_LOCK(process_tree_lock);
        return -1;
    }

    /* Delete the process from its parent */
    list_delete(parent->p_children, it);

    /* Add it to the init */
    list_push(init->p_children, p);

    /* Update parent */
    p->p_parent = init;

    RELEASE_LOCK(process_tree_lock);
    return 0;
}

void save_registers(struct interrupt_registers * registers) {
    /* Called before a system call */
    memcpy(&current_process->p_regs, registers, sizeof(struct interrupt_registers));
}

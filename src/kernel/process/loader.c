#include <minios/kernel.h>
#include <minios/mm.h>
#include <minios/process.h>
#include <minios/spinlock.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/assert.h>
#include <minios/elf.h>

#include <sys/types.h>
#include <string.h>

/* For now,
 * this OS will support ELF. After the shell is implemented,
 * I will modify it to support shebangs too.
 */


/* Defines a type for functions which load files according to the required 
 * binary file type: e.g. A.out, ELF, #!, ... */
typedef int (*func_load_t)(struct process *, struct file *, char **, int, char **);

/* Defines the structure which holds pointers to different loader functions,
 * and their corresponding identifiers */
static struct _bin_fmt {
    func_load_t load;
    char magic[4]; /* Magic identifier */
} _bin_fmts[] = {
    {
        elf_load,
        {
            ELFMAG0,
            ELFMAG1,
            ELFMAG2,
            ELFMAG3
        }
    }
};

/* Only ELF so far */
#define BIN_FMT_CNT             1

/* Checks whether the first 4 bytes of a file match the magic constant required for a certain format. */
static bool _match(char * magic, char * header) {
    int i;

    for (i = 0; i < 4; i++) {
        if (magic[i] == '\0')
            continue;

        if (magic[i] != header[i])
            return false;
    }

    return true;
}

/* Returns the valid format by looking through the formats in _bin_fmts */
static int _get_valid_fmt(char * bytes) {
    int i;

    for (i = 0; i < BIN_FMT_CNT; i++) {
        if (_match(_bin_fmts[i].magic, bytes)) {
            return i;
        }
    }

    return -1;
}

/* Loads the file specified by path into the virtual memory of the process p.
 * argv and envp are pushed into some registers, to be handled by the
 * _start in crt0
 */
int process_load(struct process * p, char * path, char ** argv, int argc, char ** envp) {
    /*
     * Read the first 4 bytes of the file,
     * if it exists.
     */

    struct file * f = kopen(path, 0);

    if (!f)
        return -ENOENT;

    /* Check if it's a regular file. */
    if (!S_ISREG(f->f_vn->v_mode)) {
        kclose(f);
        return -EACCES;
    }

    char bytes[4];

    ssize_t n = vfop_pread(f, bytes, 4, 0);
    if (n < 0) /* Error while reading... */
        return n;

    /* Get the valid format, according to these first four bytes. */

    int fmt = _get_valid_fmt(bytes);

    /* No valid formats, ouch. */
    if (fmt < 0)
        return -ENOEXEC;

    /* Just pass this to the corresponding lib */
    int ret = _bin_fmts[fmt].load(p, f, argv, argc, envp);

    /* And take care to close any opened files. */
    kclose(f);

    return ret;
}

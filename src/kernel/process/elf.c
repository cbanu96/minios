#include <minios/kernel.h>
#include <minios/mm.h>
#include <minios/process.h>
#include <minios/spinlock.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/assert.h>
#include <minios/elf.h>

#include <sys/io.h>
#include <sys/types.h>
#include <string.h>

extern addr_t kernel_cr3;

int elf_load(struct process * p, struct file * f, char ** argv, int argc, char ** envp) {
    /* The way this function works is this:
     * We first load the file from hdd or wherever it's stored into the kernel heap.
     * Then, we copy its contents accordingly into the process image
     * Free the kernel heap
     * argv and envp onto the process heap
     * and we're done.
     */

    /* Get the length of the file */
    off_t length = vop_get_size(f->f_vn);
    
    /* Allocate a big enough chunk of memory */
    char * contents = (char *) kmalloc(sizeof(char) * length);

    /* Read the whole file into memory. Kinda weird since we're using only
     * RAMFS at the moment, but let's assume this is a HDD or whatever */
    vfop_pread(f, contents, length, 0);

    ELF32_Header * elf = (ELF32_Header *) contents;

    /* Now, even though this function should be called only if the format of the file
     * is valid, let's still check the header.
     * Who knows whatever mysterious errors may slip through? */

    if (elf->e_ident[EI_MAG0] != ELFMAG0 ||
        elf->e_ident[EI_MAG1] != ELFMAG1 ||
        elf->e_ident[EI_MAG2] != ELFMAG2 ||
        elf->e_ident[EI_MAG3] != ELFMAG3) {
        /* Not a valid ELF file */

        kfree(elf); /* Oops, almost leaked the memory... */
        return -ENOEXEC;
    }

    /* Since this may be executed on the current process inside a system call,
     * we must first deallocate any memory used by the current process, and if any interrupts happen, don't let the scheduler run this,
     * lest we get the dreaded SEGFAULT.
     *
     * But keep in mind that the argv,envp are most certainly in the memory used by the process.
     * Solution: allocate them in the kernel memory, nice and clean (tho the extra memory cannot be helped...) :)
     */

    uint16_t i;

    int envc = 0;
    while (envp[envc++] != NULL);
    envc--;

    char ** kargv, ** kenvp;

    kargv = kmalloc(sizeof(char *) * argc);
    kenvp = kmalloc(sizeof(char *) * envc);

    for (i = 0; i < argc; i++) {
        size_t len = 1 + strlen(argv[i]);
        kargv[i] = kmalloc(sizeof(char) * len);
        memcpy(kargv[i], argv[i], len);
    }

    for (i = 0; i < envc; i++) {
        size_t len = 1 + strlen(envp[i]);
        kargv[i] = kmalloc(sizeof(char) * len);
        memcpy(kenvp[i], envp[i], len);
    }

    /* Point argv and envp to kernel their counterparts, cba modifying tons of variables below. */
    argv = kargv;
    envp = kenvp;

    process_clear_pd(p);

    /* Heap will be placed right after the highest address in the program */
    addr_t heap = 0;

    /* Loop through the section headers */
    addr_t addr = (addr_t) elf + elf->e_shoff;

    for (i = 0; i < elf->e_shnum; i++, addr += elf->e_shentsize) {
        ELF32_SHeader * section = (ELF32_SHeader *) addr;

        /* If the sh_addr is null, this section must be skipped. */
        if (section->sh_addr == 0)
            continue;

        /* Get the page aligned beginning and ending of the process image section */
        addr_t begin = PAGE_ALIGN_DOWN(section->sh_addr);
        addr_t size  = PAGE_ALIGN_UP(section->sh_addr + section->sh_size) - begin;

        if (begin + size > heap)
            heap = begin + size;

        /* Allocate memory, and give it user privileges, not kernel. */
        addr_t alloc_addr;
        for (alloc_addr = begin; alloc_addr < begin + size; alloc_addr += PAGE_SIZE) {
            /* process_alloc_page automatically takes care of any overlapping addresses (this may happen because of the page alignment) */
            process_alloc_page(p, alloc_addr);
        }

        if (section->sh_type == SHT_NOBITS) {
            /* If it's a NOBITS (.bss), zero it out */
            process_memset(p, (void *) section->sh_addr, 0, section->sh_size);
        }
        else {
            /* else, just copy */

            /* Make sure kernel pages are updated, so the "elf" structure is in p's kernel memory as well. */
            process_update_kernel_pages(p);

            process_memcpy(p, (void *) section->sh_addr, (void *)((addr_t) elf + section->sh_offset), section->sh_size);
        }
    }

    /* Allocate the user stack */
    for (addr = PROC_IMAGE_USER_STACK_BOTTOM; addr <= PROC_IMAGE_USER_STACK_TOP; addr += PAGE_SIZE) {
        process_alloc_page(p, addr);
    }

    p->p_regs.useresp = PROC_IMAGE_USER_STACK_TOP;

    /* Set the eip */
    p->p_regs.eip     = (addr_t) elf->e_entry;


    /* We don't need the ELF anymore */
    kfree(elf);

    /* Make sure the first 4KB of the heap are allocated */
    for (addr = heap; addr <= PAGE_ALIGN_UP(heap + sizeof(char *) * (argc + envc + 2)); addr += PAGE_SIZE)
        process_alloc_page(p, addr);

    /* Place the ** ptrs at the beginning of the heap */
    char ** p_argv = (char **) heap;
    heap += sizeof(char *) * (argc + 1);

    char ** p_envp = (char **) heap;
    heap += sizeof(char *) * (envc + 1);

    /* For each env or arg, place them here */
    for (i = 0; i < argc; i++) {
        int length = strlen(argv[i]) + 1;
        for (addr = heap; addr < PAGE_ALIGN_UP(heap + length); addr += PAGE_SIZE) {
            process_alloc_page(p, addr);
        }

        write_cr3(p->p_cr3);
        p_argv[i] = (char *) heap;
        write_cr3(kernel_cr3);

        process_memcpy(p, (void *) heap, argv[i], length);
        heap += length;
    }

    write_cr3(p->p_cr3);
    p_argv[argc] = NULL;
    write_cr3(kernel_cr3);

    for (i = 0; i < envc; i++) {
        int length = strlen(envp[i]) + 1;
        for (addr = heap; addr < PAGE_ALIGN_UP(heap + length); addr += PAGE_SIZE) {
            process_alloc_page(p, addr);
        }

        write_cr3(p->p_cr3);
        p_envp[i] = (char *) heap;
        write_cr3(kernel_cr3);

        process_memcpy(p, (void *) heap, envp[i], length);
        heap += length;
    }

    /* Free memory used by kernel argv,envp */
    kfree(argv);
    kfree(envp);

    write_cr3(p->p_cr3);
    p_envp[envc] = NULL;
    write_cr3(kernel_cr3);

    /* Put argv, argc and envp in registers. */
    p->p_regs.eax = argc;
    p->p_regs.ebx = (uint32_t) p_argv;
    p->p_regs.ecx = (uint32_t) p_envp;

    /* Save the heap pointer. */
    p->p_heap = heap;

    return 0;
}

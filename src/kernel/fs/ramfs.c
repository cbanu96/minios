#include <minios/kernel.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/mm.h>
#include <minios/spinlock.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>

#define RAMFS_DEV_NUMBER      1
#define RAMFS_FIRST_UNRES_INO 1
#define RAMFS_BLOCK_SIZE      1024
#define RAMFS_MAX_BLOCKS      128

static int ramfs_vn_delete(struct vnode *);
static int ramfs_vn_free(struct vnode *);

static int ramfs_dir_mknod(struct vnode *, const char *, mode_t, int);
static int ramfs_dir_create(struct vnode *, const char *, mode_t);
static int ramfs_dir_link(struct vnode *, const char *, struct vnode *);
static int ramfs_dir_symlink(struct vnode *, const char *, const char *);
static int ramfs_dir_mkdir(struct vnode *, const char *, mode_t);
static int ramfs_dir_rename(struct vnode *, size_t, const char *);
static int ramfs_dir_rmdir(struct vnode *, size_t);
static int ramfs_dir_unlink(struct vnode *, size_t);
static int ramfs_dir_lookup(struct vnode *, size_t, struct vnode **, char **);

static int     ramfs_file_truncate(struct vnode *, off_t);
static off_t   ramfs_file_get_size(struct vnode *);
off_t   ramfs_file_seek(struct file *, off_t, int);
ssize_t ramfs_file_read(struct file *, char *, size_t);
ssize_t ramfs_file_write(struct file *, char *, size_t);
static ssize_t ramfs_file_pread(struct file *, char *, size_t, off_t);
static ssize_t ramfs_file_pwrite(struct file *, char *, size_t, off_t);

int ramfs_file_open(struct vnode *, struct file *);
int ramfs_file_close(struct vnode *, struct file *);

static spinlock_t ramfs_lock;

static struct vnode_ops ramfs_dir_ops = {
    .create  = &ramfs_dir_create,
    .link    = &ramfs_dir_link,
    .symlink = &ramfs_dir_symlink,
    .mknod   = &ramfs_dir_mknod,
    .mkdir   = &ramfs_dir_mkdir,
    .rename  = &ramfs_dir_rename,
    .rmdir   = &ramfs_dir_rmdir,
    .unlink  = &ramfs_dir_unlink,
    .truncate = NULL,
    .get_size = NULL,
    .lookup  = &ramfs_dir_lookup,
    .delete  = &ramfs_vn_delete,
    .free    = &ramfs_vn_free
};

static struct file_ops ramfs_dir_fops = {
    .seek   = NULL,
    .open   = NULL,
    .close  = NULL,
    .read   = NULL,
    .write  = NULL,
    .pread  = NULL,
    .pwrite = NULL
};

static struct vnode_ops ramfs_file_ops = {
    .create  = NULL,
    .link    = NULL,
    .symlink = NULL,
    .mknod   = NULL,
    .mkdir   = NULL,
    .rename  = NULL,
    .rmdir   = NULL,
    .unlink  = NULL,
    .truncate = &ramfs_file_truncate,
    .get_size = &ramfs_file_get_size,
    .lookup  = NULL,
    .delete  = &ramfs_vn_delete,
    .free    = &ramfs_vn_free
};

static struct file_ops ramfs_file_fops = {
    .seek   = &ramfs_file_seek,
    .open   = &ramfs_file_open,
    .close  = &ramfs_file_close,
    .read   = &ramfs_file_read,
    .write  = &ramfs_file_write,
    .pread  = &ramfs_file_pread,
    .pwrite = &ramfs_file_pwrite 
};

static int ramfs_dir_rmdir_self(struct vnode * v) {
    if (!S_ISDIR(v->v_mode))
        return -ENOTDIR;

    list_t * entries = (list_t *) ((struct ramfs_int_dir_data *) v->v_fsdata)->d_children;

    if (entries->length > 2)
        return -ENOTEMPTY;

    struct ramfs_internal_entry *dotdot = list_pop_tail(entries);
    struct ramfs_internal_entry *dot = list_pop_tail(entries);

    dec_nlink(dotdot->e_vn);
    dec_nlink(dot->e_vn);

    kfree(dotdot);
    kfree(dot);

    return 0;
}

static int ramfs_get_free_inode() {
    static int last_ino = RAMFS_FIRST_UNRES_INO;
    return last_ino++;
} 

static inline void * ramfs_alloc_data(int flag) {
    if (S_ISDIR(flag)) {
        struct ramfs_int_dir_data * data = kmalloc(sizeof(struct ramfs_int_dir_data));
        data->d_children = list_create();

        return data;
    } else {
        struct ramfs_int_fil_data * data = kmalloc(sizeof(struct ramfs_int_fil_data));
        data->f_blocks = list_create();
        data->f_size = 0;

        return data;
    }
}

static inline void ramfs_free_data(struct vnode * v) {
    if (S_ISDIR(v->v_mode)) {
        struct ramfs_int_dir_data * dir_data = (struct ramfs_int_dir_data *) v->v_fsdata;

        /* Free the list, their values should be empty when free()ing */
        list_free(dir_data->d_children);
        /* Free the data */
        kfree(dir_data);

        /* Replace with NULL */
        v->v_fsdata = NULL;
    } else {
        struct ramfs_int_fil_data * fil_data = (struct ramfs_int_fil_data *) v->v_fsdata;

        fil_data->f_size = 0;
        /* Free blocks */
        list_free_val(fil_data->f_blocks);
        /* Free the actual list */
        list_free(fil_data->f_blocks);
        /* And free the data structure */
        kfree(fil_data);

        /* Replace with NULL */
        v->v_fsdata = NULL;
    }
}

int ramfs_dir_add_child(struct vnode * v, struct vnode * w, const char * n) {
    if (!S_ISDIR(v->v_mode))
        return -ENOTDIR;

    list_t * entries = (list_t *) ((struct ramfs_int_dir_data *) v->v_fsdata)->d_children;
    list_node_t * it;
    struct ramfs_internal_entry * entry;

    list_foreach(it, entries) {
        entry = (struct ramfs_internal_entry *) it->value;

        if (!strcmp(entry->e_name, n))
            return -EEXIST;
    }

#if 0
    klog (KDEBUG
            "entries: 0x%x\n"
            "fsdata: 0x%x\n"
            "children: 0x%x\n",
            entries, v->v_fsdata, ((struct ramfs_int_dir_data *) v->v_fsdata)->d_children);
#endif

    entry = (struct ramfs_internal_entry *) kmalloc(sizeof(struct ramfs_internal_entry));

    strcpy(entry->e_name, n);
    entry->e_vn = w;

    inc_nlink(w);

    ACQUIRE_LOCK(ramfs_lock);
    list_push(entries, entry); 
    RELEASE_LOCK(ramfs_lock);

    return 0;
}

static int ramfs_dir_mknod(struct vnode * v, const char * n, mode_t mode, int flag) {
    struct vnode * child = (struct vnode *) kmalloc(sizeof(struct vnode));

    int err = 0;

    memset(child, 0, sizeof(struct vnode));
    child->v_dev = v->v_dev;
    child->v_ino = ramfs_get_free_inode();
    child->v_mode = mode | flag;
    child->v_fsdata = ramfs_alloc_data(flag); 

    if (S_ISDIR(flag)) {
        child->v_ops  = &ramfs_dir_ops;
        child->v_fops = &ramfs_dir_fops;

        ramfs_dir_add_child(child, child, ".");
        ramfs_dir_add_child(child, v, "..");
    } else if (S_ISREG(flag)) {
        child->v_ops  = &ramfs_file_ops;
        child->v_fops = &ramfs_file_fops;
    } else if (S_ISLNK(flag)) {
        /* todo - implement */
        err = -EPERM;
        goto free;
    } else {
        err = -EPERM;
        goto free;
    }

    err = ramfs_dir_add_child(v, child, n);
    if (err < 0) {
        goto free;
    }

    return 0;

free:
    if (S_ISDIR(flag)) {
        ramfs_dir_rmdir_self(child);
    }
    ramfs_vn_delete(child);
    return err;
}

static int ramfs_dir_create(struct vnode * v, const char * n, mode_t mode) {
    return ramfs_dir_mknod(v, n, mode, S_IFREG);
}

static int ramfs_dir_link(struct vnode * v, const char * n, struct vnode * link) {
    return ramfs_dir_add_child(v, link, n);
}

static int ramfs_dir_symlink(struct vnode * v, const char * n, const char * symlink) {
    /* TODO */
    (void)v;
    (void)n;
    (void)symlink;
    return 0;
}

static int ramfs_dir_mkdir(struct vnode * v, const char * n, mode_t mode) {
    return ramfs_dir_mknod(v, n, mode, S_IFDIR);
}

static int ramfs_dir_rename(struct vnode * v, size_t idx, const char * to) {
    if (!S_ISDIR(v->v_mode)) {
        return -ENOTDIR;
    }

    if (idx < 2) /* Can't rename . and .. */
        return -EPERM;

    struct vnode * child;
    char * fname;
    int err;
    
    err = ramfs_dir_lookup(v, idx, &child, &fname);
    if (err) {
        return err;
    }

    ACQUIRE_LOCK(ramfs_lock);
    strcpy(fname, to);
    RELEASE_LOCK(ramfs_lock);
    return 0;
}

static int ramfs_dir_rmdir(struct vnode * v, size_t idx) {
    if (!S_ISDIR(v->v_mode)) {
       return -ENOTDIR;
    }

    if (idx < 2) /* Can't remove . and .. */
       return -EPERM;

    struct vnode * child;
    char * fname;
    int err;

    err = ramfs_dir_lookup(v, idx, &child, &fname);
    if (err)
        return err;

    err = ramfs_dir_rmdir_self(child);
    if (err)
        return err;

    return ramfs_dir_unlink(v, idx);
}

static int ramfs_dir_unlink(struct vnode * v, size_t idx) {
    if (!S_ISDIR(v->v_mode))
        return -ENOTDIR;

    if (idx < 2)
        return -EPERM;

    list_t * entries = (list_t *) ((struct ramfs_int_dir_data *) v->v_fsdata)->d_children;

    ACQUIRE_LOCK(ramfs_lock);
    list_node_t *it = list_find_idx(entries, idx);
    RELEASE_LOCK(ramfs_lock);

    if (!it)
        return -ENOENT;

    struct ramfs_internal_entry * entry = (struct ramfs_internal_entry *)(it->value);

    if (entry->e_vn->v_refcount > 0 && entry->e_vn->v_nlink == 1) {
        /* This is the last instance in the VFS tree corresponding to that specific vnode child,
         * but, more importantly, the vnode is still opened by some process or other (refcount > 0)
         */

        /* Therefore, we must not dec_nlink(). */
        return -EBUSY;
    }

    dec_nlink(entry->e_vn);

    ACQUIRE_LOCK(ramfs_lock);
    kfree(entry);
    list_delete(entries, it);
    RELEASE_LOCK(ramfs_lock);

    return 0;
}

static int ramfs_dir_lookup(struct vnode * v, size_t idx, struct vnode ** w, char ** n) {
    if (!S_ISDIR(v->v_mode))
        return -ENOTDIR;

    struct ramfs_internal_entry * entry;

    list_t * entries = (list_t *) ((struct ramfs_int_dir_data *) v->v_fsdata)->d_children;

    ACQUIRE_LOCK(ramfs_lock);
    list_node_t * it = list_find_idx(entries, idx);
    RELEASE_LOCK(ramfs_lock);

    if (!it)
        return -ENOENT;

    entry = (struct ramfs_internal_entry *)(it->value);

    if (w)
        *w = entry->e_vn;

    if (n)
        *n = entry->e_name;

    return 0;
}

/* precisely truncating to exactly {off} bytes is unpractical
 * for our ramfs... we'll just round it up a bit.
 */
static int ramfs_file_truncate(struct vnode * v, off_t off) {
    if (S_ISDIR(v->v_mode))
        return -EISDIR;

    struct ramfs_int_fil_data * data = (struct ramfs_int_fil_data *) v->v_fsdata;
    list_t * blocks = data->f_blocks;

    if (off >= (off_t) RAMFS_MAX_BLOCKS * RAMFS_BLOCK_SIZE)
        return -EFBIG;

    size_t block = off / RAMFS_BLOCK_SIZE;

    ACQUIRE_LOCK(ramfs_lock);
    /* Free/allocate blocks as needed */
    while (blocks->length > block) {
        /* pop the tail and free the block */
        kfree(list_pop_tail(blocks));
    }
    while (blocks->length <= block) {
        list_push(blocks, kmalloc(RAMFS_BLOCK_SIZE));
    }
    /* And update the size of the file in _fsdata */
    if (data->f_size < off) {
        data->f_size = off;
    }

    RELEASE_LOCK(ramfs_lock);

    return 0;
}

/* Returns actual size of the file */
static off_t ramfs_file_get_size(struct vnode * v) {
    struct ramfs_int_fil_data * data = (struct ramfs_int_fil_data *) v->v_fsdata;

    return data->f_size;
}

static int ramfs_vn_delete(struct vnode * v) {
    if (v->v_refcount) {
        return -EBUSY;
    }

    ACQUIRE_LOCK(ramfs_lock);
    ramfs_free_data(v);
    kfree(v);
    RELEASE_LOCK(ramfs_lock);

    return 0;
}

static int ramfs_vn_free(struct vnode * v) {
    if (v->v_refcount || v->v_nlink)
        return -1;

    return ramfs_vn_delete(v);
}

off_t ramfs_file_seek(struct file * f, off_t off, int whence) {
    off_t file_size = ((struct ramfs_int_fil_data *) (f->f_vn->v_fsdata))->f_size;

    if (S_ISDIR(f->f_vn->v_mode))
        return 0;

    switch (whence) {
        case SEEK_SET:
            if (off < 0)
                return -1;
            f->f_pos = off;
            break;
        case SEEK_CUR:
            if (f->f_pos + off < 0)
                return -1;
            f->f_pos += off;
            break;
        case SEEK_END:
            if (file_size + off < 0)
                return -1;
            else
                f->f_pos = file_size + off;
            break;
    }

    return f->f_pos;
}

ssize_t ramfs_file_read(struct file * f, char * b, size_t n) {
    ssize_t err = vfop_pread(f, b, n, f->f_pos);
    if (err < 0) {
        return err;
    } else {
        vfop_seek(f, err, SEEK_CUR);
        return err;
    }
}

ssize_t ramfs_file_write(struct file * f, char * b, size_t n) {
    ssize_t err = vfop_pwrite(f, b, n, f->f_pos);
    if (err < 0) {
        return err;
    } else {
        vfop_seek(f, err, SEEK_CUR);
        return err;
    }
}

static ssize_t ramfs_file_pread(struct file * f, char * b, size_t n, off_t o) {
    if (o > ramfs_file_get_size(f->f_vn)) {
        return 0;
    }

    list_t * blocks = (list_t *) ((struct ramfs_int_fil_data *) f->f_vn->v_fsdata)->f_blocks;
    list_node_t * it;

    ssize_t start, end, r;
    start = o;
    end = o + n;
    r = 0;
    ssize_t fsize = ramfs_file_get_size(f->f_vn);
    if (fsize < end)
        end = fsize;

    while (start < end) {
        ssize_t processed;
        size_t block_idx = start / RAMFS_BLOCK_SIZE;
        size_t block_off = start % RAMFS_BLOCK_SIZE;

        it = list_find_idx(blocks, block_idx);
        void * block = it->value;

        processed = RAMFS_BLOCK_SIZE - block_off;
        if (end - start < processed)
            processed = end - start;

        memcpy(b+r, block + block_off, processed);

        start += processed;
        r += processed;
    }

    return r;
}

static ssize_t ramfs_file_pwrite(struct file * f, char * b, size_t n, off_t o) {
    int err;
    off_t f_allocated;

    list_t * blocks = (list_t *) ((struct ramfs_int_fil_data *) f->f_vn->v_fsdata)->f_blocks;
    list_node_t * it;

    f_allocated = (off_t) RAMFS_BLOCK_SIZE * blocks->length;

    if ((off_t) (o + n) > f_allocated) {
        err = ramfs_file_truncate(f->f_vn, o+n); /* by truncate I mean extend */
        if (err < 0) {
            return err;
        }
    }

    ssize_t start, end, r;
    start = o;
    end = o + n;
    r = 0;
    f_allocated = (off_t) RAMFS_BLOCK_SIZE * blocks->length;
    if (f_allocated < end)
        end = f_allocated;

    while (start < end) {
        ssize_t processed;
        size_t block_idx = start / RAMFS_BLOCK_SIZE;
        size_t block_off = start % RAMFS_BLOCK_SIZE;

        it = list_find_idx(blocks, block_idx);
        void * block = it->value;

        processed = RAMFS_BLOCK_SIZE - block_off;
        if (end - start < processed)
            processed = end - start;

        memcpy(block + block_off, b+r, processed);

        start += processed;
        r += processed;
    }

    return r;
}

int ramfs_file_open(struct vnode * v, struct file * f) {
    f->f_vn = v;
    f->f_pos = 0;
    inc_refcount(v);

    return 0;
}

int ramfs_file_close(struct vnode * v, struct file * f) {
    f->f_vn = NULL;
    f->f_pos = 0;
    dec_refcount(v);
    return 0;
}

int ramfs_init(struct vnode * root, void * device) {
    (void)device;

    klog (KINFO "Setting up ramfs... ");

    memset(root, 0, sizeof(struct vnode));

    root->v_dev = RAMFS_DEV_NUMBER;
    root->v_ino = ramfs_get_free_inode();
    root->v_mode = 0755 | S_IFDIR;
    root->v_fsdata = ramfs_alloc_data(S_IFDIR);
    root->v_ops  = &ramfs_dir_ops;
    root->v_fops = &ramfs_dir_fops;

    klog (KINFO "\t\t\t\tDone!\n");

    return 0;
}

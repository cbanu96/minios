#include <minios/kernel.h>
#include <minios/fs/vfs.h>
#include <minios/fs/device.h>
#include <minios/log.h>
#include <minios/mm.h>
#include <minios/spinlock.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>

#define DEVFS_DEV_NUMBER 2

static int devfs_vn_delete(struct vnode *);
static int devfs_root_add_child(struct vnode *, struct vnode *, const char *);
static int devfs_root_mknod(struct vnode *, const char *, mode_t, int);
static int devfs_root_lookup(struct vnode *, size_t, struct vnode **, char **);

extern int ramfs_file_open(struct vnode *, struct file *);
extern int ramfs_file_close(struct vnode *, struct file *);
extern off_t   ramfs_file_seek(struct file *, off_t, int);
extern ssize_t ramfs_file_read(struct file *, char *, size_t);
extern ssize_t ramfs_file_write(struct file *, char *, size_t);
static ssize_t devfs_dev_pread(struct file *, char *, size_t, off_t);
static ssize_t devfs_dev_pwrite(struct file *, char *, size_t, off_t);

static spinlock_t devfs_lock;

static struct vnode_ops devfs_root_ops = {
    .create  = NULL,
    .link    = NULL,
    .symlink = NULL,
    .mknod   = &devfs_root_mknod,
    .mkdir   = NULL,
    .rename  = NULL,
    .rmdir   = NULL,
    .unlink  = NULL,
    .truncate = NULL,
    .get_size = NULL,
    .lookup  = &devfs_root_lookup,
    .delete  = &devfs_vn_delete
};

static struct file_ops devfs_root_fops = {
    .seek   = NULL,
    .open   = NULL,
    .close  = NULL,
    .read   = NULL,
    .write  = NULL,
    .pread  = NULL,
    .pwrite = NULL
};

static struct vnode_ops devfs_dev_ops = {
    .create  = NULL,
    .link    = NULL,
    .symlink = NULL,
    .mknod   = NULL,
    .mkdir   = NULL,
    .rename  = NULL,
    .rmdir   = NULL,
    .unlink  = NULL,
    .truncate = NULL,
    .get_size = NULL,
    .lookup  = NULL,
    .delete  = &devfs_vn_delete
};

static struct file_ops devfs_dev_fops = {
    .seek   = &ramfs_file_seek,
    .open   = &ramfs_file_open,
    .close  = &ramfs_file_close,
    .read   = &ramfs_file_read,
    .write  = &ramfs_file_write,
    .pread  = &devfs_dev_pread,
    .pwrite = &devfs_dev_pwrite 
};

static inline void * devfs_alloc_root_data() {
    struct ramfs_int_dir_data * data = (struct ramfs_int_dir_data *) kmalloc(sizeof(struct ramfs_int_dir_data));
    data->d_children = list_create();

    return data;
}

static int devfs_get_free_inode() {
    static int last_ino = 1;
    return last_ino++;
}

static int devfs_root_add_child(struct vnode * v, struct vnode * w, const char * n) {
    list_t * entries = (list_t *) ((struct ramfs_int_dir_data *)v->v_fsdata)->d_children;
    list_node_t * it;
    struct ramfs_internal_entry * entry;

    list_foreach(it, entries) {
        entry = (struct ramfs_internal_entry *) it->value;
        if (!strcmp(entry->e_name, n))
            return -EEXIST;
    }

    entry = (struct ramfs_internal_entry *) kmalloc(sizeof(struct ramfs_internal_entry));

    strcpy(entry->e_name, n);
    entry->e_vn = w;

    inc_nlink(w);

    ACQUIRE_LOCK(devfs_lock);
    list_push(entries, entry); 
    RELEASE_LOCK(devfs_lock);

    return 0;
}

static int devfs_vn_delete(struct vnode * v) {
    UNUSED_VAR(v);
    /* devfs stuff can't be deleted. not even by superuser */
    return -EPERM;
}

static int devfs_root_mknod(struct vnode * v, const char * n, mode_t mode, int flag) {
    struct vnode * child = (struct vnode *) kmalloc(sizeof(struct vnode));
    memset(child, 0, sizeof(struct vnode));

    if (!S_ISCHR(flag) && !S_ISBLK(flag)) {
        goto free;
    }

    child->v_dev = v->v_dev;
    child->v_ino = devfs_get_free_inode();
    child->v_mode = mode | flag;
    child->v_fsdata = NULL;
    child->v_ops = &devfs_dev_ops;
    child->v_fops = &devfs_dev_fops;

    devfs_root_add_child(v, child, n);
    return 0;

free:
    kfree(child);
    return -1;
}

static int devfs_root_lookup(struct vnode * v, size_t idx, struct vnode ** w, char ** n) {
    struct ramfs_internal_entry * entry;

    list_t * entries = (list_t *)((struct ramfs_int_dir_data *) v->v_fsdata)->d_children;

    ACQUIRE_LOCK(devfs_lock);
    list_node_t * it = list_find_idx(entries, idx);
    RELEASE_LOCK(devfs_lock);

    if (!it)
        return -ENOENT;

    entry = (struct ramfs_internal_entry *)(it->value);

    if (w)
        *w = entry->e_vn;

    if (n)
        *n = entry->e_name;

    return 0;
}


static ssize_t devfs_dev_pread(struct file * f, char * b, size_t n, off_t o) {
    struct device * dev = (struct device *)(f->f_vn->v_fsdata);
    if (!dev) return -ENOENT;
    if (!dev->pread) return -EPERM;

    return dev->pread(dev, b, n, o);
}

static ssize_t devfs_dev_pwrite(struct file * f, char * b, size_t n, off_t o) {
    struct device * dev = (struct device *)(f->f_vn->v_fsdata);
    if (!dev) return -ENOENT;
    if (!dev->pwrite) return -EPERM;

    return dev->pwrite(dev, b, n, o);
}

int devfs_init(struct vnode * root, void * device) {
    (void)device;

    klog (KINFO "Setting up devfs... ");
    root->v_dev = DEVFS_DEV_NUMBER;
    root->v_ino = devfs_get_free_inode();
    root->v_mode = 0755 | S_IFDIR;
    root->v_fsdata = devfs_alloc_root_data();
    root->v_ops  = &devfs_root_ops;
    root->v_fops = &devfs_root_fops;

    devfs_root_add_child(root, root, ".");
    devfs_root_add_child(root, root, "..");

    klog (KINFO "\t\t\t\tDone!\n");

    return 0;
}

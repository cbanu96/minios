#include <minios/kernel.h>
#include <minios/x86/portio.h>
#include <minios/x86/dt.h>
#include <minios/fs/vfs.h>
#include <minios/fs/device.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>
#include <sys/io.h>

#define COM_PIPE_SIZE 256

static struct vnode * com1_pipe;
static struct file * com1_pipe_file; 

static struct vnode * com2_pipe;
static struct file * com2_pipe_file; 

static struct vnode * com3_pipe;
static struct file * com3_pipe_file; 

static struct vnode * com4_pipe;
static struct file * com4_pipe_file; 

static bool com_poll_read(int port) {
    return inb(port + 5) & 0x01;
}

static bool com_poll_write(int port) {
    return inb(port + 5) & 0x20;
}

static char com_read_one(int port) {
    while (!com_poll_read(port));
    char x = inb(port);
    if (x == '\r')
        x = '\n';
    outb(port, x);
    return x;
}

static void com_write_one(int port, char x) {
    while (!com_poll_write(port));
    outb(port, x);
}

static void com_handle_13() {
    /* Get the port */
    int port;

    struct file * f;

    /* If the byte waiting at COM1_PORT + 1 has byte 0 set,
     * then data is waiting on COM1, else it's waiting on COM3. */
    uint8_t val = inb(COM1_PORT + 1);
    if (val & 1) {
        port = COM1_PORT;
        f = com1_pipe_file;
    } else {
        port = COM3_PORT;
        f = com3_pipe_file;
    }
    
    /* Read the byte from the data register */
    char data = com_read_one(port);

    /* EOI the IRQ */
    irq_send_eoi(IRQ(COM1_IRQ)); /* same irq for com1/com3 */

    /* Bag this char. in the pipe */
    vfop_write(f, &data, 1);
}

static void com_handle_24() {
    /* Analogous to com_handle_13 */

    /* Get the port */
    int port;

    struct file * f;

    /* If the byte waiting at COM2_PORT + 1 has byte 0 set,
     * then data is waiting on COM2, else it's waiting on COM4. */
    uint8_t val = inb(COM2_PORT + 1);
    if (val & 1) {
        port = COM2_PORT;
        f = com2_pipe_file;
    } else {
        port = COM4_PORT;
        f = com4_pipe_file;
    }

    /* Read the byte from the data register */
    char data = com_read_one(port);

    /* EOI the IRQ */
    irq_send_eoi(IRQ(COM2_IRQ)); /* same irq for com2/com4 */

    /* Bag this char. in the pipe */
    vfop_write(f, &data, 1);
}

static void com_handler(struct interrupt_registers * regs) {
    switch (regs->int_no) {
        case IRQ(COM1_IRQ): 
            com_handle_13();
            break;
        case IRQ(COM2_IRQ):
            com_handle_24();
            break;
    }
}

static void _com_init_port(int port) {
    /* Enable serial port */
    outb(port + 1, 0x00);
    outb(port + 3, 0x80); /* DLAB: on */
    outb(port,     0x03); /* Divisor: 3 => Baud Rate = 38400 */
    outb(port + 1, 0x00); 
    outb(port + 3, 0x03); /* 8 bits, no parity, 1 stop bit, DLAB: off */
    outb(port + 2, 0xC7); /* FIFO / clear */
    outb(port + 4, 0x0B); /* Enable interrupts */
    outb(port + 1, 0x01);
}

static int _get_port_by_file(struct file * f) {
    if (f == com1_pipe_file)
        return COM1_PORT;
    if (f == com2_pipe_file)
        return COM2_PORT;
    if (f == com3_pipe_file)
        return COM3_PORT;
    if (f == com4_pipe_file)
        return COM4_PORT;

    PANIC("_get_port_by_file: Given file isn't a pipe to a COMx!!!");
    return 0;
}

static int com_probe(struct device * dev, struct vnode * v) {
    UNUSED_VAR(dev);
    UNUSED_VAR(v);
    return 0; /* always present */
}

static ssize_t com_pread(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(o);

    struct file * pipe_file = (struct file *) dev->data;

    return vfop_read(pipe_file, b, n);
}

static ssize_t com_pwrite(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(o);

    size_t written = 0;

    while (written < n) {
        com_write_one(_get_port_by_file((struct file *)dev->data), b[written]);
        written++;
    }

    return n;
}

static int _com_init_dev(struct vnode * rootdev, struct file * pipe_file, const char * name) {
    struct device * dev = (struct device *) kmalloc(sizeof(struct device));

    dev->d_block_size = 1; /* Unused */
    dev->probe = &com_probe;
    dev->pread = &com_pread;
    dev->pwrite = &com_pwrite;
    dev->data = pipe_file;

    return device_create(dev, name, rootdev);
}

int com_init(struct vnode * rootdev) {
    int err;

    /* Init pipes */
    com1_pipe = make_pipe(COM_PIPE_SIZE);
    com2_pipe = make_pipe(COM_PIPE_SIZE);
    com3_pipe = make_pipe(COM_PIPE_SIZE);
    com4_pipe = make_pipe(COM_PIPE_SIZE);

    com1_pipe_file = kmalloc(sizeof(struct file));
    com2_pipe_file = kmalloc(sizeof(struct file));
    com3_pipe_file = kmalloc(sizeof(struct file));
    com4_pipe_file = kmalloc(sizeof(struct file));

    memset(com1_pipe_file, 0, sizeof(struct file));
    memset(com2_pipe_file, 0, sizeof(struct file));
    memset(com3_pipe_file, 0, sizeof(struct file));
    memset(com4_pipe_file, 0, sizeof(struct file));

    vfop_open(com1_pipe, com1_pipe_file, O_RDWR);
    vfop_open(com2_pipe, com2_pipe_file, O_RDWR);
    vfop_open(com3_pipe, com3_pipe_file, O_RDWR);
    vfop_open(com4_pipe, com4_pipe_file, O_RDWR);

    /* Register devices */
    err = _com_init_dev(rootdev, com1_pipe_file, "ttyS0");
    if (err < 0)
        return err;

    err = _com_init_dev(rootdev, com2_pipe_file, "ttyS1");
    if (err < 0)
        return err;

    err = _com_init_dev(rootdev, com3_pipe_file, "ttyS2");
    if (err < 0)
        return err;

    err = _com_init_dev(rootdev, com4_pipe_file, "ttyS3");
    if (err < 0)
        return err;

    /* Init IRQ handlers */

    interrupt_set_handler(IRQ(COM1_IRQ), com_handler);
    interrupt_set_handler(IRQ(COM2_IRQ), com_handler);

    /* Set up ports - baud, parity, ... */

    _com_init_port(COM1_PORT); 
    _com_init_port(COM2_PORT); 
    _com_init_port(COM3_PORT); 
    _com_init_port(COM4_PORT); 

    return 0;
}

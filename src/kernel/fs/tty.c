#include <minios/kernel.h>
#include <minios/x86/portio.h>
#include <minios/x86/dt.h>
#include <minios/drv/vga.h>
#include <minios/drv/kbd.h>
#include <minios/fs/vfs.h>
#include <minios/fs/device.h>
#include <minios/fs/tty.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <ds/list.h>
#include <ds/rbuf.h>
#include <ds/buffer.h>
#include <string.h>

#include <sys/types.h>
#include <sys/io.h>

extern uint8_t   kbd_us_layout;
extern size_t    kbd_us_layout_size;

void tty_write_line(tty_t * tty) {
    /* Pass data from linebuffer to unread, to be read from the tty */
    rbuf_write(tty->unread, tty->linebuffer->b_buffer, tty->linebuffer->b_size);
    buffer_clear(tty->linebuffer);
}

void tty_handle_keycode(tty_t * tty, int code) {
    if (code == KBD_LCTRL) {
        tty->kbd_state |= TTY_KBD_CTRL;
        return;
    }

    if (code == -KBD_LCTRL) {
        tty->kbd_state &= ~TTY_KBD_CTRL;
        return;
    }

    if (code == KBD_LSHIFT) {
        tty->kbd_state |= TTY_KBD_LSHIFT;
        return;
    }

    if (code == -KBD_LSHIFT) {
        tty->kbd_state &= ~TTY_KBD_LSHIFT;
        return;
    }

    if (code == KBD_RSHIFT) {
        tty->kbd_state |= TTY_KBD_RSHIFT;
        return;
    }

    if (code == -KBD_RSHIFT) {
        tty->kbd_state &= ~TTY_KBD_RSHIFT;
        return;
    }

    if (code == KBD_CAPSLOCK) {
        tty->kbd_state ^= TTY_KBD_CAPSLOCK;
        return;
    }

    if (code == KBD_NUMLOCK) {
        tty->kbd_state ^= TTY_KBD_NUMLOCK;
        return;
    }

    if (code == KBD_SCROLLLOCK) {
        tty->kbd_state ^= TTY_KBD_SCROLLLOCK;
        return;
    }

    uint8_t modifiers = 0;
    if (tty->kbd_state & (TTY_KBD_LSHIFT | TTY_KBD_RSHIFT))
        modifiers |= 0x01;
    if (tty->kbd_state & TTY_KBD_CAPSLOCK)
        modifiers |= 0x02;

    if (code == KBD_ENTER) {
        tty_write_line(tty);
        /* echo mode on */
        char ch = {'\n'};
        vnputs(&ch, 1);
        return;
    } else {
        if (code < 0)
            return;

        if (code == KBD_BCKSPC) {
            /* pop from line buffer */
            if (buffer_pop(tty->linebuffer)) {
                char ch = {'\b'};
                vnputs(&ch, 1);
            }
            return;
        }

        uint32_t ucode = (uint32_t) code;

        if (tty->kbd_layout_size < (ucode * 4 + modifiers))
            return;

        char ch = tty->kbd_layout[ucode * 4 + modifiers];
        buffer_push(tty->linebuffer, ch);

        /* Echo mode on by default, gotta do termmode later */
        vnputs(&ch, 1);
    }
}

void tty_notify(tty_t * tty) {
    while (!kbd_empty()) {
        tty_handle_keycode(tty, kbd_dequeue());
    }
}

int tty_probe(struct device * dev, struct vnode * v) {
    UNUSED_VAR(dev);
    UNUSED_VAR(v);

    return 0;
}

ssize_t tty_pread(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(o);

    tty_t * tty = (tty_t *) dev->data;

    return rbuf_read(tty->unread, b, n);
}

ssize_t tty_pwrite(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(dev);
    UNUSED_VAR(o);

    vnputs(b, n);

    return n;
}

int tty_init(struct vnode * rootdev) {
    tty_t * tty = (tty_t *) kmalloc(sizeof(tty_t));
    tty->kbd_state = 0;

    tty->kbd_layout = &kbd_us_layout;
    tty->kbd_layout_size = kbd_us_layout_size;
    tty->linebuffer = buffer_create();
    tty->unread     = rbuf_create(TTY_BUFFER_SIZE);
    tty->fg_process = NULL;

    struct device * dev = (struct device *) kmalloc(sizeof(struct device));

    dev->probe = &tty_probe;
    dev->pread = &tty_pread;
    dev->pwrite = &tty_pwrite;

    dev->data = tty;

    device_create(dev, "tty", rootdev);

    kbd_setty(tty);

    return 0;
}

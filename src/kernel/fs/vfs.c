#include <minios/kernel.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/mm.h>
#include <minios/spinlock.h>
#include <minios/process.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>

/* VFS Tree Root */
struct vnode * vroot;

/* VFS Lock */
static spinlock_t vfs_lock;

/* nlink Lock */
static spinlock_t nlink_lock;

/* refcount Lock */
static spinlock_t refcount_lock;

/* Current Process */
extern struct process * current_process;

int vfs_mount (struct vnode * mount, struct vnode * root, void * device,
                int (*init)(struct vnode *, void *)) {

    ACQUIRE_LOCK(vfs_lock);
    int err = init(root, device);

    if (err) {
        goto release;
    }

    mount->v_mount = root;

    RELEASE_LOCK(vfs_lock);
    return 0;

release:
    RELEASE_LOCK(vfs_lock);
    return err;
}

int find_file(struct vnode * parent, struct vnode ** child, const char * name) {
    char * dname;

    size_t idx = 0;

    while (!vop_lookup(parent, idx, child, &dname)) {
        if (!strcmp(dname, name)) {
            return idx;
        }
        idx++;
    }

    if (child)
        *child = NULL;

    return -1;
}

static void _print_recursive_subtree(struct vnode * vn, int depth) {
    size_t idx = 2;
    int i;
    char * dname;
    struct vnode * child;

    while (!vop_lookup(vn, idx, &child, &dname)) {
        for (i = 0; i < depth; i++)
            klog (KDEBUG "\t");
        klog (KDEBUG "%s\n", dname);
        _print_recursive_subtree(child, depth + 1);

        idx++;
    }
}

void print_vfs_tree(struct vnode * start) {
    klog (KDEBUG "VFS Tree:\n");
    _print_recursive_subtree(start, 0);
}

/* _walk_path and _walk_path_parent are the weightlifters, doing most of the work, wrapped for the system calls nicely in walk_path, and walk_path_parent */
static struct vnode * _walk_path(struct vnode * start, char * path) {
    struct vnode * current = start, * child = NULL;

    size_t i, len = strlen(path), l = 0;

    for (i = 0; i < len && path[i] == '/'; i++);
    l = i;

    for (; i <= len; i++ ) {
        bool test = false;
        if (path[i] == '/') {
            test = true;
            path[i] = '\0';
            find_file(current, &child, path+l);
            l = i+1;
            path[i] = '/';
        }

        if (path[i] == '\0') {
            if (l != i) {
                test = true;
                find_file(current, &child, path+l);
            }
        }

        if (test) {
            if (!child)
                return NULL;
            current = child;
        }
    }

    return current;
}

static struct vnode * _walk_path_parent(struct vnode * start, char * path, size_t * fname_off_ptr) {
    ssize_t i = strlen(path) - 1;
    for (; path[i] != '/' && i >= 0; i--);

    if (fname_off_ptr)
        *fname_off_ptr = i+1;

    path[i] = '\0';
    struct vnode * parent = _walk_path(start, path);
    path[i] = '/';

    return parent;
}

/* Nice little wrappers for the vfs system calls */
struct vnode * walk_path(const char * path) {
    /* As we don't want to modify most of the strings passed as argument, we'll just temporarily allocate them on the kernel heap */

    char * kpath = (char *) kmalloc(sizeof(char) * strlen(path));
    strcpy(kpath, path);

    struct vnode * rvn = NULL; /* return vnode */

    if (current_process != NULL) {
        /* Multitasking initialized, we MUST use the current process's cwd/root */
        if (path[0] == '/') {
            /* Absolute path */
            rvn = _walk_path(current_process->p_root, kpath);
            kfree(kpath);
            return rvn;
        } else {
            rvn = _walk_path(current_process->p_cwd, kpath);
            kfree(kpath);
            return rvn;
        }
    }

    /* Multitasking isn't initialised, we must still be setting up our initial file system, pathwalking through /dev or something like that */

    /* As we don't have a CWD yet, any relative path is meaningless and will be treated as an absolute one. */
    rvn = _walk_path(vroot, kpath);
    kfree(kpath);
    return rvn;
}

struct vnode * walk_path_parent(const char * path, char ** filename) {
    /* Analogous to walk_path */

    char * kpath = (char *) kmalloc(sizeof(char) * strlen(path));
    strcpy(kpath, path);
    struct vnode * rvn = NULL;
    size_t filename_offset = 0;

    if (current_process != NULL) {
        if (path[0] == '/') {
            rvn = _walk_path_parent(current_process->p_root, kpath, &filename_offset);
            if (filename != NULL)
                *filename = (char *)path + filename_offset;
            kfree(kpath);
            return rvn;
        } else {
            rvn = _walk_path_parent(current_process->p_cwd, kpath, &filename_offset);
            if (filename != NULL)
                *filename = (char *)path + filename_offset;
            kfree(kpath);
            return rvn;
        }
    }

    rvn = _walk_path_parent(vroot, kpath, &filename_offset);
    if (filename != NULL)
        *filename = (char *)path + filename_offset;
    kfree(kpath);
    return rvn;
}

void inc_nlink(struct vnode * v) {
    ACQUIRE_LOCK(nlink_lock);
    v->v_nlink++;
    RELEASE_LOCK(nlink_lock);
}

void dec_nlink(struct vnode * v) {
    ACQUIRE_LOCK(nlink_lock);
    v->v_nlink--;
    if (!v->v_nlink)
        vop_delete(v);
    RELEASE_LOCK(nlink_lock);
}

void inc_refcount(struct vnode * v) {
    ACQUIRE_LOCK(refcount_lock);
    v->v_refcount++;
    RELEASE_LOCK(refcount_lock);
}

void dec_refcount(struct vnode * v) {
    ACQUIRE_LOCK(refcount_lock);
    v->v_refcount--;
    if (!v->v_refcount)
        vop_free(v);
    RELEASE_LOCK(refcount_lock);
}

struct file * kopen(char * path, int oflags) {
    struct file * f = (struct file *) kmalloc(sizeof(struct file));

    struct vnode * vn = walk_path(path);

    if (!vn) {
        if (oflags & O_CREAT) {
            char * filename;
            struct vnode * vp = walk_path_parent(path, &filename);

            if (vop_create(vp, filename, 0644) < 0)
                goto free;

            vn = walk_path(path);
        } else {
            goto free;
        }
    }

    vfop_open(vn, f, oflags);

    return f;

free:
    kfree(f);
    return NULL;
}

void kclose(struct file * f) {
    vfop_close(f->f_vn, f);
    kfree(f);
}

int kmkdir(char * path) {
    struct vnode * vn = walk_path(path);
    if (vn)
        return -1;

    char *filename;
    struct vnode * vp = walk_path_parent(path, &filename);
    return vop_mkdir(vp, filename, 0755);
}

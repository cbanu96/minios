#include <minios/kernel.h>
#include <minios/fs/vfs.h>
#include <minios/fs/device.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>

#define INITRD_MAX_ENTRIES 256
#define INITRD_MAX_DATA 65536
#define INITRD_MAGIC 0xDEADBEEF

extern addr_t initrd_begin;
extern size_t initrd_size;

static struct initrd_header {
    uint32_t magic;
    uint32_t entries;
    uint32_t data_begin;
} * header;

struct initrd_entry {
    uint32_t flag;
    char     path[64];
    uint32_t data_offset;
    uint32_t data_length;
};

static int initrd_load_entry(struct initrd_entry * e) {
    if (S_ISDIR(e->flag)) {
        int err = kmkdir(e->path);
        if (err < 0)
            return err;
    } else {
        struct file * f = kopen(e->path, O_CREAT);
        if (!f)
            return -1;

        char * file_data = (char *) (initrd_begin + header->data_begin + e->data_offset);

        int n = vfop_write(f, file_data, e->data_length);
        if (n < 0) {
            PANIC ("\nCouldn't write to %s: %d", e->path, -n);
        }

        kclose(f);
    }

    return 0;
}

int initrd_load() {
    header = (struct initrd_header *) initrd_begin;

    if (header->magic != INITRD_MAGIC)
        return -1;

    klog (KDEBUG "Loading initrd...");

    size_t i;
    for (i = 0; i < header->entries; i++ ) {
        struct initrd_entry * entry = (struct initrd_entry *) (initrd_begin + sizeof(struct initrd_header) + i * sizeof(struct initrd_entry));
        int err = initrd_load_entry(entry);

        if (err < 0)
            return -1;
    }

    klog (KDEBUG "\t\t\t\tDone!\n");

    return 0;
}

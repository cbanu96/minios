#include <minios/kernel.h>
#include <minios/fs/vfs.h>
#include <minios/fs/device.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>

int device_create(struct device * dev, const char * name, struct vnode * rootdev) {
    vop_mknod(rootdev, name, 0664, S_IFBLK);
    struct vnode * vdev;

    find_file(rootdev, &vdev, name);
    if (!vdev)
        return -1;

    vdev->v_fsdata = dev;

    return 0;
}

static int zero_probe(struct device * dev, struct vnode * v) {
    UNUSED_VAR(dev);
    UNUSED_VAR(v);
    return 0;
}

static ssize_t zero_pread(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(dev);
    UNUSED_VAR(o);
    size_t i;
    for ( i = 0; i < n; i++ ) {
        b[i] = '\0';
    }

    return n;
}

static ssize_t zero_pwrite(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(dev);
    UNUSED_VAR(b);
    UNUSED_VAR(n);
    UNUSED_VAR(o);
    return 0; /* always succeeds */
}

int zero_init(struct vnode * rootdev) {
    struct device * devzero = (struct device *) kmalloc(sizeof(struct device));
    devzero->d_block_size = 1;
    devzero->probe = &zero_probe;
    devzero->pread = &zero_pread;
    devzero->pwrite = &zero_pwrite;

    int err = device_create(devzero, "zero", rootdev);
    return err;
}

static int null_probe(struct device * dev, struct vnode * v) {
    UNUSED_VAR(dev);
    UNUSED_VAR(v);
    return 0;
}

static ssize_t null_pread(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(dev);
    UNUSED_VAR(b);
    UNUSED_VAR(n);
    UNUSED_VAR(o);
    return 0;
}

static ssize_t null_pwrite(struct device * dev, char * b, size_t n, off_t o) {
    UNUSED_VAR(dev);
    UNUSED_VAR(b);
    UNUSED_VAR(n);
    UNUSED_VAR(o);
    return 0; /* always succeeds */
}

int null_init(struct vnode * rootdev) {
    struct device * devnull = (struct device *) kmalloc(sizeof(struct device));

    devnull->d_block_size = 1;
    devnull->probe = &null_probe;
    devnull->pread = &null_pread;
    devnull->pwrite = &null_pwrite;

    int err = device_create(devnull, "null", rootdev);

    if (err < 0)
        kfree(devnull);
    return err;
}

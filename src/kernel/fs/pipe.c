#include <minios/kernel.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>
#include <minios/mm.h>
#include <minios/spinlock.h>

#include <ds/list.h>
#include <ds/rbuf.h>
#include <string.h>

#include <sys/types.h>

#define PIPE_BUFFER_SIZE    256
#define PIPEFS_DEV_NUMBER   3

/* A pipe has two ends, one for reading and one for writing.
 * For each of these ends, we will make a vnode to point to 
 * the following underlying data structure: */
struct pipe {
    rbuf_t * p_buf; /* Pipe buffer for reading/writing */
};

static int pipefs_get_free_inode() {
    static int last_ino = 1;
    return last_ino++;
}

static int pipe_delete(struct vnode *);
static int pipe_free(struct vnode *);
static int pipe_open(struct vnode *, struct file *);
static int pipe_close(struct vnode *, struct file *);
static ssize_t pipe_read(struct file *, char *, size_t);
static ssize_t pipe_write(struct file *, char *, size_t);

static struct vnode_ops pipe_ops = {
    .create  = NULL,
    .link    = NULL,
    .symlink = NULL,
    .mknod   = NULL,
    .mkdir   = NULL,
    .rename  = NULL,
    .rmdir   = NULL,
    .unlink  = NULL,
    .truncate = NULL,
    .get_size = NULL,
    .lookup  = NULL,
    .delete  = pipe_delete,
    .free    = pipe_free,
};

static struct file_ops pipe_fops = {
    .seek   = NULL,
    .open   = pipe_open,
    .close  = pipe_close,
    .read   = pipe_read,
    .write  = pipe_write,
    .pread  = NULL,
    .pwrite = NULL
};

static int pipe_delete(struct vnode * v) {
    struct pipe * pipe = (struct pipe *) v->v_fsdata;

    /* Check if the pipe's refcount is 0 */
    if (v->v_refcount) {
        return -EBUSY;
    }

    if (!pipe) {
        /* Already deleted */
        goto free;
    }

    /* There shouldn't be any need for spinlocking this portion of code, as the refcount = 0 */
    rbuf_destroy(pipe->p_buf);
    kfree(pipe);

free:
    kfree(v);

    return 0;
}

static int pipe_free(struct vnode * v) {
    return pipe_delete(v);
}

static int pipe_open(struct vnode * v, struct file * f) {
    struct pipe * pipe = (struct pipe *) v->v_fsdata;

    if (!pipe) {
        /* Pipe was closed already... */
        return -ENXIO;
    }

    f->f_vn = v;
    f->f_pos = 0; /* unused for pipes */

    inc_refcount(v);

    return 0;
}

static int pipe_close(struct vnode * v, struct file * f) {
    f->f_vn = NULL;
    f->f_pos = 0;

    dec_refcount(v);

    return 0;
}

static ssize_t pipe_read(struct file * f, char * buf, size_t num) {
    struct pipe * pipe = (struct pipe *) f->f_vn->v_fsdata;

    return rbuf_read(pipe->p_buf, buf, num);
}

static ssize_t pipe_write(struct file * f, char * buf, size_t num) {
    struct pipe * pipe = (struct pipe *) f->f_vn->v_fsdata;

    return rbuf_write(pipe->p_buf, buf, num);
}

struct vnode * make_pipe(size_t capacity) {
    struct pipe * underlying = (struct pipe *) kmalloc(sizeof(struct pipe));
    memset(underlying, 0, sizeof(struct pipe));

    underlying->p_buf = rbuf_create(capacity);
    
    /* allocate pipes */
    struct vnode * vn = (struct vnode *) kmalloc(sizeof(struct vnode));

    memset(vn, 0, sizeof(struct vnode));

    vn->v_dev = PIPEFS_DEV_NUMBER;
    vn->v_ino = pipefs_get_free_inode();

    vn->v_mode = S_IFIFO;

    vn->v_ops  = &pipe_ops;
    vn->v_fops = &pipe_fops;

    vn->v_fsdata = underlying;

    return vn;
}

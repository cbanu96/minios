#include <minios/kernel.h>
#include <minios/drv/vga.h>
#include <minios/x86/portio.h>
#include <sys/io.h>
#include <sys/types.h>

static volatile uint16_t *mem_ptr;
static uint8_t color;
static uint8_t clear_color;

static const uint8_t columns = 80;
static const uint8_t rows    = 25;
static uint16_t mem_size;

static int8_t crt_col;
static int8_t crt_row;

#define VGA_MEM_IDX(r, c) ((r) * columns + (c))

static uint8_t mkcolor(vga_color_t fg, vga_color_t bg) {
    return fg | (bg << 4);
}

static void setcolor(vga_color_t fg, vga_color_t bg) {
    color = mkcolor(fg, bg);
}

static void fill(uint8_t r1, uint8_t c1, uint8_t r2, uint8_t c2, char c, uint8_t clr) {
    uint8_t i, j;
    for ( i = r1; i <= r2; i++ ) {
        for ( j = c1; j <= c2; j++ ) {
            mem_ptr[VGA_MEM_IDX(i, j)] = (clr << 8) | (c);
        }
    }
}

static void clrscr() {
    fill(0, 0, rows - 1, columns - 1, ' ', clear_color);
}

static void scroll() {
    uint8_t i, j;
    for ( i = 0; i < rows - 1; i++ ) {
        for ( j = 0; j < columns; j++ ) {
            mem_ptr[VGA_MEM_IDX(i, j)] = mem_ptr[VGA_MEM_IDX(i+1, j)];
        }
    }

    fill(rows - 1, 0, rows - 1, columns - 1, ' ', clear_color);
}

static void printchar(char x) {
    uint8_t spaces = 0;
    /* We need to handle special characters, such as BACKSPACE, TAB and NEWLINE */
    switch (x) {
        case 8:  /* BS (BACKSPACE) */
            if (crt_col)
                crt_col--;
            else
            if (crt_row)
                crt_row--;

            if (crt_col < 0 && crt_row > 0)
                crt_col = columns, crt_row--;

            mem_ptr[VGA_MEM_IDX(crt_row, crt_col)] = (clear_color << 8) | ' ';
            break;
        case 9:  /* TAB */
            /* Print recursively n spaces, where n is the number
             * of spaces till the next multiple of 8 */

            spaces = 8 - (crt_col & 7);
            while (spaces-- && crt_col <= columns) {
                printchar(' ');
            }

            break;
        case 10: /* LF (NEWLINE) */
            crt_col = 0;
            crt_row++;
            if (crt_row >= rows) {
                scroll();
                crt_row = rows - 1;
            }

            break;
        default:
            if (x < 0x20) {
                /* If they weren't handled earlier, stop. */
                return;
            }

            mem_ptr[VGA_MEM_IDX(crt_row, crt_col)] = (color << 8) | x;

            crt_col++;

            if (crt_col >= columns) {
                crt_col = 0;
                crt_row++;
            }

            if (crt_row >= rows) {
                scroll();
                crt_row = rows - 1;
            }

            break;
    }
}

static void setcursorpos(uint8_t row, uint8_t col) {
    uint16_t pos = VGA_MEM_IDX(row, col);
    outb(VGA_BASE_PORT, 0x0F);
    outb(VGA_BASE_PORT_DATA, (uint8_t)(pos & 0xFF));
    outb(VGA_BASE_PORT, 0x0E);
    outb(VGA_BASE_PORT_DATA, (uint8_t)((pos >> 8) & 0xFF));
}

void vputs(const char* x) {
    uint32_t flags = read_eflags();
    cli();
    while (*x != '\0')
        printchar(*x++);

    if (flags & (1 << 9))
        sti();
}

void vnputs(char * str, size_t n) {
    uint32_t flags = read_eflags();
    cli();
    while (n--)
        printchar(*str++);

    if (flags & (1 << 9))
        sti();
}

void vsetcolor(vga_color_t fg, vga_color_t bg) {
    setcolor(fg, bg);
}

void vgainit(void) {
    /*
     * Text-mode:
     *
     * The video memory is mapped usually at 0xB8000,
     * and the screen is 80x25 with 16 colors
     */

    mem_ptr = (uint16_t *) VGA_FRAMEBUFFER_ADDR;
    mem_size = sizeof(uint16_t) * columns * rows;

    crt_col = crt_row = 0;
    color = clear_color = mkcolor(VGA_CLR_LIGHT_GREY, VGA_CLR_BLACK);
    clrscr();

    /* Hide cursor position by moving it out of the 25x80 area */
    setcursorpos(0xFF, 0xFF);
}

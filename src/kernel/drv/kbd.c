#include <minios/kernel.h>
#include <minios/x86/portio.h>
#include <minios/x86/dt.h>
#include <minios/fs/vfs.h>
#include <minios/fs/tty.h>
#include <minios/fs/device.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <minios/drv/kbd.h>

#include <ds/list.h>
#include <ds/rbuf.h>
#include <ds/tree.h>
#include <string.h>

#include <sys/types.h>
#include <sys/io.h>

/* KB US Layout */

const uint8_t kbd_us_layout[] = {
    0, 0, 0, 0,
    0, 0, 0, 0, /* KBD_ESC */
    '1', '!', '1', '!',
    '2', '@', '2', '@',
	'3', '#', '3', '#',
	'4', '$', '4', '$',
	'5', '%', '5', '%',
	'6', '^', '6', '^',
	'7', '&', '7', '&',
	'8', '*', '8', '*',
	'9', '(', '9', '(',
	'0', ')', '0', ')',
	'-', '_', '-', '_',
	'=', '+', '=', '+',
	'\b', '\b', '\b', '\b',
	'\t', '\t', '\t', '\t',
	'q', 'Q', 'Q', 'q',
	'w', 'W', 'W', 'w',
	'e', 'E', 'E', 'e',
	'r', 'R', 'R', 'r',
	't', 'T', 'T', 't',
	'y', 'Y', 'Y', 'y',
	'u', 'U', 'U', 'u',
	'i', 'I', 'I', 'i',
	'o', 'O', 'O', 'o',
	'p', 'P', 'P', 'p',
	'[', '{', '[', '{',
	']', '}', ']', '}',
	'\n', '\n', '\n', '\n',
	0, 0, 0, 0, /* KBD_LCTRL */
	'a', 'A', 'A', 'a',
	's', 'S', 'S', 's',
	'd', 'D', 'D', 'd',
	'f', 'F', 'F', 'f',
	'g', 'G', 'G', 'g',
	'h', 'H', 'H', 'h',
	'j', 'J', 'J', 'j',
	'k', 'K', 'K', 'k',
	'l', 'L', 'L', 'l',
	';', ':', ';', ':',
	'\'', '"', '\'', '"',
	'`', '~', '`', '~',
	0, 0, 0, 0, /* KBD_LSHIFT */
	'\\', '|', '\\', '|',
	'z', 'Z', 'Z', 'z',
	'x', 'X', 'X', 'x',
	'c', 'C', 'C', 'c',
	'v', 'V', 'V', 'v',
	'b', 'B', 'B', 'b',
	'n', 'N', 'N', 'n',
	'm', 'M', 'M', 'm',
	',', '<', ',', '<',
	'.', '>', '.', '>',
	'/', '?', '/', '?',
	0, 0, 0, 0, /* KBD_RSHIFT */
	'*', '*', '*', '*',
	0, 0, 0, 0, /* KBD_LALT */
	' ', ' ', ' ', ' ',
	0, 0, 0, 0, /* KBD_CAPSLOCK */
	0, 0, 0, 0, /* KBD_F1 */
	0, 0, 0, 0, /* KBD_F2 */
	0, 0, 0, 0, /* KBD_F3 */
	0, 0, 0, 0, /* KBD_F4 */
	0, 0, 0, 0, /* KBD_F5 */
	0, 0, 0, 0, /* KBD_F6 */
	0, 0, 0, 0, /* KBD_F7 */
	0, 0, 0, 0, /* KBD_F8 */
	0, 0, 0, 0, /* KBD_F9 */
	0, 0, 0, 0, /* KBD_F10 */
	0, 0, 0, 0, /* KBD_NUMLOCK */
	0, 0, 0, 0, /* KBD_SCROLLLOCK */
	0, 0, 0, 0, /* KBD_KPAD7 */
	0, 0, 0, 0, /* KBD_KPAD8 */
	0, 0, 0, 0, /* KBD_KPAD9 */
	'-', '-', '-', '-',
	0, 0, 0, 0, /* KBD_KPAD4 */
	0, 0, 0, 0, /* KBD_KPAD5 */
	0, 0, 0, 0, /* KBD_KPAD6 */
	'+', '+', '+', '+'
};
size_t kbd_us_layout_size = 316;

static bool kbd_escaped = false;
static uint8_t leds = 0;

static list_t * keys;
static tty_t * kbd_tty;

void kbd_enqueue(int keycode) {
    /* Make sure that our OS won't get crashed by simply getting spammed
     * on the keyboard, without anybody picking up or discarding the
     * characters.
     */
    if (keys->length < KBD_BUFFER_SIZE)
        list_push(keys, (void *) keycode);
}

int kbd_dequeue() {
    return (int) list_pop_head(keys);
}

bool kbd_empty() {
    return keys->length == 0;
}

int kbd_decode(uint8_t scancode) {
    if (scancode == KBD_SEQESCP) {
        kbd_escaped = true;
        return 0;
    }

    int decoded = 0;

    if (kbd_escaped) {
        decoded = (scancode & 0x7F) + 0x80;
    } else {
        decoded = scancode & 0x7F;
    }

    if (scancode & 0x80) {
        /* released = -pressed */
        decoded = -decoded;
    }

    kbd_escaped = false;

    return decoded;
}

void kbd_set_leds(uint8_t val) {
    while (inb(PS2_REGISTER_PORT) & 0x02);
    outb(PS2_REGISTER_PORT, 0xED);
    while (inb(PS2_REGISTER_PORT) & 0x02);
    outb(PS2_REGISTER_PORT, val);

    leds = val;
}

void kbd_handler(struct interrupt_registers * registers) {
    UNUSED_VAR(registers);

    while (inb(PS2_REGISTER_PORT) & 0x02);
    int value = kbd_decode(inb(PS2_DATA_PORT));

    if (value == 0) {
        irq_send_eoi(IRQ(1));
        return;
    }

    uint8_t _leds = leds;

    if (value == KBD_CAPSLOCK)
        _leds ^= LED_CAPSLOCK;

    if (value == KBD_SCROLLLOCK)
        _leds ^= LED_SCROLLLOCK;

    if (value == KBD_NUMLOCK)
        _leds ^= LED_NUMLOCK;

    if (leds != _leds)
        kbd_set_leds(_leds);

    kbd_enqueue(value);

    tty_notify(kbd_tty);

    irq_send_eoi(IRQ(1));
}

void kbd_setty(tty_t * tty) {
    kbd_tty = tty;
}

int kbd_init() {
    /* TODO: initializing ps2 properly. For now we rely on QEMU and GRUB to do the right thing. */
    keys = list_create();

    interrupt_set_handler(IRQ(1), kbd_handler);

    return 0;
}

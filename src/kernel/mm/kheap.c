#include <minios/kernel.h>
#include <minios/assert.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <sys/types.h>

#include <boot/multiboot.h>

/*
 * The heap will be made of blocks and holes.
 * Each of these will contain an identifying magic number in the header,
 * the size of the block/hole, and a pointer to the header of the block/hole.
 */


typedef struct {
    uint32_t magic;
#define HEADER_MAGIC    0xFEBB14C0
    uint32_t size;
    uint32_t type;          /* Make this struct 12-bytes, so all addresses returned by
                             * kmalloc() are 4-byte aligned */
#define HEADER_BLOCK    0x1
#define HEADER_HOLE     0x2
} __attribute__((packed)) header_t;

typedef struct {
    header_t *header;       /* Pointer to header of this hole/block */
} __attribute__((packed)) footer_t;


static uint32_t kernel_heap_start_addr;
static uint32_t kernel_heap_end_addr;
static uint32_t kernel_heap_length;
static uint32_t kernel_heap_min_length;
static uint32_t kernel_heap_max_length;

static header_t * next_header(header_t * header) {
    uint32_t next_addr = (uint32_t) header + header->size + sizeof(header_t) + sizeof(footer_t);

    if (next_addr >= kernel_heap_end_addr)
        return NULL;

    return (header_t *)next_addr;
}

static header_t * prev_header(header_t * header) {
    uint32_t prev_footer_addr = (uint32_t) header - sizeof(footer_t);
    if (prev_footer_addr < kernel_heap_start_addr)
        return NULL;

    return ((footer_t *)prev_footer_addr)->header;
}

static uint32_t kheap_slice(header_t *header, uint32_t n) {

    if (header->type == HEADER_BLOCK) {
        /* This header is already in use */
        return (uint32_t) NULL;
    }

    if (n > header->size) {
        /* Not enough bytes in this header */
        return (uint32_t) NULL;
    }

    if (n + sizeof(header_t) + sizeof(footer_t) >= header->size) {
        /* We splice this hole and there isn't enough left to make another hole... */
        header->type = HEADER_BLOCK;
        footer_t *footer = (footer_t *)((uint32_t) header + sizeof(header_t) + header->size);
        footer->header = header;

        return (uint32_t) header + sizeof(header_t); /* We'll just give everything... who cares? */
    }


    footer_t *old = (footer_t *)((uint32_t) header + sizeof(header_t) + header->size);

    header->type = HEADER_BLOCK;
    header->size = n;
    ((footer_t *)((uint32_t) header + sizeof(header_t) + header->size))->header = header;

    header_t *hole = (header_t *)((uint32_t) header + sizeof(header_t) + header->size + sizeof(footer_t));
    hole->magic = HEADER_MAGIC;
    hole->type  = HEADER_HOLE;
    hole->size  = (uint32_t) old - (uint32_t) hole - sizeof(header_t);
    old->header = hole;


    return (uint32_t)header + sizeof(header_t);
}

static void kheap_merge(header_t *header) {
    /* Get a contiguous chunk of holes, identified by the leftmost and rightmost holes,
     * and merge everything together. */
    header_t *left = header, *tmp;
    while ( (tmp = prev_header(left)) != NULL ) {
        if (tmp->type != HEADER_HOLE)
            break;
        left = tmp;
    }

    header_t *right = header;
    while ( (tmp = next_header(right)) != NULL ) {
        if (tmp->type != HEADER_HOLE)
            break;
        right = tmp;
    }

    if (left == right) /* Can't merge with anything */
        return;

    /* Merge by setting the size of the left header and updating the right footer */
    footer_t *footer = (footer_t *)((uint32_t) right + sizeof(header_t) + right->size);
    left->size = (uint32_t) footer - (uint32_t) left - sizeof(header_t);
    footer->header = left;
}

static void kheap_extend() {
    if (kernel_heap_end_addr == kernel_heap_start_addr + kernel_heap_max_length) {
        PANIC("Out of memory.\n");
        return;
    }

    mm_alloc_page_kernel(kernel_heap_end_addr, NULL);

    /* create new hole */
    header_t *header = (header_t *) kernel_heap_end_addr;
    kernel_heap_end_addr += PAGE_SIZE;
    kernel_heap_length += PAGE_SIZE;
    footer_t *footer = (footer_t *) (kernel_heap_end_addr - sizeof(footer_t));

    header->magic = HEADER_MAGIC;
    header->type  = HEADER_HOLE;
    header->size  = PAGE_SIZE - sizeof(header_t) - sizeof(footer_t);
    footer->header = header;

    kheap_merge(header);
}

void * kmalloc(uint32_t bytes) {
    /* Go through all the headers and return a block with
     * enough size for 'bytes' bytes.
     * If none exist, kheap_extend() and kmalloc() again,
     * since we will now hopefully have a hole big enough.
     */

    header_t *header;

    if (kernel_heap_length == 0)
        kheap_extend();

    header = (header_t *) kernel_heap_start_addr;

    do {
        if (header == NULL)
            break;

        void *tmp = (void *)kheap_slice(header, bytes);
        if (tmp != NULL) {
            return tmp;
        }
    } while ((header = next_header(header)));


    /* Calculate the number of frames we must add to the heap
     * in order to be able to allocate the desired number of bytes */
    uint32_t extend_times = PAGE_ALIGN_UP(bytes) / PAGE_SIZE;
    while (extend_times--)
        kheap_extend();

    /* Recursively call kmalloc again, we'll find a hole this time */
    return kmalloc(bytes);
}

void * kmallocp(uint32_t bytes) {
    /* An easy way we can get a page-aligned kmalloc
     * in this implementation is quite straightforward:
     * Say we have to get n * PAGE_SIZE bytes page-aligned.
     * We will allocate (n+2) new pages,
     * split right before the second page and right after page_aligned_address + bytes,
     * so that the header + sizeof(header) is page-aligned.
     */

    header_t *first_h, *second_h, *third_h;
    footer_t *first_f, *second_f, *third_f;

    /* Check the existing holes for a hole which contains a page aligned address, has enough bytes, and can be split */
    header_t * header = (header_t *) kernel_heap_start_addr;

    do {
        if (header->type != HEADER_HOLE)
            continue;

        uint32_t data_begin = (uint32_t) header + sizeof(header_t);
        uint32_t data_end   = (uint32_t) header + sizeof(header_t) + header->size;
        uint32_t pa_addr = PAGE_ALIGN_UP(data_begin); /* First possible page aligned address after data_begin */
        if (pa_addr >= data_end) {
            continue;
        }

        uint32_t bytes_right = data_end - pa_addr;
        uint32_t bytes_left  = pa_addr - data_begin;
        if (bytes_right < bytes) {
            /* Not enough bytes to allocate */
            continue;
        }

        if (bytes_left < sizeof(header_t) + sizeof(footer_t) + 16) {
            /* Cannot make another hole with the minimum size of 16 bytes*/
            continue;
        }

        /* We can now surely splice this hole */
        first_h = (header_t *) header;
        first_f = (footer_t *) (pa_addr - sizeof(footer_t) - sizeof(header_t));
        second_h = (header_t *) (pa_addr - sizeof(header_t));
        second_f = (footer_t *) (pa_addr + bytes);
        third_h = (header_t *) (pa_addr + bytes + sizeof(footer_t));
        third_f = (footer_t *) data_end;

        first_h->size = bytes_left - sizeof(header_t) - sizeof(footer_t);
        first_f->header = first_h;

#if 0
        klog (KINFO "data_begin: 0x%x\n"
                    "data_end: 0x%x\n"
                    "pa_addr: 0x%x\n"
                    "bytes_left: %d\n"
                    "bytes_right: %d\n"
                    "first_h: 0x%x\n"
                    "first_f: 0x%x\n"
                    "second_h: 0x%x\n"
                    "second_f: 0x%x\n"
                    "third_h: 0x%x\n"
                    "third_f: 0x%x\n",
                    data_begin, data_end, pa_addr, bytes_left, bytes_right, first_h, first_f, second_h, second_f, third_h, third_f);
#endif

        /* Check if, after removing the necessary amount of bytes, another hole can form to the right of the requested block */
        if (bytes_right > bytes + sizeof(header_t) + 16) {
            second_h->magic = HEADER_MAGIC;
            second_h->size = bytes;
            second_h->type = HEADER_BLOCK;
            second_f->header = second_h;

            third_h->magic = HEADER_MAGIC;
            third_h->size = bytes_right - bytes - sizeof(header_t) - sizeof(footer_t);
            third_h->type = HEADER_HOLE;
            third_f->header = third_h;
        } else {
            /* second_h is paired with third_f, second_f and third_h arenpart of the contents of the requested block */
            second_h->magic = HEADER_MAGIC;
            second_h->size = bytes_right;
            second_h->type = HEADER_BLOCK;
            third_f->header = second_h;
        }

        return (void *) pa_addr;
    } while ((header = next_header(header)));


    /* Extend the heap until the last hole's size is greater than (N+2) * PAGE_SIZE */

    /* Even if the number of bytes to be allocated isn't page-aligned, we will assume it is. */
    uint32_t all_bytes = (bytes = PAGE_ALIGN_UP(bytes)) + 2 * PAGE_SIZE;

    footer_t * last_footer = (footer_t *) (kernel_heap_end_addr - sizeof(footer_t));
    header_t * last_header = (header_t *) (last_footer->header);

    while (last_header->size < all_bytes) {
        kheap_extend();

        last_footer = (footer_t *) (kernel_heap_end_addr - sizeof(footer_t));
        last_header = (header_t *) (last_footer->header);
    }

    uint32_t old_end_addr = (uint32_t) last_header;

    /* At this point, we have, AT MINIMUM, a (N+2) * PAGE_SIZE - h - f sized hole.
     * We must now determine a point to split this, which would be the old_end_address + PAGE_SIZE - h
     * The layout of the N+2 pages will be this:
     *  H___________________________________________________________F
     * Which will become this:
     *  H____HOLE____F H_______BLOCK________F H______HOLE___________F
     */


    first_h     = (header_t *) (old_end_addr);
    first_f     = (footer_t *) (old_end_addr + PAGE_SIZE - sizeof(header_t) - sizeof(footer_t));
    second_h    = (header_t *) (old_end_addr + PAGE_SIZE - sizeof(header_t));
    second_f    = (footer_t *) (old_end_addr + PAGE_SIZE + bytes);
    third_h     = (header_t *) (old_end_addr + PAGE_SIZE + bytes + sizeof(footer_t));
    third_f     = (footer_t *) (kernel_heap_end_addr - sizeof(footer_t));

#if 0
    klog (KINFO "first_h: 0x%x\n"
                "first_f: 0x%x\n"
                "second_h: 0x%x\n"
                "second_f: 0x%x\n"
                "third_h: 0x%x\n"
                "third_f: 0x%x\n",
                first_h, first_f, second_h, second_f, third_h, third_f);
#endif

    first_h->magic = HEADER_MAGIC;
    first_h->size  = PAGE_SIZE - sizeof(header_t) - sizeof(footer_t) - sizeof(header_t);
    first_h->type  = HEADER_HOLE;
    first_f->header = first_h;

    second_h->magic = HEADER_MAGIC;
    second_h->size  = bytes;
    second_h->type  = HEADER_BLOCK;
    second_f->header = second_h;

    third_h->magic = HEADER_MAGIC;
    third_h->size  = (uint32_t) third_f - (uint32_t) third_h - sizeof(header_t);
    third_h->type  = HEADER_HOLE;
    third_f->header = third_h;

    return (void *)((uint32_t) second_h + sizeof(header_t));
}

void kfree(void * ptr) {
    /* Go through all the headers until one starts
     * at the address addr - sizeof(header_t).
     * Free that one, merge it, done. */

    addr_t addr = (addr_t)ptr;

    ASSERT(addr >= kernel_heap_start_addr && addr <= kernel_heap_end_addr);

    header_t *header = (header_t *) kernel_heap_start_addr;
    do {
        if (header == NULL)
            break;

        if ((uint32_t)header == addr - sizeof(header_t)) {
            if (header->type == HEADER_HOLE) {
                PANIC("kfree()'ing an already kfree()'d memory block\n");
            }

            /* This is the one. */
            header->type = HEADER_HOLE;
            kheap_merge(header);
            return;
        }
    } while ((header = next_header(header)));

    PANIC("kfree()'ing an address that isn't the beginning of a block");
}

void kheapinit(uint32_t max_size) {
    /* Make sure that the start and the size are page aligned */
    kernel_heap_start_addr  = PAGE_ALIGN_DOWN(KERNEL_HEAP_BEGIN);
    kernel_heap_end_addr    = kernel_heap_start_addr;
    kernel_heap_length      = 0;
    kernel_heap_min_length  = KERNEL_HEAP_MIN_SIZE;
    kernel_heap_max_length  = PAGE_ALIGN_DOWN(max_size);

    /* Extend the heap until it has the required size */
    for(;kernel_heap_end_addr < kernel_heap_start_addr + kernel_heap_min_length;) {
        kheap_extend();
    }
}


void kheap_debug() {
    header_t *header = (header_t *) kernel_heap_start_addr;

    do {
        if (header == NULL) break;

        footer_t *footer = (footer_t *) ((uint32_t) header + sizeof(header_t) + header->size);
        klog (KDEBUG "[0x%x] ", footer);

        klog (KDEBUG "[%s] start 0x%x, size %d      %s\n",
                header->type == HEADER_HOLE ? "HOLE" : "BLOCK",
                (uint32_t) header + sizeof(header_t),
                header->size,
                (uint32_t) (footer->header) == (uint32_t) header ? "OK" : "CORRUPT");

    } while((header = next_header(header)));
}


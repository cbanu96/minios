#include <minios/kernel.h>
#include <minios/assert.h>
#include <minios/process.h>
#include <minios/x86/dt.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <sys/io.h>
#include <sys/types.h>
#include <string.h>

#include <boot/multiboot.h>


/* PDE/PTE flags */
#define         FLAG_4MB        0x80
#define         FLAG_4KB        0x00

#define         FLAG_KERNEL     0x00
#define         FLAG_USER       0x04

#define         FLAG_READONLY   0x00
#define         FLAG_READWRITE  0x02

#define         FLAG_NOTPRES    0x00
#define         FLAG_PRESENT    0x01

mem_stat_t mem_stat;

/* extern decls from phys.c and kheap.c */
extern void phys_mem_init();
extern void phys_mem_push_region(uint32_t base, uint32_t size);
extern void phys_mem_push(uint32_t addr);
extern uint32_t phys_mem_pop();

/* forward decls */
void pf_handler(struct interrupt_registers *regs);
static void print_mem_stats();
static void mm_higher_half_map(uint32_t);

void mminit(multiboot_info_t *mb_hdr) {
    klog (KINFO "Setting up memory management... ");
    /* First, since paging is already enabled,
     * we must set up a page fault handler ASAP */
    interrupt_set_handler(ISR(14), pf_handler);

    /* Make sure the kernel size doesn't exceed 4MB */
    uint32_t kernel_end = PAGE_ALIGN_UP(KERNEL_VIRT_END);
    uint32_t max_address = KERNEL_VMA + 0x00400000;

    if (max_address < kernel_end)
        klog (KWARN
            "Kernel size exceeds 4MB.\n"
            "The OS may not boot correctly.\n");

    /* Check that the memory map is available */
    ASSERTF(mb_hdr->flags & MULTIBOOT_INFO_MEM_MAP,
            "Memory map not made available by the bootloader. Halting...");

    /* Initialize memory statistics */
    memset(&mem_stat, 0, sizeof(mem_stat));

    /*
     * Give all available memory to the physical memory allocator,
     * except for the kernel and its modules.
     */
    phys_mem_init();

    /* Set the initrd_begin and initrd_size variables when parsing
     * the GRUB modules.
     */
    extern addr_t initrd_begin;
    extern size_t initrd_size;

    /* Read the memory map given by the bootloader */
    struct multiboot_mmap_entry *mmap_entry;

    for (mmap_entry = (struct multiboot_mmap_entry *) VIRT_ADDR(mb_hdr->mmap_addr);
         (uint32_t) mmap_entry < (uint32_t) VIRT_ADDR(mb_hdr->mmap_addr) + mb_hdr->mmap_length;
         mmap_entry = (struct multiboot_mmap_entry *) ((uint32_t) mmap_entry + mmap_entry->size + sizeof(mmap_entry->size))) {

        /* Skip over reserved memory */
        if (mmap_entry->type == MULTIBOOT_MEMORY_RESERVED)
            continue;

        uint32_t base_addr      =                 (uint32_t)(mmap_entry->addr & 0xFFFFFFFF);
        uint32_t length         = PAGE_ALIGN_DOWN((uint32_t)(mmap_entry->len  & 0xFFFFFFFF));

        mem_stat.total_memory += length; /* Count total available memory */

        uint32_t begin = base_addr;
        uint32_t end = base_addr + length;
        uint32_t curr_addr = begin;

        while (curr_addr < end) {
            uint32_t region_start = curr_addr;
            uint32_t region_end = end;


            /* Skip kernel */
            if (curr_addr < PHYS_ADDR(kernel_end)) {
                curr_addr = PHYS_ADDR(kernel_end);
                continue;
            }

            /* Skip initrd */
            if (mb_hdr->mods_count != 0) {
                addr_t initrd_end = initrd_begin + initrd_size;

                if (PHYS_ADDR(initrd_begin) <= curr_addr &&
                    curr_addr < PHYS_ADDR(initrd_end)) {
                    curr_addr = PHYS_ADDR(initrd_end);
                    continue;
                }

                if (region_start <= PHYS_ADDR(initrd_begin) &&
                    PHYS_ADDR(initrd_begin) < region_end) {
                    region_end = PHYS_ADDR(initrd_begin);
                }

                if (region_end <= region_start) {
                    break;
                }
            }

            /* OM NOM NOM */

            phys_mem_push_region(PAGE_ALIGN_UP(region_start),
                   PAGE_ALIGN_UP(region_end) - PAGE_ALIGN_UP(region_start));

            curr_addr = PAGE_ALIGN_UP(region_end);
        }
    }

    /* Higher half map the initrd, since it was not mapped in boot.S */
    addr_t initrd_page;
    for (initrd_page = initrd_begin; initrd_page < initrd_begin + initrd_size; initrd_page += PAGE_SIZE) {
        mm_higher_half_map(PHYS_ADDR(initrd_page));
    }

    /* Remove identity map */
    uint32_t begin = 0;
    while (begin <= PAGE_ALIGN_DOWN(PHYS_ADDR(kernel_end))) {
        mm_free_page(begin, false);
        begin += PAGE_SIZE;
    }

    /* Initialize the kernel heap.
     * Place it at address 0xD0000000
     * Give it a minimum of 16 MB and a maximum of TOTAL_MEMORY / 16, which, for 4GB is 256MB.
     */

    uint32_t kheap_size = mem_stat.total_memory;
    if (kheap_size < 0x01000000U)
        kheap_size = 0x01000000U; /* 16 MB */

    klog (KINFO "Kernel Heap...");
    kheapinit(kheap_size);

    klog (KINFO "\tDone!\n");
}

static __attribute__((unused)) void print_mem_stats() {
    klog(KDEBUG "\n"
            "Total memory:     %d KB\n"
            "Available memory: %d KB\n"
            "Used memory:      %d KB\n"
            "Free memory:      %d KB\n",
            mem_stat.total_memory >> 10,
            mem_stat.available_memory >> 10,
            mem_stat.used_memory >> 10,
            mem_stat.free_memory >> 10);
}

static void tlb_flush() {
    /* Flush the Translation Lookaside Buffer
     * by refreshing the address CR3 holds. */
    write_cr3(read_cr3());
}

static void update_pd(addr_t virt, addr_t phys, uint32_t flags) {
    uint32_t pde_idx = virt / PAGE_TABLE_SIZE;
    uint32_t pte_idx = (virt % PAGE_TABLE_SIZE) / PAGE_SIZE;

    /* Update PDE and PTE, using the fractal mapping */
    uint32_t *pd = (uint32_t *) PDE_ADDRESS;
    uint32_t *pt = (uint32_t *) (PTE_ADDRESS + PAGE_SIZE * pde_idx);

    /* Update the PTE only if the PDE is already present */
    if (pd[pde_idx] & FLAG_PRESENT) {
        if (pt[pte_idx] & FLAG_PRESENT) {
            PANIC("update_pd(0x%x): this page is already mapped.\n", virt);
        }

        pt[pte_idx] = phys | flags;
    } else {
        /* PDE isn't present, we need to allocate memory for the page table too */
        uint32_t pt_phys = phys_mem_pop();
        pd[pde_idx] = pt_phys | flags;

        /* Initialize the new page directory */
        uint16_t i = 0;

        for ( i = 0; i < 1024; i++ )
            pt[i] = 0;

        pt[pte_idx] = phys | flags;
    }

    tlb_flush();
}

void mm_higher_half_map(addr_t phys) {
    update_pd(VIRT_ADDR(phys), phys, FLAG_PRESENT | FLAG_KERNEL | FLAG_READWRITE);
}

void mm_alloc_page_kernel_phys(addr_t virt, addr_t phys) {
    update_pd(virt, phys, FLAG_PRESENT | FLAG_KERNEL | FLAG_READWRITE);
}

void mm_alloc_page_user_phys(addr_t virt, addr_t phys) {
    update_pd(virt, phys, FLAG_PRESENT | FLAG_USER | FLAG_READWRITE);
}

void mm_alloc_page_kernel(addr_t virt, addr_t *phys) {
    /* Obtain an unused page frame from our physical memory allocator */
    uint32_t phys_addr = phys_mem_pop();

    if (phys != NULL)
        *phys = phys_addr;

    update_pd(virt, phys_addr, FLAG_PRESENT | FLAG_KERNEL | FLAG_READWRITE);
}

void mm_alloc_page_user(addr_t virt, addr_t *phys) {
    uint32_t phys_addr = phys_mem_pop();

    if (phys != NULL)
        *phys = phys_addr;

    update_pd(virt, phys_addr, FLAG_PRESENT | FLAG_USER | FLAG_READWRITE);
}

void mm_free_page(addr_t virt, bool pass) {
    uint32_t pde_idx = virt / PAGE_TABLE_SIZE;
    uint32_t pte_idx = (virt % PAGE_TABLE_SIZE) / PAGE_SIZE;

    uint32_t *pd = (uint32_t *) PDE_ADDRESS;
    uint32_t *pt = (uint32_t *) (PTE_ADDRESS + PAGE_SIZE * pde_idx);

    if (pd[pde_idx] & FLAG_PRESENT) {
        uint32_t phys = PAGE_ALIGN_DOWN(pt[pte_idx]);

        if (pt[pte_idx] & FLAG_PRESENT) {
            pt[pte_idx] = 0;
        }

        /* Check if this PT is fully unmapped */
        uint16_t i;
        for (i = 0; i < 1024; i++) {
            if (pt[i] & FLAG_PRESENT)
                i = 1025;
        }

        if (i == 1024) {
            /* Free the space allocated for the page table */
            phys_mem_push(PAGE_ALIGN_DOWN(pd[pde_idx]));
            pd[pde_idx] = 0;
        }

        if (pass)
            phys_mem_push(phys); /* Pass the free'd block to the physical memory allocator */

        tlb_flush();
    }
}

void mm_free_pde(size_t idx, bool pass) {
    uint32_t *pd = (uint32_t *) PDE_ADDRESS;
    uint32_t *pt = (uint32_t *) (PTE_ADDRESS + PAGE_SIZE * idx);

    if (pd[idx] & FLAG_PRESENT) {
        uint16_t i = 0;
        for (i = 0; i < 1024; i++) {
            if (pt[i] & FLAG_PRESENT) {
                if (pass) {
                    phys_mem_push(PAGE_ALIGN_DOWN(pt[i]));
                }

                pt[i] = 0;
            }
        }

        if (pass)
            phys_mem_push(PAGE_ALIGN_DOWN(pd[idx]));

        pd[idx] = 0;
    }

    tlb_flush();
}

void pf_handler(struct interrupt_registers *regs) {
    unsigned long address = read_cr2();

    PANIC (KERROR "Page fault at address 0x%x->0x%x->0x%x\n"
                 "Flags: P %d, W %d, U %d, R %d, I %d\n",
                 address, address / PAGE_TABLE_SIZE, (address % PAGE_TABLE_SIZE) / PAGE_SIZE,
                 (regs->err_code & 1) > 0 ? 1 : 0,
                 (regs->err_code & 2) > 0 ? 1 : 0,
                 (regs->err_code & 4) > 0 ? 1 : 0,
                 (regs->err_code & 8) > 0 ? 1 : 0,
                 (regs->err_code & 16) > 0 ? 1 : 0);
}

/* Get the physical address using a virtual address and the physical address of the page directory.
 */
uint32_t __get_phys_addr(uint32_t cr3, uint32_t virt) {
    uint32_t old_cr3 = read_cr3();

    write_cr3(cr3);

    uint32_t pde_idx = virt / PAGE_TABLE_SIZE;
    uint32_t pte_idx = (virt % PAGE_TABLE_SIZE) / PAGE_SIZE;

    uint32_t *pd = (uint32_t *) PDE_ADDRESS;
    uint32_t *pt = (uint32_t *) (PTE_ADDRESS + PAGE_SIZE * pde_idx);

    uint32_t phys = 0;

    if (pd[pde_idx] & FLAG_PRESENT) {
        if (pt[pte_idx] & FLAG_PRESENT) {
            phys = PAGE_ALIGN_DOWN(pt[pte_idx]);
        }
    }

    write_cr3(old_cr3);

    return phys;
}

/* Checks whether the given pde is present. */
bool __check_pd_present(uint32_t pde) {
    uint32_t *pd = (uint32_t *) PDE_ADDRESS;

    return (pd[pde] & FLAG_PRESENT) > 0 ? true : false;
}

/* Checks whether the given pte is present. */
bool __check_pt_present(uint32_t pde, uint32_t pte) {
    uint32_t *pt = (uint32_t *) (PTE_ADDRESS + PAGE_SIZE * pde);
    
    if (!__check_pd_present(pde))
        return false;

    return (pt[pte] & FLAG_PRESENT) > 0 ? true : false;
}

/* Checks whether the given virtual address is present. */
bool __check_addr_present(addr_t virt) {
    uint32_t pde = virt / PAGE_TABLE_SIZE;
    uint32_t pte = (virt % PAGE_TABLE_SIZE) / PAGE_SIZE;

    return __check_pt_present(pde, pte);
}

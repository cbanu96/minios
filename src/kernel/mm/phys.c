#include <minios/kernel.h>
#include <minios/assert.h>
#include <minios/x86/dt.h>
#include <minios/log.h>
#include <minios/mm.h>

#include <sys/io.h>
#include <sys/types.h>


#define STACK_SIZE 0x400
#define NUM_STACKS 0x400
#define STACK_POS(x) ((x) % STACK_SIZE)
#define STACK_IDX(x) ((x) / STACK_SIZE)
#define STACK_VIRTUAL_ADDR  0xC0500000

/* Labels inside boot/boot.S */
extern void stack_list_phys();
extern void stack0();

static uint32_t **stack_list;
static uint32_t stack_used;
static uint32_t stack_reserved;

/* Memory usage statistics from mm/mm.c */
extern mem_stat_t mem_stat;

void phys_mem_init() {
    stack_list = (uint32_t **)&stack_list_phys;
    stack_list[0] = (uint32_t *)&stack0;
}

static inline void stack_new() {
    /* We can call mm_alloc_page() here because we already have 1024 entries in our current stack,
     * so we can use one of those to allocate a new stack, or two if we also need a page table entry.
     */
    uint32_t addr = STACK_VIRTUAL_ADDR + 4 * STACK_SIZE * STACK_IDX(stack_reserved);
    mm_alloc_page_kernel(addr, NULL);

    stack_list[STACK_IDX(stack_reserved)] = (uint32_t *)addr;
}

void phys_mem_push_region(uint32_t base, uint32_t size) {
    /* Align the beginning and the end of the pushed region */
    uint32_t addr = PAGE_ALIGN_DOWN(base);
    uint32_t end = PAGE_ALIGN_DOWN(base + size - addr);

    while ( addr < end ) {
        stack_list[STACK_IDX(stack_reserved)][STACK_POS(stack_reserved)] = addr;
        stack_reserved++;
        mem_stat.available_memory += PAGE_SIZE;
        mem_stat.free_memory += PAGE_SIZE;

        if (STACK_POS(stack_reserved) == 0) {
            /* We need a new stack, obviously. */
            stack_new();
        }

        addr += PAGE_SIZE;
    }
}

void phys_mem_push(uint32_t addr) {
    mem_stat.used_memory -= PAGE_SIZE;
    mem_stat.free_memory += PAGE_SIZE;
    stack_used--;
    stack_list[STACK_IDX(stack_used)][STACK_POS(stack_used)] = addr;
}

uint32_t phys_mem_pop() {
    uint32_t addr = stack_list[STACK_IDX(stack_used)][STACK_POS(stack_used)];
    stack_used++;
    mem_stat.used_memory += PAGE_SIZE;
    mem_stat.free_memory -= PAGE_SIZE;

    return addr;
}


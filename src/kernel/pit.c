#include <minios/kernel.h>
#include <minios/x86/pit.h>
#include <minios/x86/portio.h>

#include <sys/types.h>
#include <sys/io.h>

void pit_init(int freq) {
    int divider = PIT_BASE_FREQ / freq;

    /* Output to the PIT command register the following bits:
       0 - 0 ( 16-bit binary )
       1-3 - 011 ( mode 3 - square wave )
       4-5 - 11  ( low byte + high byte )
       6-7 - 00  ( channel 0 )
       0b00110110 = 0x36 
       */
    outb(PIT_CMD, 0x36);

    /* send low byte */
    outb(PIT_CH0, divider & 0xFF);

    /* send high byte */
    outb(PIT_CH0, (divider >> 8) & 0xFF);
}

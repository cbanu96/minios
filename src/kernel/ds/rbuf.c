#include <minios/kernel.h>
#include <minios/mm.h>
#include <minios/spinlock.h>
#include <minios/log.h>

#include <string.h>
#include <ds/rbuf.h>

/* Allocates and initializes a ring buffer */
rbuf_t * rbuf_create(size_t capacity) {
    /* Allocates enough memory for the data structure. */
    rbuf_t * rbuf = (rbuf_t *) kmalloc(sizeof(rbuf_t));

    memset(rbuf, 0, sizeof(rbuf_t));


    rbuf->r_capacity = capacity;

    /* Allocates the memory required for the underlying data buffer. */
    rbuf->r_buf  = (char *) kmalloc(sizeof(char) * capacity);

    return rbuf;
}

/* Frees a ring buffer */
void rbuf_destroy(rbuf_t * rbuf) {
    if (!rbuf)
        return;

    if (rbuf->r_buf)
        kfree(rbuf->r_buf);

    kfree(rbuf);
}

static bool rbuf_can_write(rbuf_t * rbuf) {
    bool result = (rbuf->r_size < rbuf->r_capacity);
    return result;
}

static bool rbuf_can_read(rbuf_t * rbuf) {
    bool result = (rbuf->r_size > 0);
    return result;
}

/*
 * Returns the position at which to write.
 */
inline static size_t rbuf_write_at(rbuf_t * rbuf) {
    int pos = rbuf->r_begin + rbuf->r_size;

    if (pos >= rbuf->r_capacity)
        pos -= rbuf->r_capacity;

    return (size_t) pos;
}

/*
 * Returns the position at which to read.
 */
inline static size_t rbuf_read_at(rbuf_t * rbuf) {
    return rbuf->r_begin;
}

static void rbuf_write_one(rbuf_t * rbuf, char byte) {
    rbuf->r_buf[rbuf_write_at(rbuf)] = byte;
    rbuf->r_size++;
}

static void rbuf_read_one(rbuf_t * rbuf, char * byte) {
    *byte = rbuf->r_buf[rbuf_read_at(rbuf)];
    rbuf->r_begin++;
    rbuf->r_size--;

    if (rbuf->r_begin == rbuf->r_capacity)
        rbuf->r_begin = 0;
}

/* Writes bytes from the given buffer to buf, until
 * either num characters are written, or the buffer gets full.
 */
size_t rbuf_write(rbuf_t * rbuf, char * buf, size_t num) {
    size_t written = 0;
    while (rbuf_can_write(rbuf) && written < num) {
        rbuf_write_one(rbuf, buf[written]);
        written++;
    }

    return written;
}

/* Reads bytes from the given buffer to buf, until
 * either num characters are read, or the buffer gets empty.
 */
size_t rbuf_read(rbuf_t * rbuf, char * buf, size_t num) {
    size_t read = 0;

    while (rbuf_can_read(rbuf) && read < num) {
        rbuf_read_one(rbuf, &buf[read]);
        read++;
    }

    return read;
}

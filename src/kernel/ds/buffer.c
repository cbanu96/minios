#include <minios/mm.h>

#include <ds/buffer.h>
#include <string.h>

buffer_t * buffer_create() {
    buffer_t * buf      = (buffer_t *) kmalloc(sizeof(buffer_t));

    buf->b_buffer       = (char *) kmalloc(sizeof(char) * BUFFER_INITIAL_SIZE);
    buf->b_size         = 0;
    buf->b_capacity     = BUFFER_INITIAL_SIZE;

    memset(buf->b_buffer, 0, sizeof(char) * BUFFER_INITIAL_SIZE);

    return buf;
}

void buffer_destroy(buffer_t * buf) {
    kfree(buf->b_buffer);
    kfree(buf);
}

static bool buffer_extend(buffer_t * buf) {
    if (buf->b_capacity == BUFFER_INITIAL_SIZE)
        return false;

    /* New buffer's size is twice the size of the old one. */
    char * tmp_buffer = (char *) kmalloc(sizeof(char) * 2 * buf->b_capacity);
    
    /* Zero the new one */
    memset(tmp_buffer, 0, sizeof(char) * 2 * buf->b_capacity);

    /* Copy the data from the old one into the new one */
    memcpy(tmp_buffer, buf->b_buffer, sizeof(char) * buf->b_capacity);

    /* Free memory used by the old one */
    kfree(buf->b_buffer);

    /* Point the buffer_t structure to the new buffer */
    buf->b_buffer = tmp_buffer;
    buf->b_capacity = 2 * buf->b_capacity;

    return true;
}

void buffer_clear(buffer_t * buf) {
    char * tmp_buffer = (char *) kmalloc(sizeof(char) * BUFFER_INITIAL_SIZE);
    memset(tmp_buffer, 0, sizeof(char) * BUFFER_INITIAL_SIZE);
    buf->b_capacity = BUFFER_INITIAL_SIZE;
    buf->b_size = 0;
    kfree(buf->b_buffer);
    buf->b_buffer = tmp_buffer;
}

bool buffer_push(buffer_t * buf, char byte) {
    if (buf->b_size == buf->b_capacity) {
        if (!buffer_extend(buf))
            return false;
    }

    buf->b_buffer[buf->b_size++] = byte;
    return true;
}

bool buffer_pop(buffer_t * buf) {
    if (buf->b_size > 0) {
        --buf->b_size;
        return true;
    }

    return false;
}

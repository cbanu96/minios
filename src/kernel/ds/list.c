#include <minios/mm.h>

#include <ds/list.h>

list_t * list_create() {
    list_t * ret = (list_t *) kmalloc(sizeof(list_t));
    ret->head = ret->tail = NULL;
    ret->length = 0;

    return ret;
}

void list_free_val(list_t * list) {
    list_node_t *it;

    list_foreach(it, list) {
        kfree(it->value);
    }
}

void list_free(list_t * list) {
    list_node_t *it = list->head;

    while(it) {
        list_node_t *next = it->next;
        kfree(it);
        it = next;
    }

    kfree(list);
}

void list_insert(list_t * list, list_node_t * node) {
    node->next = NULL;
    node->list = list;

    if (list->length == 0) {
        list->head = list->tail = node;
        node->prev = NULL;
    } else {
        node->prev = list->tail;
        list->tail->next = node;
        list->tail = node;
    }

    list->length++;
}

list_node_t * list_push(list_t * list, void * ptr) {
    list_node_t * node = (list_node_t *) kmalloc(sizeof(list_node_t));

    node->value = ptr;
    node->next = node->prev = node->list = NULL;

    list_insert(list, node);

    return node;
}

void list_delete(list_t * list, list_node_t * node) {
    if (!node)
        return;

    if (list->head == node) {
        list->head = node->next;
    }

    if (list->tail == node) {
        list->tail = node->prev;
    }

    if (node->prev) {
        node->prev->next = node->next;
    }

    if (node->next) {
        node->next->prev = node->prev;
    }

    node->prev = node->next = node->list = NULL;
    list->length--;

    kfree(node);
}

void * list_pop_head(list_t * list) {
    list_node_t * node = list->head;
    if (list->head == NULL)
        return NULL;

    void * value = node->value;
    list_delete(list, node);
    return value;
}

void * list_pop_tail(list_t * list) {
    list_node_t * node = list->tail;
    if (list->tail == NULL)
        return NULL;

    void * value = node->value;
    list_delete(list, node);
    return value;
}

list_node_t * list_find(list_t * list, void * value) {
    list_node_t *it;

    list_foreach(it, list) {
        if (it->value == value) {
            return it;
        }
    }

    return NULL;
}

list_node_t * list_find_idx(list_t * list, size_t idx) {
    list_node_t *it;
    size_t i = 0;

    list_foreach(it, list) {
        if (i == idx)
            return it;

        i++;
    }
    
    return NULL;
}

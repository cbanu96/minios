#include <minios/mm.h>

#include <ds/tree.h>

tree_t * tree_create() {
    tree_t * tree = kmalloc(sizeof(tree_t));
    tree->root = NULL;

    return tree;
}

void tree_root(tree_t * tree, tree_node_t * root) {
    tree->root = root;
}

void tree_free_val(tree_t * tree) {
    tree_node_free_val(tree->root);
}

void tree_free(tree_t * tree) {
    tree_node_free(tree->root);
    kfree(tree);
}

tree_node_t * tree_node_create(void * value) {
    tree_node_t * node = (tree_node_t *) kmalloc(sizeof(tree_node_t));

    node->parent = NULL;
    node->children = (list_t *) list_create();
    node->value = value;

    return node;
}

void tree_node_append_child(tree_node_t * parent, tree_node_t * child) {
    list_push(parent->children, child);
    child->parent = parent;
}

void tree_node_delete(tree_node_t * node) {
    /* Make every child point to this node's parent.
     * I.e.
     *       1              1
     *      / \  delete(2) / \
     *     2   3    =>    4   3
     *    /
     *   4
     */

    if (node == NULL) return;

    tree_node_t *parent = node->parent;

    if (parent == NULL) {
        /* root node, just stop */
        return;
    }

    list_node_t *it;
    list_foreach(it, node->children) {
        tree_node_t * child = (tree_node_t *)(it->value);

        child->parent = parent;
        list_push(parent->children, child);
    }

    list_delete(parent->children, list_find(parent->children, node));

    list_free(node->children);
    kfree(node);
}

void tree_node_free_val(tree_node_t * node) {
    kfree(node->value);

    list_node_t *it;
    list_foreach(it, node->children) {
        tree_node_t *child = (tree_node_t *)(it->value);

        tree_node_free_val(child);
    }
}

void tree_node_free(tree_node_t * node) {
    list_node_t *it;
    list_foreach(it, node->children) {
        tree_node_t *child = (tree_node_t *)(it->value);

        tree_node_free(child);
    }

    list_free(node->children);
    kfree(node);
}

tree_node_t * tree_node_find(tree_node_t * node, void * value) {
    if (node->value == value)
        return node;

    list_node_t *it;
    list_foreach(it, node->children) {
        tree_node_t *child = (tree_node_t *)(it->value);
        tree_node_t *needle = tree_node_find(child, value);
        if (needle != NULL)
            return needle;
    }

    return NULL;
}

tree_node_t * tree_find(tree_t * tree, void * value) {
    return tree_node_find(tree->root, value);
}


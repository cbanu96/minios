#include <minios/kernel.h>
#include <minios/version.h>
#include <minios/drv/kbd.h>
#include <minios/drv/vga.h>
#include <minios/x86/dt.h>
#include <minios/fs/vfs.h>
#include <minios/fs/device.h>
#include <minios/process.h>
#include <minios/syscall.h>
#include <minios/mm.h>
#include <minios/log.h>
#include <minios/assert.h>

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno_defs.h>

#include <boot/multiboot.h>

#include <ds/list.h>
#include <ds/tree.h>
#include <ds/rbuf.h>

addr_t initrd_begin;
size_t initrd_size;

#ifdef DEBUG
static void test_list() {
    klog(KDEBUG "Testing <ds/list.h>\n");
    
    list_t *list = list_create();
    list_node_t *ln0 = list_push(list, 1);
    list_node_t *ln1 = list_push(list, 2);
    list_node_t *ln2 = list_push(list, 3);
    list_node_t *ln3 = list_push(list, 4);

    klog(KDEBUG "list_foreach(): ");
    list_node_t *it;
    list_foreach(it, list) {
        klog(KDEBUG "%d ", it->value);
    }
    klog(KDEBUG "\n");

    klog(KDEBUG "list_delete([2]);\n");
    list_delete(list, ln2);

    klog(KDEBUG "list_foreach(): ");
    list_foreach(it, list) {
        klog(KDEBUG "%d ", it->value);
    }
    klog(KDEBUG "\n");

    klog(KDEBUG "list_pop_head() = %d\n", list_pop_head(list));
    klog(KDEBUG "list_pop_tail() = %d\n", list_pop_tail(list));

    klog(KDEBUG "list_find(2) = %d\n", list_find(list, 2));

    list_free(list);
}

static void tree_print(tree_node_t * node) {
    /* Tree linearization */
    list_node_t *it;

    klog(KDEBUG "%d ", node->value);
    list_foreach(it, node->children) {
        tree_node_t *child = (tree_node_t *)(it->value);

        tree_print(child);
        klog(KDEBUG "%d ", node->value);
    }
}

static void test_tree() {
    klog(KDEBUG "Testing <ds/tree.h>\n");
    tree_t *tree = tree_create();
    tree_root(tree, tree_node_create(100));
    
    tree_node_append_child(tree->root, tree_node_create(1));
    tree_node_append_child(tree->root, tree_node_create(2));
    tree_node_t *t1 = tree_node_create(3);
    tree_node_append_child(tree->root, t1);
    tree_node_append_child(t1, tree_node_create(4));

    tree_print(tree->root); klog(KDEBUG "\n");
    tree_node_delete(t1);

    tree_print(tree->root); klog(KDEBUG "\n");

    tree_node_delete(tree->root->children->head->value);
    tree_print(tree->root); klog(KDEBUG "\n");

    tree_node_delete(tree_find(tree, 4));
    tree_print(tree->root); klog(KDEBUG "\n");

    tree_free(tree);
}

static void test_buffer() {
    klog (KDEBUG "Testing <ds/rbuf.h>\n");
    rbuf_t * rbuf = rbuf_create(15);
    
    char byte[] = "123456789012345678";

    int num = rbuf_write(rbuf, byte, 15);
    klog (KDEBUG "%d\n", num);

    char read[16];

    rbuf_read(rbuf, read, 10);

    rbuf_write(rbuf, byte+3, 10);

    rbuf_read(rbuf, read, 5);
    read[15] = '\0';
    klog (KDEBUG "%s\n", read);

    BREAKPOINT();
}
#endif

void kinit(uint32_t magic, multiboot_info_t *mb_hdr, uint32_t initial_esp) {
    /* Initialize the screen */
    vgainit();

    /* Check multiboot information */
    ASSERTF(magic == MULTIBOOT_BOOTLOADER_MAGIC,
        "Invalid magic constant in EAX at kinit().\n"
        "Are you sure you are using a multiboot-compliant\n"
        "bootloader?");

    ASSERTF(mb_hdr != NULL,
        "Invalid pointer to multiboot-header structure in EBX at kinit().\n"
        "Are you sure you are using a multiboot-compliant\n"
        "bootloader?");

    /* Enable "early" logging for messages */
    klogearly(true);

    /* Set up the descriptor tables */
    dtinit(initial_esp);

    /* Parse GRUB modules */
    uint32_t * mods = (uint32_t *)(VIRT_ADDR(mb_hdr->mods_addr));
    if (mb_hdr->mods_count == 0) {
        PANIC("No initrd present!\n");
    }

    initrd_begin = VIRT_ADDR(mods[0]);
    initrd_size  = mods[1] - mods[0];

    /* Initialize memory management */
    mminit(mb_hdr);

#if 0 /* Test kmallocp() */
    void * ptr1 = kmallocp(4096);

    klog (KINFO "-----\n");
    kheap_debug();

    void * ptr2 = kmallocp(4096);

    klog (KINFO "-----\n");
    kheap_debug();

    void * ptr3 = kmallocp(4096);

    klog (KINFO "-----\n");
    kheap_debug();

    void * ptr4 = kmallocp(4096);

    klog (KINFO "-----\n");
    kheap_debug();

    kfree(ptr1);
    kfree(ptr2);
    kfree(ptr3);

    klog (KINFO "-----\n");
    kheap_debug();

    ptr1 = kmallocp(4096);

    kfree(ptr4);
    klog (KINFO "-----\n");
    kheap_debug();

    kfree (ptr1);
    klog (KINFO "-----\n");
    kheap_debug();

    BREAKPOINT();
#endif

#ifdef DEBUG
    /* Test data structures */
    test_list();
    test_tree();
    test_buffer();
#endif

    extern struct vnode * vroot;

    /* Initialize root vnode */
    vroot = (struct vnode *) kmalloc(sizeof(struct vnode));
    vroot->v_nlink = 1; /* init it with 1, to prevent it from EVER being deleted */
    if (ramfs_init(vroot, NULL))
        PANIC ("\nCouldn't init ramfs on / ...\n");
    /* link . and .. to the vroot, prevent escaping the FS */
    ramfs_dir_add_child(vroot, vroot, ".");
    ramfs_dir_add_child(vroot, vroot, "..");

    if (initrd_load())
        PANIC ("\nCouldn't parse initrd...\n");

    /* Mount devfs on /dev */
    if (find_file(vroot, NULL, "dev") < 0) /* /dev not present in initrd */ {
        vop_mkdir(vroot, "dev", 0755);
    }

    struct vnode * devfsroot = (struct vnode *) kmalloc(sizeof(struct vnode));
    vfs_mount(walk_path("dev"), devfsroot, NULL, devfs_init);


#ifdef DEBUG
    print_vfs_tree(vroot);
#endif

    klog(KINFO "Initializing devices...");

    /* Load devices */
    if (zero_init(devfsroot) < 0)
        PANIC("\nCouldn't initialize /dev/zero\n");

    if (null_init(devfsroot) < 0)
        PANIC("\nCouldn't initialize /dev/null\n");

    if (com_init(devfsroot) < 0)
        PANIC("\nCouldn't initialize serial ports\n");

    if (kbd_init() < 0)
        PANIC("\nCouldn't initialize keyboard\n");

    if (tty_init(devfsroot) < 0)
        PANIC("\nCouldn't initialize /dev/tty\n");

    klog(KINFO "\t\t\t\tDone!\n");

    /* Init multitasking */
    klog(KINFO "Making the jump to multitasking...");

    /* We have serial ports now, COM1 is used for debugging */
    klogoutput(kopen("/dev/ttyS0", O_RDWR));
    klogearly(false);

    schedinit();
}

#include <minios/kernel.h>
#include <minios/x86/dt.h>
#include <minios/syscall.h>
#include <minios/fs/vfs.h> /* struct stat */
#include <minios/log.h>
#include <minios/process.h>
#include <minios/mm.h>

#include <sys/types.h>
#include <sys/io.h>

#include <errno_defs.h>

extern struct process * current_process; /* process.c */
extern struct process * init; /* process.c */
extern addr_t kernel_cr3;  /* sched.c */

int sys_nop() {
    return 0;
}

int sys_open(const char * path, int oflags) {
    struct vnode * vn = walk_path(path);

    if (oflags & O_CREAT) {
        if (oflags & O_EXCL) {
            /* Fail if file already exists */
            if (vn)
                return -EEXIST;
        }

        if (!vn) {
            /* Creation */
            char * filename;

            struct vnode * vp = walk_path_parent(path, &filename);
            int err = vop_create(vp, filename, 0644);

            if (err < 0)
                return err;

            vn = walk_path(path);
        }
    }

    if (!(oflags & (O_RDWR | O_RDONLY | O_WRONLY)) && oflags != 0)
        return -EINVAL;

    /* Actual open */

    int fd = process_fd_open(current_process, vn, oflags);

    return fd;
}

int sys_close(int fd) {
    return process_fd_close(current_process, fd);
}

int sys_read(int fd, void * buf, size_t num) {
    struct file * f = process_fd_file(current_process, fd);
    int err;

    if (!f)
        return -EBADF;

    if ((err = __valid_node(f->f_vn)) < 0)
        return err;

    if (S_ISDIR(f->f_vn->v_mode))
        return -EISDIR;

    if (!vf_can_read(f))
        return -EBADF;

    return vfop_read(f, buf, num);
}

int sys_write(int fd, void * buf, size_t num) {
    struct file * f = process_fd_file(current_process, fd);
    int err;
    if (!f)
        return -EBADF;

    if ((err = __valid_node(f->f_vn)) < 0)
        return err;

    if (S_ISDIR(f->f_vn->v_mode))
        return -EISDIR;

    if (!vf_can_write(f))
        return -EBADF;

    return vfop_write(f, buf, num);
}

int sys_pread(int fd, void * buf, size_t num, off_t offset) {
    struct file * f = process_fd_file(current_process, fd);
    int err;

    if (!f)
        return -EBADF;

    if ((err = __valid_node(f->f_vn)) < 0)
        return err;

    if (S_ISDIR(f->f_vn->v_mode))
        return -EISDIR;

    if (!vf_can_read(f))
        return -EBADF;

    return vfop_pread(f, buf, num, offset);
}

int sys_pwrite(int fd, void * buf, size_t num, off_t offset) {
    struct file * f = process_fd_file(current_process, fd);
    int err;

    if (!f)
        return -EBADF;

    if ((err = __valid_node(f->f_vn)) < 0)
        return err;

    if (S_ISDIR(f->f_vn->v_mode))
        return -EISDIR;

    if (!vf_can_write(f))
        return -EBADF;

    return vfop_pwrite(f, buf, num, offset);
}

int sys_lseek(int fd, off_t offset, int whence) {
    struct file * f = process_fd_file(current_process, fd);
    int err;

    if (!f)
        return -EBADF;

    if ((err = __valid_node(f->f_vn)) < 0)
        return err;

    return vfop_seek(f, offset, whence);
}

static int do_stat(struct vnode * v, struct stat * buf) {
    buf->st_dev = v->v_dev;
    buf->st_ino = v->v_ino;
    buf->st_mode = v->v_mode;
    buf->st_nlink = v->v_nlink;
    buf->st_uid = v->v_uid;
    buf->st_gid = v->v_gid;
    buf->st_atime = v->v_atime;
    buf->st_mtime = v->v_mtime;
    buf->st_ctime = v->v_ctime;
    buf->st_rdev  = 0;
    buf->st_size  = 0;

    int err;
    err = vop_get_size(v);
    if (err >= 0)
        buf->st_size = err;

    return 0;
}

int sys_stat(const char * path, struct stat * buf) {
    struct vnode * vn = walk_path(path);

    if (!vn) {
        return -ENOENT;
    }
    
    int err = do_stat(vn, buf);

    if (err) {
        return err;
    }

    return 0;
}

int sys_fstat(int fd, struct stat * buf) {
    struct file * f = process_fd_file(current_process, fd);
    int err;

    if (!f)
        return -EBADF;

    if ((err = __valid_node(f->f_vn)) < 0)
        return err;

    if ((err = do_stat(f->f_vn, buf)) < 0)
        return err;

    return 0;
}

int sys_isatty(int fd) {
    UNUSED_VAR(fd);
    return -ENOSYS;
}

int sys_link(const char * oldpath, const char * newpath) {
    char * filename;
    struct vnode * vn_old;
    struct vnode * vn_new_parent;
    
    vn_old = walk_path(oldpath);
    vn_new_parent = walk_path_parent(newpath, &filename);

    if (!vn_old || !vn_new_parent)
        return -ENOENT;
    
    int err = vop_link(vn_new_parent, filename, vn_old);
    if (err < 0)
        return err;

    return 0;
}

int sys_unlink(const char * path) {
    struct vnode * parent;
    struct vnode * child;
    char * filename;

    parent = walk_path_parent(path, &filename);

    if (!parent)
        return -ENOENT;

    int idx = find_file(parent, &child, filename);
    if (idx < 0)
        return -ENOENT;

    int err = vop_unlink(parent, idx);
    if (err < 0)
        return err;

    return 0;
}

int sys_mkdir(const char * pathname, mode_t mode) {
    struct vnode * parent;
    char * filename;

    parent = walk_path_parent(pathname, &filename);
    if (!parent)
        return -ENOENT;

    return vop_mkdir(parent, filename, mode);
}

int sys_rmdir(const char * pathname) {
    struct vnode * vn;
    struct vnode * parent;
    char * filename;

    vn = walk_path(pathname);
    parent = walk_path_parent(pathname, &filename);

    if (!vn || !parent)
        return -ENOENT;

    int idx = find_file(parent, NULL, filename);
    if (idx < 0) /* Kind of redundant, since we already checked vn != NULL */
        return -ENOENT;

    return vop_rmdir(parent, idx);
}

int sys_dup(int oldfd) {
    int newfd = process_fd_dup(current_process, oldfd, -1);

    return newfd;
}

int sys_dup2(int oldfd, int newfd) {
    newfd = process_fd_dup(current_process, oldfd, newfd);

    return newfd;
}

int sys_fork() {
    write_cr3(kernel_cr3);

    struct process * child = process_clone(current_process);

    if (child == NULL)
        return -ENOMEM;

    /* Update child's eax to 0 */
    child->p_regs.eax = 0;

    /* Add child to the pid table and to the task queue */
    ptable_insert(child->p_pid, child);
    process_push(child);

    write_cr3(current_process->p_cr3);
    /* Return child pid to the parent process */
    return child->p_pid;
}

int sys_execve(const char * filename, char * const argv[], char * const envp[]) {
    int argc = 0;
    while (argv[argc++] != NULL);
    argc--;

    if (argc > PROC_MAX_ARGV)
        return -E2BIG;

    int err = process_load(current_process, (char *) filename, (char **) argv, argc, (char **) envp);

    if (err < 0)
        return err;
    else {
        /* If successful, we MUST task switch to the current process
         * Otherwise, the registers in the PCB will not be updated when IRET'ing back to user-mode via syscall handler
         */
        write_cr3(kernel_cr3);
        process_start(current_process);
        /* ^ Hope this works! */

        return -1; /* Should never reach this */
    }
}

void sys_exit(int status) {
    if (current_process == init) {
        /* Can't be killed! */
        return;
    }

    process_exit(current_process, status);

    /* What to do now?
     * 1. Close all fds (Done in process_exit)
     * 2. Mark current process as OVER, to be picked up by scheduled cleanup (Done in process_exit)
     * 3. Send SIGCHLD to the parent process (TODO after implementing signals)
     *
     * The infinite loop should be done in the user-space exit()
     */
    return;
}

int sys_getpid() {
    return current_process->p_pid;
}

int sys_getppid() {
    if (!current_process->p_parent)
        return current_process->p_pid;

    return current_process->p_parent->p_pid;
}

void * sys_sbrk(int increment) {
    write_cr3(kernel_cr3);
    void * result = process_sbrk(current_process, increment);
    write_cr3(current_process->p_cr3);

    return result;
}

int sys_kill(pid_t pid, int signal) {
    UNUSED_VAR(pid);
    UNUSED_VAR(signal);
    return -ENOSYS;
}

pid_t sys_wait(int * status) {
    UNUSED_VAR(status);
    return -ENOSYS;
}

pid_t sys_waitpid(pid_t pid, int * status, int options) {
    UNUSED_VAR(pid);
    UNUSED_VAR(status);
    UNUSED_VAR(options);
    return -ENOSYS;
}

int sys_times() {
    return -ENOSYS;
}

int sys_gettimeofday() {
    return -ENOSYS;
}

void * syscall_table[SYSCALL_NUM] = {
    [SYSCALL_NOP] = sys_nop,
    [SYSCALL_OPEN] = sys_open,
    [SYSCALL_CLOSE] = sys_close,
    [SYSCALL_READ] = sys_read,
    [SYSCALL_WRITE] = sys_write,
    [SYSCALL_PREAD] = sys_pread,
    [SYSCALL_PWRITE] = sys_pwrite,
    [SYSCALL_LSEEK] = sys_lseek,
    [SYSCALL_STAT] = sys_stat,
    [SYSCALL_FSTAT] = sys_fstat,
    [SYSCALL_ISATTY] = sys_isatty,
    [SYSCALL_LINK] = sys_link,
    [SYSCALL_UNLINK] = sys_unlink,
    [SYSCALL_MKDIR] = sys_mkdir,
    [SYSCALL_RMDIR] = sys_rmdir,
    [SYSCALL_DUP] = sys_dup,
    [SYSCALL_DUP2] = sys_dup2,
    [SYSCALL_FORK] = sys_fork,
    [SYSCALL_EXECVE] = sys_execve,
    [SYSCALL_EXIT] = sys_exit,
    [SYSCALL_GETPID] = sys_getpid,
    [SYSCALL_GETPPID] = sys_getppid,
    [SYSCALL_SBRK] = sys_sbrk,
    [SYSCALL_KILL] = sys_kill,
    [SYSCALL_WAIT] = sys_wait,
    [SYSCALL_TIMES] = sys_times,
    [SYSCALL_GETTIMEOFDAY] = sys_gettimeofday
};

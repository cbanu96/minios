#include <minios/kernel.h>

#include <minios/x86/portio.h>
#include <minios/x86/dt.h>
#include <minios/syscall.h>
#include <minios/log.h>

#include <sys/io.h>
#include <sys/types.h>

#include <string.h>

/* flags: bits 4 and 5 are zeroed */
#define FLAG_GRAN_PAGE(x)  (x<<7)  /* 0 for byte gran, 1 for page gran */
#define FLAG_GRAN_32BIT(x) (x<<6) /* 0 for 16-bit, 1 for 32-bit selectors */

/* access byte */
#define FLAG_PRESENT(x) (x<<7)
#define FLAG_RL(x)      ((x & 3) << 5)
#define FLAG_CD         (1 << 4) /* This must always be 1 for code and data segments */
#define FLAGS_CODE      FLAG_PRESENT(1) | FLAG_CD | 0x0A
#define FLAGS_DATA      FLAG_PRESENT(1) | FLAG_CD | 0x02

/* GDT Entry structure */
struct gdt_entry {
    uint16_t limit16;
    uint16_t base16;
    uint8_t base8;
    uint8_t access;
    uint8_t flags; /* upper nibble = flags, lower nibble = limit[16..19] */
    uint8_t base8h; /* Highest 8 bits of base */
} __attribute__((packed));

/* IDT Entry structure */
struct idt_entry {
    uint16_t offset16l;
    uint16_t selector;
    uint8_t zero; /* always zero */
    uint8_t flags;
    uint16_t offset16h;
} __attribute__((packed));

/* The format LGDT, LIDT instructions want the GDT/IDT in */
struct dtreg {
    uint16_t size;      /* Size in bytes of the GDT */
    uint32_t offset;    /* Starting address of the GDT */
} __attribute__((packed));

struct tss_entry {
    uint32_t prev_tss; /* unused - will use software task switching */

    /* kernel stack segment and pointer: */
    uint32_t esp0;
    uint32_t ss0;

    /* unused: */
    uint32_t esp1;
    uint32_t ss1;
    uint32_t esp2;
    uint32_t ss2;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t edx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es;
    uint32_t cs;
    uint32_t ss;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
    uint32_t ldt;
    uint16_t trap;
    uint16_t iomap_base;
} __attribute__((packed));


struct gdt_entry gdt_gates[6];
struct idt_entry idt_gates[256];
struct dtreg gdt_ptr, idt_ptr;
struct tss_entry tss_entry;

extern void gdtflush(); /* boot/dt.S */
extern void tssflush();
extern void idtflush(); 

/* boot/interrupts.S */
extern void isr0 ();
extern void isr1 ();
extern void isr2 ();
extern void isr3 ();
extern void isr4 ();
extern void isr5 ();
extern void isr6 ();
extern void isr7 ();
extern void isr8 ();
extern void isr9 ();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();
extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();

extern void syscall_handler(); /* syscall.S */

static struct gdt_entry gdt_gate(uint32_t base, uint32_t limit, uint8_t access, uint8_t gran) {
    struct gdt_entry entry;

    entry.base16    = base & 0xFFFF;
    entry.base8     = (base >> 16) & 0xFF;
    entry.base8h    = (base >> 24) & 0xFF;

    entry.limit16   = limit & 0xFFFF;
    entry.access    = access;
    entry.flags     = gran & 0xF0;
    entry.flags     |= (limit >> 16) & 0x0F;

    return entry;
}

static void gdtinit(uint32_t esp0) {
    /* We will use a flat memory model, in which every segment is from 0 to 4GB */
    gdt_ptr.size = 6 * sizeof(struct gdt_entry) - 1;
    gdt_ptr.offset = (uint32_t) &gdt_gates;

    gdt_gates[0] = gdt_gate(0, 0, 0, 0); /* NULL descriptor */
    gdt_gates[1] = gdt_gate(0, 0xFFFFFFFF, FLAGS_CODE | FLAG_RL(0), FLAG_GRAN_PAGE(1) | FLAG_GRAN_32BIT(1)); /* Kernel Code Segment */
    gdt_gates[2] = gdt_gate(0, 0xFFFFFFFF, FLAGS_DATA | FLAG_RL(0), FLAG_GRAN_PAGE(1) | FLAG_GRAN_32BIT(1)); /* Kernel Data Segment */
    gdt_gates[3] = gdt_gate(0, 0xFFFFFFFF, FLAGS_CODE | FLAG_RL(3), FLAG_GRAN_PAGE(1) | FLAG_GRAN_32BIT(1)); /* User-space Code Segment */
    gdt_gates[4] = gdt_gate(0, 0xFFFFFFFF, FLAGS_DATA | FLAG_RL(3), FLAG_GRAN_PAGE(1) | FLAG_GRAN_32BIT(1)); /* User-space Data Segment */

    memset(&tss_entry, 0, sizeof(tss_entry));
    tss_entry.ss0 = 0x10;
    tss_entry.esp0 = esp0;
    tss_entry.iomap_base = sizeof(tss_entry);

    uint32_t tss_entry_base = (uint32_t) &tss_entry;
    uint32_t tss_entry_limit = sizeof(tss_entry);
    gdt_gates[5] = gdt_gate(tss_entry_base, tss_entry_limit, 0xE9, 0);

    gdtflush();
    tssflush();
}

void set_tss_esp(uint32_t esp0) {
    tss_entry.esp0 = esp0;
}

static struct idt_entry idt_gate(uint32_t offset, uint16_t selector, uint8_t flags) {
    struct idt_entry entry;

    entry.offset16l = offset & 0xFFFF;
    entry.offset16h = (offset >> 16) & 0xFFFF;

    entry.selector = selector;
    entry.zero = 0;
    entry.flags = flags;

    return entry;
}

static interrupt_handler_t irqs[16] = {NULL};

static interrupt_handler_t exceptions[32] = {NULL};
static char* exception_names[32] = {
    "Division By Zero",
    "Debug",
    "Non Maskable Interrupt",
    "Breakpoint",
    "Into Detected Overflow",
    "Out of Bounds",
    "Invalid Opcode",
    "No Coprocessor",
    "Double Fault",
    "Coprocessor Segment Overrun",
    "Bad TSS",
    "Segment Not Present",
    "Stack Fault",
    "General Protection Fault",
    "Page Fault",
    "Unknown Interrupt",
    "Coprocessor Fault",
    "Alignment Check",
    "Machine Check",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};

static void idtinit() {
    cli();

    idt_ptr.size = 256 * sizeof(struct idt_entry) - 1;
    idt_ptr.offset = (uint32_t) &idt_gates;

    memset(&idt_gates, 0, sizeof(idt_gates));

    /* Remap the PIC */
    outb(PIC_MASTER_CMDPORT, 0x11); /* Send initialization byte */
    outb(PIC_SLAVE_CMDPORT, 0x11);

    outb(PIC_MASTER_DATPORT, 0x20); /* Send offsets: 32 for MASTER, 40 for SLAVE */
    outb(PIC_SLAVE_DATPORT, 0x28);

    outb(PIC_MASTER_DATPORT, 4); /* (1<<2) - IRQ2 = cascade between MASTER and SLAVE */
    outb(PIC_SLAVE_DATPORT, 2);  /* cascade identity for the SLAVE */

    outb(PIC_MASTER_DATPORT, 1); /* 8086 Mode */
    outb(PIC_SLAVE_DATPORT, 1);

    outb(PIC_MASTER_DATPORT, 0);
    outb(PIC_SLAVE_DATPORT, 0);

    /* 0x08 = kernel code segment,
     * 0x8E = present bit | i386 interrupt gate bits */

    /* ISR0-ISR31 */
    idt_gates[0] = idt_gate((uint32_t) &isr0, 0x08, 0x8E);
    idt_gates[1] = idt_gate((uint32_t) &isr1, 0x08, 0x8E);
    idt_gates[2] = idt_gate((uint32_t) &isr2, 0x08, 0x8E);
    idt_gates[3] = idt_gate((uint32_t) &isr3, 0x08, 0x8E);
    idt_gates[4] = idt_gate((uint32_t) &isr4, 0x08, 0x8E);
    idt_gates[5] = idt_gate((uint32_t) &isr5, 0x08, 0x8E);
    idt_gates[6] = idt_gate((uint32_t) &isr6, 0x08, 0x8E);
    idt_gates[7] = idt_gate((uint32_t) &isr7, 0x08, 0x8E);
    idt_gates[8] = idt_gate((uint32_t) &isr8, 0x08, 0x8E);
    idt_gates[9] = idt_gate((uint32_t) &isr9, 0x08, 0x8E);
    idt_gates[10] = idt_gate((uint32_t) &isr10, 0x08, 0x8E);
    idt_gates[11] = idt_gate((uint32_t) &isr11, 0x08, 0x8E);
    idt_gates[12] = idt_gate((uint32_t) &isr12, 0x08, 0x8E);
    idt_gates[13] = idt_gate((uint32_t) &isr13, 0x08, 0x8E);
    idt_gates[14] = idt_gate((uint32_t) &isr14, 0x08, 0x8E);
    idt_gates[15] = idt_gate((uint32_t) &isr15, 0x08, 0x8E);
    idt_gates[16] = idt_gate((uint32_t) &isr16, 0x08, 0x8E);
    idt_gates[17] = idt_gate((uint32_t) &isr17, 0x08, 0x8E);
    idt_gates[18] = idt_gate((uint32_t) &isr18, 0x08, 0x8E);
    idt_gates[19] = idt_gate((uint32_t) &isr19, 0x08, 0x8E);
    idt_gates[20] = idt_gate((uint32_t) &isr20, 0x08, 0x8E);
    idt_gates[21] = idt_gate((uint32_t) &isr21, 0x08, 0x8E);
    idt_gates[22] = idt_gate((uint32_t) &isr22, 0x08, 0x8E);
    idt_gates[23] = idt_gate((uint32_t) &isr23, 0x08, 0x8E);
    idt_gates[24] = idt_gate((uint32_t) &isr24, 0x08, 0x8E);
    idt_gates[25] = idt_gate((uint32_t) &isr25, 0x08, 0x8E);
    idt_gates[26] = idt_gate((uint32_t) &isr26, 0x08, 0x8E);
    idt_gates[27] = idt_gate((uint32_t) &isr27, 0x08, 0x8E);
    idt_gates[28] = idt_gate((uint32_t) &isr28, 0x08, 0x8E);
    idt_gates[29] = idt_gate((uint32_t) &isr29, 0x08, 0x8E);
    idt_gates[30] = idt_gate((uint32_t) &isr30, 0x08, 0x8E);
    idt_gates[31] = idt_gate((uint32_t) &isr31, 0x08, 0x8E);
    
    /* IRQ32-47 */
    idt_gates[32] = idt_gate((uint32_t) &irq0, 0x08, 0x8E);
    idt_gates[33] = idt_gate((uint32_t) &irq1, 0x08, 0x8E);
    idt_gates[34] = idt_gate((uint32_t) &irq2, 0x08, 0x8E);
    idt_gates[35] = idt_gate((uint32_t) &irq3, 0x08, 0x8E);
    idt_gates[36] = idt_gate((uint32_t) &irq4, 0x08, 0x8E);
    idt_gates[37] = idt_gate((uint32_t) &irq5, 0x08, 0x8E);
    idt_gates[38] = idt_gate((uint32_t) &irq6, 0x08, 0x8E);
    idt_gates[39] = idt_gate((uint32_t) &irq7, 0x08, 0x8E);
    idt_gates[40] = idt_gate((uint32_t) &irq8, 0x08, 0x8E);
    idt_gates[41] = idt_gate((uint32_t) &irq9, 0x08, 0x8E);
    idt_gates[42] = idt_gate((uint32_t) &irq10, 0x08, 0x8E);
    idt_gates[43] = idt_gate((uint32_t) &irq11, 0x08, 0x8E);
    idt_gates[44] = idt_gate((uint32_t) &irq12, 0x08, 0x8E);
    idt_gates[45] = idt_gate((uint32_t) &irq13, 0x08, 0x8E);
    idt_gates[46] = idt_gate((uint32_t) &irq14, 0x08, 0x8E);
    idt_gates[47] = idt_gate((uint32_t) &irq15, 0x08, 0x8E);

    /* SYSCALL_INT will be invoked from user-space
     * we need to set the DPL to 3, not 0
     */
    idt_gates[SYSCALL_INT] = idt_gate((uint32_t) &syscall_handler, 0x08, 0xEE);

    idtflush();
}

void irq_send_eoi(uint32_t num) {
    if (num >= 40)
        outb(PIC_SLAVE_CMDPORT, PIC_EOI);
    outb(PIC_MASTER_CMDPORT, PIC_EOI);
}

/* Called from boot/interrupts.S,
 * will choose the appropriate way to respond to
 * exceptions and IRQs */
void interrupt_handler(struct interrupt_registers *regs) {
    /* If we are already inside a system call and an IRQ/ISR occurs,
     * we must save the registers before switching to the next piece
     * of kernel code.
     */
    if (regs->int_no >= 32 && regs->int_no <= 47) {
        /* Send an End of Interrupt signal to the MASTER
         * and SLAVE, if necessary, PICs,
         * if there is no handler */
        if (irqs[regs->int_no - 32] == NULL) {
            irq_send_eoi(regs->int_no);
        } else {
            /* Handle this IRQ */
            interrupt_handler_t handler = irqs[regs->int_no - 32];
            handler(regs);
        }
    } else {
        if (exceptions[regs->int_no] == NULL) {
            /* Handler undefined for this exception,
             * we will print an error message and halt the system. */
            klog (KERROR "Unhandled interrupt: %d (%s) \t error code: 0x%x\n", regs->int_no, exception_names[regs->int_no], regs->err_code);
            cli();
            hlt();
        } else {
            interrupt_handler_t handler = exceptions[regs->int_no];
            handler(regs);
        }
    }
}

/* Sets either an IRQ or an exception handler */
void interrupt_set_handler(uint8_t entry, interrupt_handler_t handler) {
    if (entry >= 32) {
        irqs[entry - 32] = handler;
    } else {
        exceptions[entry] = handler;
    }
}

void dtinit(uint32_t esp0) {
    klog(KINFO "Setting up descriptor tables... GDT... ");
    gdtinit(esp0);
    klog(KINFO                                " IDT...");
    idtinit();
    klog(KINFO "\tDone!\n");
}

#include <minios/kernel.h>
#include <minios/drv/vga.h>
#include <minios/fs/vfs.h>
#include <minios/log.h>

#include <string.h>
#include <sys/types.h>
#include <stdarg.h>

extern size_t vsnprintf(char *s, size_t n, const char *fmt, va_list ap);

static uint8_t loglevel = LOGLEVEL_DEBUG; /* default */
static bool logearly;

static struct file * f_output;

void klogoutput(struct file * f) {
    f_output = f;
}

void klogearly(bool enable) {
    logearly = enable;
}

void kloglevel(uint8_t level) {
    loglevel = level;
}

void klog(const char* fmt, ...) {
    char buffer[256];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buffer, 256, fmt, ap);
    va_end(ap);

    char flag = buffer[0];

    /* Quit early if this message doesn't have the required log level */
    if (flag < loglevel)
        return;

    if (logearly) {
        vputs(&buffer[0] + 1);
    } else {
        vfop_write(f_output, &buffer[0] + 1, strlen(&buffer[0] + 1));
    }
}

#include "include/syscall.h"

#include <sys/errno.h>
#include <sys/types.h>

#include <errno.h>

#undef errno

extern int errno;
extern char ** environ;

SYSCALL0_DECL(SYSCALL_NOP, int, nop);
SYSCALL2_DECL(SYSCALL_OPEN, int, open, const char *, int);
SYSCALL1_DECL(SYSCALL_CLOSE, int, close, int);
SYSCALL3_DECL(SYSCALL_READ, ssize_t, read, int, void *, size_t);
SYSCALL3_DECL(SYSCALL_WRITE, ssize_t, write, int, void *, size_t);
SYSCALL4_DECL(SYSCALL_PREAD, ssize_t, pread, int, void *, size_t, off_t);
SYSCALL4_DECL(SYSCALL_PWRITE, ssize_t, pwrite, int, void *, size_t, off_t);
SYSCALL3_DECL(SYSCALL_LSEEK, off_t, lseek, int, off_t, int);
SYSCALL2_DECL(SYSCALL_STAT, int, stat, const char *, void *);
SYSCALL2_DECL(SYSCALL_FSTAT, int, fstat, int, void *);
SYSCALL1_DECL(SYSCALL_ISATTY, int, isatty, int);
SYSCALL2_DECL(SYSCALL_LINK, int, link, const char *, const char *);
SYSCALL1_DECL(SYSCALL_UNLINK, int, unlink, const char *);
SYSCALL2_DECL(SYSCALL_MKDIR, int, mkdir, const char *, mode_t);
SYSCALL1_DECL(SYSCALL_RMDIR, int, rmdir, const char *);
SYSCALL1_DECL(SYSCALL_DUP, int, dup, int);
SYSCALL2_DECL(SYSCALL_DUP2, int, dup2, int, int);
SYSCALL0_DECL(SYSCALL_FORK, int, fork);
SYSCALL3_DECL(SYSCALL_EXECVE, int, execve, const char *, char * const *, char * const *);
SYSCALL0_DECL(SYSCALL_GETPID, int, getpid);
SYSCALL0_DECL(SYSCALL_GETPPID, int, getppid);
SYSCALL1_DECL(SYSCALL_SBRK, void *, syscall_sbrk, int);
SYSCALL2_DECL(SYSCALL_KILL, int, kill, pid_t, int);
SYSCALL1_DECL(SYSCALL_WAIT, pid_t, wait, int *);
SYSCALL3_DECL(SYSCALL_WAITPID, pid_t, waitpid, pid_t, int *, int);
SYSCALL0_DECL(SYSCALL_TIMES, int, times);
SYSCALL0_DECL(SYSCALL_GETTIMEOFDAY, int, gettimeofday);

void * sbrk(int increment) {
    void * ret = syscall_sbrk(increment);
    if (ret == (void *)-1) {
        errno = ENOMEM;
    }

    return ret;
}

/* exit() is special: */
void _exit(int status) {
    asm volatile("int $0x80" :: "a"(SYSCALL_EXIT), "b"(status));

    /* We have to infinite loop */
    for (;;);
}


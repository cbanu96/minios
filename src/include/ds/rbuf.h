#ifndef __INCLUDE_DS_RBUF_H
#define __INCLUDE_DS_RBUF_H

#include <sys/types.h>
#include <minios/spinlock.h>

typedef struct {
    int r_begin;
    int r_size;

    uint16_t r_flags;

    int r_capacity;
    char * r_buf;

    spinlock_t r_lock;
} rbuf_t;

/* Ring buffer flags */
#define RBUF_W_NONBLOCK 0x01
#define RBUF_R_NONBLOCK 0x02

rbuf_t * rbuf_create(size_t);

void rbuf_destroy(rbuf_t *);

size_t rbuf_write(rbuf_t *, char *, size_t);
size_t rbuf_read(rbuf_t *, char *, size_t);




#endif /* __INCLUDE_DS_RBUF_H */

#ifndef __INCLUDE_DS_LIST_H
#define __INCLUDE_DS_LIST_H

#include <sys/types.h>

typedef struct list_node {
    struct list_node *next;
    struct list_node *prev;

    void * value;
    void * list;    /* Pointer to the list to which this node currently belongs */
} list_node_t;

typedef struct {
    list_node_t *head;
    list_node_t *tail;

    size_t length;
} list_t;

list_t * list_create(void);
void list_free_val(list_t *);
void list_free(list_t *);

void list_insert(list_t *, list_node_t *);
list_node_t * list_push(list_t *, void *);

void list_delete(list_t *, list_node_t *);
void * list_pop_head(list_t *);
void * list_pop_tail(list_t *);

list_node_t * list_find(list_t *, void *);
list_node_t * list_find_idx(list_t *, size_t); 

#define list_foreach(it, list) for (it = list->head; it != NULL; it = it->next)
#define list_foreach_rev(it, list) for (it = list->tail; it != NULL; it = it->prev)

#endif /* __INCLUDE_DS_LIST_H */

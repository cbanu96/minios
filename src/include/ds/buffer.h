#ifndef __INCLUDE_DS_BUFFER_H
#define __INCLUDE_DS_BUFFER_H

#include <sys/types.h>
#include <minios/spinlock.h>

typedef struct {
    size_t b_size;
    size_t b_capacity;

    char * b_buffer;
} buffer_t;

buffer_t * buffer_create();
void buffer_destroy(buffer_t *);

void buffer_clear(buffer_t *);
bool buffer_push(buffer_t *, char);
bool buffer_pop(buffer_t * buf);

#define BUFFER_MAX_SIZE 8192 /* Max. 8 KiB of data per buffer */
#define BUFFER_INITIAL_SIZE 16

#endif /* __INCLUDE_DS_RBUF_H */

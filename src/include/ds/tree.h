#ifndef __INCLUDE_DS_TREE_H
#define __INCLUDE_DS_TREE_H

#include <sys/types.h>
#include <ds/list.h>

typedef struct tree_node {
    struct tree_node *parent;
    list_t * children; /* List of children nodes */
    void * value;
} tree_node_t;

typedef struct {
    tree_node_t *root;
} tree_t;

tree_t * tree_create();
void tree_root(tree_t *, tree_node_t *);
void tree_free_val(tree_t *);
void tree_free(tree_t *);

tree_node_t * tree_node_create(void * value);
void tree_node_append_child(tree_node_t *, tree_node_t *);
void tree_node_delete(tree_node_t *);
void tree_node_free_val(tree_node_t *);
void tree_node_free(tree_node_t *);

tree_node_t * tree_node_find(tree_node_t *, void *);
tree_node_t * tree_find(tree_t *, void *);

#endif /* __INCLUDE_DS_LIST_H */

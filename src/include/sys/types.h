#ifndef __INCLUDE_SYS_TYPES_H
#define __INCLUDE_SYS_TYPES_H

/* ---> Generic */

typedef char int8_t;
typedef unsigned char uint8_t;

typedef short int int16_t;
typedef unsigned short int uint16_t;

typedef long int int32_t;
typedef unsigned long int uint32_t;

typedef long long int64_t;
typedef unsigned long long uint64_t;

typedef _Bool bool;
#define true 1
#define false 0

#define NULL ((void *)0)

typedef uint32_t size_t;
typedef int32_t ssize_t;

typedef uint32_t addr_t;

/* ---> File/Directory/Device */

typedef uint32_t ino_t;

typedef uint32_t dev_t;

typedef int16_t mode_t;

typedef int32_t off_t;

/* ---> Users/Groups/Processes */

typedef int32_t pid_t;

typedef int32_t gid_t;

typedef int32_t uid_t;

typedef int32_t id_t;

/* ---> Misc */

typedef int32_t clock_t;

typedef int32_t time_t;

#endif /* __INCLUDE_SYS_TYPES_H */

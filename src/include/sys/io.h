#ifndef __INCLUDE_SYS_IO_H
#define __INCLUDE_SYS_IO_H

#include <sys/types.h>

static inline void outb(uint16_t port, uint8_t value) {
    asm volatile ( "outb %0, %1" : : "a"(value), "Nd"(port) );
}

static inline void outw(uint16_t port, uint16_t value) {
    asm volatile ( "outw %0, %1" : : "a"(value), "Nd"(port) );
}

static inline void outl(uint16_t port, uint32_t value) {
    asm volatile ( "outl %0, %1" : : "a"(value), "Nd"(port) );
}

static inline uint8_t inb(uint16_t port) {
    uint8_t result;
    asm volatile ( "inb %1, %0" : "=a"(result) : "Nd"(port) );

    return result;
}

static inline uint16_t inw(uint16_t port) {
    uint16_t result;
    asm volatile ( "inw %1, %0" : "=a"(result) : "Nd"(port) );

    return result;
}

static inline uint32_t inl(uint16_t port) {
    uint32_t result;
    asm volatile ( "inl %1, %0" : "=a"(result) : "Nd"(port) );

    return result;
}

static inline void io_wait() {
    /* Wait for an IO operation to complete by sending another byte ( 0 )
     * to an unused register (0x80)
     */

    asm volatile ( "outb %%al, $0x80" : : "a"(0) );
}

static inline uint32_t read_cr0() {
    unsigned long cr0;
    asm volatile ("mov %%cr0,%0" : "=r" (cr0));
    return cr0;
}

static inline uint32_t read_cr1() {
    unsigned long cr1;
    asm volatile ("mov %%cr1,%0" : "=r" (cr1));
    return cr1;
}

static inline uint32_t read_cr2() {
    unsigned long cr2;
    asm volatile ("mov %%cr2,%0" : "=r" (cr2));
    return cr2;
}

static inline uint32_t read_cr3() {
    unsigned long cr3;
    asm volatile ("mov %%cr3,%0" : "=r" (cr3));
    return cr3;
}

static inline uint32_t read_cr4() {
    unsigned long cr4;
    asm volatile ("mov %%cr4,%0" : "=r" (cr4));
    return cr4;
}

static inline void write_cr0(uint32_t cr0) {
    asm volatile ("mov %0, %%cr0" : : "r"(cr0));
}

static inline void write_cr1(uint32_t cr1) {
    asm volatile ("mov %0, %%cr1" : : "r"(cr1));
}

static inline void write_cr2(uint32_t cr2) {
    asm volatile ("mov %0, %%cr2" : : "r"(cr2));
}

static inline void write_cr3(uint32_t cr3) {
    asm volatile ("mov %0, %%cr3" : : "r"(cr3));
}

static inline void write_cr4(uint32_t cr4) {
    asm volatile ("mov %0, %%cr4" : : "r"(cr4));
}

static inline uint32_t read_eflags() {
    uint32_t eflags;
    asm volatile ("pushf\n\t"
                  "pop %0\n"
                  : "=r" (eflags) );
    return eflags;
}

#endif /* __INCLUDE_SYS_IO_H */

#ifndef __INCLUDE_ASSERT_H
#define __INCLUDE_ASSERT_H

/* Stringification macros in order to be able
 * to print __LINE__ as a string */
#define S(x) #x
#define S_(x) S(x)
#define S__LINE__ S_(__LINE__)

/* Formatted panic */
/* We will enable "early" logging so the panic message is printed directly to the screen */
#define PANIC(fmt, ...) do { klogearly(true); klog(KERROR fmt, ##__VA_ARGS__); cli(); hlt(); } while(0)

/* The classic assert macro */
#define ASSERT(cond) do { if (!(cond)) PANIC("Assertion failed: " #cond " in file " __FILE__ " on line " S__LINE__ "\n"); } while(0)

/* Assert with a custom formatted message */
#define ASSERTF(cond, fmt, ...) do { if (!(cond)) PANIC(fmt, ##__VA_ARGS__); } while(0)

/* Breakpoint ( interrupt 3 ) */
#define BREAKPOINT() asm ("int $0x3")


#endif /* __INCLUDE_ASSERT_H */

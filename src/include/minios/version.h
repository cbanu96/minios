#ifndef __INCLUDE_VERSION_H
#define __INCLUDE_VERSION_H

#define KERNEL_NAME     "MiniOS"
#define KERNEL_VERSION  "0.0.1"
#define KERNEL_AUTHOR   "Cristian Banu"
#define KERNEL_COMPILE_DATETIME __DATE__ " " __TIME__

#define KERNEL_WELCOME \
    "Welcome to " KERNEL_NAME " (v. " KERNEL_VERSION ") by " KERNEL_AUTHOR "\n" \
    "Last compiled at: " KERNEL_COMPILE_DATETIME "\n"

#endif /* __INCLUDE_VERSION_H */

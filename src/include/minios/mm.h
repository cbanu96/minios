#ifndef __INCLUDE_MM_H
#define __INCLUDE_MM_H

#include <boot/multiboot.h>
#include <sys/types.h>

#define KERNEL_HEAP_BEGIN       0xD0000000UL
#define KERNEL_HEAP_MIN_SIZE    0x8000UL
#define KERNEL_HEAP_MAX_SIZE    0x10000000UL   /* 256 MB */

/* Macros for 4KB pages */
#define PAGE_TABLE_SIZE     0x400000UL
#define PAGE_SIZE           0x1000UL
#define PAGE_ALIGN_DOWN(x)  ((x) & ~(0xFFFUL))
#define PAGE_ALIGN_UP(x)    PAGE_ALIGN_DOWN((x + (0xFFFUL)))
#define PAGE_ALIGNED(x)     ((x) == PAGE_ALIGN_DOWN(x))

/* Fractal Mapping macros */
#define         PDE_ADDRESS     0xFFFFF000
#define         PTE_ADDRESS     0xFFC00000


/* Memory usage */
typedef struct {
    uint32_t total_memory;
    uint32_t free_memory;
    uint32_t available_memory;
    uint32_t used_memory;
} mem_stat_t;

void mminit(multiboot_info_t * mb_hdr);
void mm_alloc_page_kernel_phys(uint32_t virt, uint32_t phys);
void mm_alloc_page_user_phys(uint32_t virt, uint32_t phys);
void mm_alloc_page_kernel(uint32_t virt, uint32_t *phys);
void mm_alloc_page_user(uint32_t virt, uint32_t *phys);
void mm_free_page(uint32_t virt, bool pass);
void mm_free_pde(size_t idx, bool pass);

void kheapinit(uint32_t max_size);
void * kmalloc(uint32_t bytes);
void * kmallocp(uint32_t bytes);
void kfree(void * ptr);

uint32_t __get_phys_addr(uint32_t cr3, uint32_t virt);

bool __check_pd_present(uint32_t pde);
bool __check_pt_present(uint32_t pde, uint32_t pte);
bool __check_addr_present(addr_t);

#endif /* __INCLUDE_MM_H */

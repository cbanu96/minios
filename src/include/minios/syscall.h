#ifndef __INCLUDE_SYSCALL_H
#define __INCLUDE_SYSCALL_H

#define SYSCALL_NUM 28
#define SYSCALL_INT 128 /* 0x80 */

/* NOP */
#define SYSCALL_NOP         0

/* VFS */
#define SYSCALL_OPEN        1
#define SYSCALL_CLOSE       2
#define SYSCALL_READ        3
#define SYSCALL_WRITE       4
#define SYSCALL_PREAD       5
#define SYSCALL_PWRITE      6
#define SYSCALL_LSEEK       7
#define SYSCALL_STAT        8
#define SYSCALL_FSTAT       9
#define SYSCALL_ISATTY      10
#define SYSCALL_LINK        11
#define SYSCALL_UNLINK      12
#define SYSCALL_MKDIR       24
#define SYSCALL_RMDIR       25
#define SYSCALL_DUP         26
#define SYSCALL_DUP2        27

/* Process */
#define SYSCALL_FORK        13
#define SYSCALL_EXECVE      14
#define SYSCALL_EXIT        15
#define SYSCALL_GETPID      16
#define SYSCALL_GETPPID     17
#define SYSCALL_SBRK        18

/* Signals */
#define SYSCALL_KILL        19
#define SYSCALL_WAIT        20
#define SYSCALL_WAITPID     21

/* Time */
#define SYSCALL_TIMES       22
#define SYSCALL_GETTIMEOFDAY 23

/* Syscall return */
#define SYSCALL_RET(rtype, retval) do { \
        if (retval < 0) { \
            errno = -retval; \
            return (rtype) -1; \
        } \
        return (rtype) retval; \
    } while (0)

/* Syscall Declaration Macros */
#define SYSCALL0_DECL(n, rtype, f) \
    rtype f(void) { \
        int res; \
        asm volatile ("int $0x80" \
                : "=a"(res) \
                : "0"(n)); \
        SYSCALL_RET(rtype, res); \
    }

#define SYSCALL1_DECL(n, rtype, f, T1) \
    rtype f(T1 a) { \
        int res; \
        asm volatile ("int $0x80" \
                : "=a"(res) \
                : "0"(n), "b"(a)); \
        SYSCALL_RET(rtype, res); \
    }

#define SYSCALL2_DECL(n, rtype, f, T1, T2) \
    rtype f(T1 a, T2 b) { \
        int res; \
        asm volatile ("int $0x80" \
                : "=a"(res) \
                : "0"(n), "b"(a), "c"(b)); \
        SYSCALL_RET(rtype, res); \
    }

#define SYSCALL3_DECL(n, rtype, f, T1, T2, T3) \
    rtype f(T1 a, T2 b, T3 c) { \
        int res; \
        asm volatile ("int $0x80" \
                : "=a"(res) \
                : "0"(n), "b"(a), "c"(b), "d"(c)); \
        SYSCALL_RET(rtype, res); \
    }

#define SYSCALL4_DECL(n, rtype, f, T1, T2, T3, T4) \
    rtype f(T1 a, T2 b, T3 c, T4 d) { \
        int res; \
        asm volatile ("int $0x80" \
                : "=a"(res) \
                : "0"(n), "b"(a), "c"(b), "d"(c), "S"(d)); \
        SYSCALL_RET(rtype, res); \
    }

#define SYSCALL5_DECL(n, rtype, f, T1, T2, T3, T4, T5) \
    rtype f(T1 a, T2 b, T3 c, T4 d, T5 e) { \
        int res; \
        asm volatile ("int $0x80" \
                : "=a"(res) \
                : "0"(n), "b"(a), "c"(b), "d"(c), "S"(d), "D"(e)); \
        SYSCALL_RET(rtype, res); \
    }

#endif /* __INCLUDE_SYSCALL_H */

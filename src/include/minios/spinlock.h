#ifndef __INCLUDE_SPINLOCK_H
#define __INCLUDE_SPINLOCK_H

typedef uint8_t volatile spinlock_t;

#define ACQUIRE_LOCK(lock) \
    while (__sync_lock_test_and_set(&lock, 0x01)) { \
        /* TODO: yield until lock is released */ \
    }

#define RELEASE_LOCK(lock) \
    __sync_lock_release(&lock);

#endif /* __INCLUDE_SPINLOCK_H */

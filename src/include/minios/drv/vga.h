#ifndef __INCLUDE_DRV_VGA_H
#define __INCLUDE_DRV_VGA_H

typedef enum {
    VGA_CLR_BLACK = 0,
    VGA_CLR_BLUE = 1,
    VGA_CLR_GREEN = 2,
    VGA_CLR_CYAN = 3,
    VGA_CLR_RED = 4,
    VGA_CLR_MAGENTA = 5,
    VGA_CLR_BROWN = 6,
    VGA_CLR_LIGHT_GREY = 7,
    VGA_CLR_DARK_GREY = 8,
    VGA_CLR_LIGHT_BLUE = 9,
    VGA_CLR_LIGHT_GREEN = 10,
    VGA_CLR_LIGHT_CYAN = 11,
    VGA_CLR_LIGHT_RED = 12,
    VGA_CLR_LIGHT_MAGENTA = 13,
    VGA_CLR_LIGHT_BROWN = 14,
    VGA_CLR_WHITE = 15
} vga_color_t;

void vgainit(void);
void vputs(const char*);
void vnputs(char *, size_t);
void vsetcolor(vga_color_t, vga_color_t);

#endif /* __INCLUDE_MINIOS_DRV_VGA_H */

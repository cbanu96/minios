#ifndef __INCLUDE_DRV_KBD_H
#define __INCLUDE_DRV_KBD_H

#include <minios/fs/tty.h>

/* Scan Code Set 2 - taken from http://wiki.osdev.org/PS/2_Keyboard */

#define KBD_ESC         0x01
#define KBD_NUM1        0x02
#define KBD_NUM2        0x03
#define KBD_NUM3        0x04
#define KBD_NUM4        0x05
#define KBD_NUM5        0x06
#define KBD_NUM6        0x07
#define KBD_NUM7        0x08
#define KBD_NUM8        0x09
#define KBD_NUM9        0x0A
#define KBD_NUM0        0x0B
#define KBD_SYM1        0x0C
#define KBD_SYM2        0x0D
#define KBD_BCKSPC      0x0E
#define KBD_TAB         0x0F
#define KBD_Q           0x10
#define KBD_W           0x11
#define KBD_E           0x12
#define KBD_R           0x13
#define KBD_T           0x14
#define KBD_Y           0x15
#define KBD_U           0x16
#define KBD_I           0x17
#define KBD_O           0x18
#define KBD_P           0x19
#define KBD_SYM3        0x1A
#define KBD_SYM4        0x1B
#define KBD_ENTER       0x1C
#define KBD_LCTRL       0x1D
#define KBD_A           0x1E
#define KBD_S           0x1F
#define KBD_D           0x20
#define KBD_F           0x21
#define KBD_G           0x22
#define KBD_H           0x23
#define KBD_J           0x24
#define KBD_K           0x25
#define KBD_L           0x26
#define KBD_SYM5        0x27
#define KBD_SYM6        0x28
#define KBD_SYM7        0x29
#define KBD_LSHIFT      0x2A
#define KBD_SYM8        0x2B
#define KBD_Z           0x2C
#define KBD_X           0x2D
#define KBD_C           0x2E
#define KBD_V           0x2F
#define KBD_B           0x30
#define KBD_N           0x31
#define KBD_M           0x32
#define KBD_SYM9        0x33
#define KBD_SYM10       0x34
#define KBD_SYM11       0x35
#define KBD_RSHIFT      0x36
#define KBD_SYM12       0x37
#define KBD_LALT        0x38
#define KBD_SPACE       0x39
#define KBD_CAPSLOCK    0x3A
#define KBD_F1          0x3B
#define KBD_F2          0x3C
#define KBD_F3          0x3D
#define KBD_F4          0x3E
#define KBD_F5          0x3F
#define KBD_F6          0x40
#define KBD_F7          0x41
#define KBD_F8          0x42
#define KBD_F9          0x43
#define KBD_F10         0x44
#define KBD_NUMLOCK     0x45
#define KBD_SCROLLLOCK  0x46
#define KBD_KPAD_NUM7   0x47
#define KBD_KPAD_NUM8   0x48
#define KBD_KPAD_NUM9   0x49
#define KBD_SYM13       0x4A
#define KBD_KPAD_NUM4   0x4B
#define KBD_KPAD_NUM5   0x4C
#define KBD_KPAD_NUM6   0x4D
#define KBD_SYM14       0x4E
#define KBD_KPAD_NUM1   0x4F
#define KBD_KPAD_NUM2   0x50
#define KBD_KPAD_NUM3   0x51
#define KBD_KPAD_NUM0   0x52
#define KBD_SYM15       0x53
#define KBD_SYSRQ       0x54
#define KBD_NOTSTD1     0x55
#define KBD_NOTSTD2     0x56
#define KBD_F11         0x57
#define KBD_F12         0x58

#define KBD_SEQESCP     0xE0 /* Followed by another byte */

#define KBD_KPAD_ENTER  0x9C
#define KBD_RCTRL       0x9D
#define KBD_FAKELSHIFT  0xAA
#define KBD_SYM16       0xB5
#define KBD_FAKERSHIFT  0xB6
#define KBD_CTRLPRTSCRN 0xB7
#define KBD_RALT        0xB8
#define KBD_CTRLBREAK   0xC6
#define KBD_HOME        0xC7
#define KBD_UP          0xC8
#define KBD_PGUP        0xC9
#define KBD_LEFT        0xCB
#define KBD_RIGHT       0xCD
#define KBD_END         0xCF
#define KBD_DOWN        0xD0
#define KBD_PGDOWN      0xD1
#define KBD_INSERT      0xD2
#define KBD_DELETE      0xD3
#define KBD_LSUPER      0xDB
#define KBD_RSUPER      0xDC
#define KBD_MENU        0xDD

#define LED_SCROLLLOCK  0x01
#define LED_NUMLOCK     0x02
#define LED_CAPSLOCK    0x04

#define KBD_BUFFER_SIZE 256

int kbd_init();
int kbd_dequeue();
bool kbd_empty();
void kbd_setty(tty_t *);

#endif /* __INCLUDE_MINIOS_DRV_KBD_H */

#ifndef __INCLUDE_PROCESS_H
#define __INCLUDE_PROCESS_H

#include <minios/x86/dt.h>
#include <minios/fs/vfs.h>
#include <minios/mm.h>

#define PROC_INIT_MAX           128      /* The initial number of maximum processes. If exceeded, will double itself. */
#define PROC_MAX_FDS            32       /* Maximum number of file descriptors per process. */

#define PROC_KERNEL_STACK_SIZE  16384    /* Obviously, each process's kernel stack should have a small footprint, so
                                            it won't om-nom-nom memory up like Cho'Gath. */

#define PROC_IMAGE_SECTIONS     2        
#define PROC_IMAGE_TEXT         0        /* Application code, data, rodata, heap, everything */
#define PROC_IMAGE_USER         1        /* Application stack used by user-level code */


#define PROC_IMAGE_USER_STACK_BOTTOM  0xB000000 /* Can't overlap kernel or user code */
#define PROC_IMAGE_USER_STACK_TOP     0xB010000 /* 64 KiB */

#define PROC_MAX_ARGV 128 /* Maximum argument count */

/* Process Table Entry */
struct ptable_entry {
    pid_t e_pid;
    struct process * e_proc;
};

/* Process Table */
struct ptable {
    struct ptable_entry * entries;
    uint32_t length;
    uint32_t used;
};

/* The Process Control Block */
struct process {
    /* Process ID */
    pid_t p_pid;

    /* Process UID / Effective UID */
    uid_t p_uid, p_euid;

    /* Process State */
#define PROC_STATE_READY        0
#define PROC_STATE_BLOCK        1
#define PROC_STATE_ACTIVE       2
#define PROC_STATE_OVER         3 /* Awaiting children or, if no children are active, cleanup */
    uint32_t p_state;

    uint32_t p_retval; /* Exit value of this process */

    addr_t p_heap; /* Address of the heap */

    /* CPU registers */
    struct interrupt_registers p_regs;

    /* Kernel stack ESP */
    uint32_t p_kesp;

    /* File descriptors - Each "file descriptor" is an offset into this array of pointers to vfs nodes */
    struct file * p_fds[PROC_MAX_FDS];

    /* CR3 - pagedir physical address */
    addr_t p_cr3;

    /* File system root and CWD */
    struct vnode * p_root;
    struct vnode * p_cwd;

    /* Process Tree Metadata */
    struct process * p_parent;
    list_t * p_children;
};

/* Process Management */
struct process * process_create(struct process *);
struct process * process_clone(struct process *);
void process_destroy(struct process *);

/* Process Image */
void process_image_copy(struct process *, struct process *);
void process_clear_pd(struct process *);
void process_alloc_page(struct process *, addr_t);
void process_dealloc_page(struct process *, addr_t);
void process_update_kernel_pages(struct process *);
void * process_sbrk(struct process *, int);

/* Process Memory Management */
void process_memcpy(struct process *, void *, void *, size_t);
void process_memset(struct process *, void *, int, size_t);

/* Process File Descriptors */
int process_fd_open(struct process *, struct vnode *, int);
int process_fd_close(struct process *, int);
int process_fd_dup(struct process *, int, int);
struct file * process_fd_file(struct process *, int);
struct vnode * process_fd_node(struct process *, int);

/* Process Running */
void process_start(struct process *);
int process_load(struct process *, char *, char **, int, char **);
void process_exit(struct process *, int);

/* Process Queue Management */
void process_push(struct process *);
struct process * process_pop();

/* Process Table Management */
void ptable_init();
void ptable_insert(pid_t, struct process *);
void ptable_delete(pid_t);
struct process * ptable_find(pid_t);

/* Process Tree Manipulation */
int process_reparent(struct process *);

/* Initialize multitasking */
void schedinit(void);


#endif /* __INCLUDE_PROCESS_H */

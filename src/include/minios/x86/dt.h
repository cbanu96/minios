#ifndef __INCLUDE_X86_DT_H
#define __INCLUDE_X86_DT_H

/* Registers which are on the stack after ISR/IRQ */
struct interrupt_registers {
    uint32_t gs, fs, es, ds; /* pushed by us */
    uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; /* pushed by PUSHA */
    uint32_t int_no, err_code; /* pushed by us in IRQ_x or ISR_x */
    uint32_t eip, cs, eflags, useresp, ss; /* pushed by the cpu automatically */
} __attribute__((packed));

/* Macros to convert ISR and IRQ numbers to
 * their corresponding entry indexes in the IDT,
 * to be used as the first parameter to interrupt_set_handler()
 */
#define ISR(x) (x)
#define IRQ(x) (x + 0x20)

typedef void (*interrupt_handler_t)(struct interrupt_registers *);

void dtinit(uint32_t);
void interrupt_set_handler(uint8_t, interrupt_handler_t);
void set_tss_esp(uint32_t esp0);
void irq_send_eoi(uint32_t num);

#endif /* __INCLUDE_X86_DT_H */

#ifndef __INCLUDE_X86_PIT_H
#define __INCLUDE_X86_PIT_H

#define PIT_BASE_FREQ 1193180

void pit_init(int freq);

#endif /* __INCLUDE_X86_PIT_H */

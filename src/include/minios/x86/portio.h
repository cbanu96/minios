#ifndef __INCLUDE_X86_PORTIO_H
#define __INCLUDE_X86_PORTIO_H

#include <minios/kernel.h>

#define VGA_FRAMEBUFFER_ADDR        VIRT_ADDR(0xB8000)

#define VGA_PORT_LOCATION           VIRT_ADDR(0x463)
                                                /* The word at this location should give us the
                                                 * location of the base I/O video port.
                                                 * Known locations:
                                                 * 0x3B4 - monochrome
                                                 * 0x3D4 - EGA/VGA colored
                                                 */

#define VGA_BASE_PORT               (*(uint16_t *)(VGA_PORT_LOCATION))
#define VGA_BASE_PORT_DATA          (VGA_BASE_PORT + 1)

#define COM1_PORT 0x3F8
#define COM2_PORT 0x2F8
#define COM3_PORT 0x3E8
#define COM4_PORT 0x2E8

#define COM1_IRQ  4
#define COM2_IRQ  3
#define COM3_IRQ  4
#define COM4_IRQ  3

#define LPT1_PORT_LOCATION          VIRT_ADDR(0x0408)
#define LPT2_PORT_LOCATION          VIRT_ADDR(0x040A)
#define LPT3_PORT_LOCATION          VIRT_ADDR(0x040C)

#define PIC_MASTER_CMDPORT          0x0020
#define PIC_MASTER_DATPORT          0x0021
#define PIC_SLAVE_CMDPORT           0x00A0
#define PIC_SLAVE_DATPORT           0x00A1
#define PIC_EOI                     0x20

#define PIT_CH0                     0x40
#define PIT_CH1                     0x41
#define PIT_CH2                     0x42
#define PIT_CMD                     0x43

#define PS2_DATA_PORT               0x60
#define PS2_REGISTER_PORT           0x64


#endif /* __INCLUDE_X86_PORTIO_H */

#ifndef __INCLUDE_KERNEL_H
#define __INCLUDE_KERNEL_H

#define _KERNEL
//#define DEBUG

/* Kernel begin address, end address, etc. */

extern void kernel_phys_start   (void);
extern void kernel_phys_end     (void);
extern void kernel_virt_start   (void);
extern void kernel_virt_end     (void);

#define KERNEL_PHYS_START       ((uint32_t)(&kernel_phys_start))
#define KERNEL_PHYS_END         ((uint32_t)(&kernel_phys_end))
#define KERNEL_VIRT_START       ((uint32_t)(&kernel_virt_start))
#define KERNEL_VIRT_END         ((uint32_t)(&kernel_virt_end))
#define KERNEL_VMA              0xC0000000

#define VIRT_ADDR(phys)             (KERNEL_VMA + phys)
#define PHYS_ADDR(virt)             (virt - KERNEL_VMA)


/* ASM helper functions */
static inline void sti() {
    asm volatile ("sti");
}

static inline void cli() {
    asm volatile ("cli");
}

static inline void hlt() {
    asm volatile ("hlt");
}

/* Necessary includes */
#include <sys/types.h>

/* Random useful macros */
#define UNUSED_VAR(x)  ((void)(x))

#endif /* __INCLUDE_KERNEL_H */

#ifndef __INCLUDE_FS_DEVICE_H
#define __INCLUDE_FS_DEVICE_H

#include <sys/types.h>

struct device {
    size_t d_block_size;

    void * data; /* underlying data structure */

    int (*probe)(struct device *, struct vnode *);
    ssize_t (*pread)(struct device *, char *, size_t, off_t);
    ssize_t (*pwrite)(struct device *, char *, size_t, off_t);
};

int device_create(struct device *, const char *, struct vnode *);

int zero_init(struct vnode *);
int null_init(struct vnode *);
int com_init(struct vnode *);
int tty_init(struct vnode *);

#endif /* __INCLUDE_FS_DEVICE_H */

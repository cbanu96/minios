#ifndef __INCLUDE_FS_VFS_H
#define __INCLUDE_FS_VFS_H

#include <sys/types.h>

#include <ds/list.h>
#include <ds/tree.h>

#include <minios/assert.h>
#include <minios/spinlock.h>

#include <errno_defs.h>

/* stat() */
#define S_IFMT  0170000
#define S_IFDIR 0040000
#define S_IFCHR 0020000
#define S_IFBLK 0060000
#define S_IFREG 0100000
#define S_IFLNK 0120000
#define S_IFIFO 0010000

#define S_ISDIR(m) ((m & S_IFMT) == S_IFDIR)
#define S_ISCHR(m) ((m & S_IFMT) == S_IFCHR)
#define S_ISBLK(m) ((m & S_IFMT) == S_IFBLK)
#define S_ISREG(m) ((m & S_IFMT) == S_IFREG)
#define S_ISLNK(m) ((m & S_IFMT) == S_IFLNK)

/* mask */
#define S_IRUSR 0400
#define S_IWUSR 0200
#define S_IXUSR 0100
#define S_IRWXU 0700

#define S_IRGRP 0040
#define S_IWGRP 0020
#define S_IXGRP 0010
#define S_IRWXG 0070

#define S_IROTH 0004
#define S_IWOTH 0002
#define S_IXOTH 0001
#define S_IRWXO 0007

#define S_ISUID 01000 /* NYI */
#define S_ISGID 02000 /* NYI */
#define S_ISVTX 04000 /* NYI */

/* open() flags */
#define O_RDONLY 0x0000
#define O_WRONLY 0x0001
#define O_RDWR   0x0002
#define O_APPEND 0x0008
#define O_CREAT  0x0200
#define O_TRUNC  0x0400
#define O_EXCL   0x0800

/* seek() */
#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

/* path_*() */
#define PATH_SEPARATOR_CHAR     '/'
#define PATH_SEPARATOR          "/"
#define PATH_PARENTDIR          ".."
#define PATH_CURRENTDIR         "."
#define PATH_MAX_LENGTH         1024
#define FILENAME_MAX_LENGTH     16

struct stat; /* struct required for stat() */

struct vnode; /* virtual node structure */

struct file; /* file abstraction over inodes for processes */

struct vnode_ops; /* vnode operations */
struct file_ops; /* vnode file operations */

struct stat {
    dev_t       st_dev;
    ino_t       st_ino;
    mode_t      st_mode;
    uint32_t    st_nlink;
    uid_t       st_uid;
    gid_t       st_gid;
    dev_t       st_rdev;
    size_t      st_size;
    time_t      st_atime;
    time_t      st_mtime;
    time_t      st_ctime;
    /* st_blksize */
    /* st_blocks */
};

/* A virtual node, won't call it an inode since that is ext*-specific,
 * though it is based on the saem principle.
 * The (v_dev, v_ino) pair uniquely identifies a vnode.
 */

struct vnode {
    dev_t            v_dev;
    ino_t            v_ino;

    mode_t           v_mode; 
    uid_t            v_uid;
    gid_t            v_gid;

    time_t           v_atime;
    time_t           v_mtime;
    time_t           v_ctime;

    uint32_t         v_nlink;
    uint32_t         v_refcount;

    struct vnode_ops * v_ops;
    struct file_ops  * v_fops;

    struct vnode     * v_mount;
    void             * v_fsdata;
};

struct ramfs_internal_entry {
    char e_name[16];
    struct vnode * e_vn;
};

struct ramfs_int_dir_data {
    list_t * d_children;
};

struct ramfs_int_fil_data {
    off_t f_size;
    list_t * f_blocks;
};

struct file {
    struct vnode *   f_vn;
    off_t            f_pos; /* Used for seek() */
    uint32_t         f_oflags; /* open() flags */
};

struct vnode_ops {
    int (*create)(struct vnode *, const char *, mode_t);

    int (*link)(struct vnode *, const char *, struct vnode *);
    int (*symlink)(struct vnode *, const char *, const char *);

    int (*mknod)(struct vnode *, const char *, mode_t, int);
    int (*mkdir)(struct vnode *, const char *, mode_t);
    int (*rename)(struct vnode *, size_t, const char *);

    int (*rmdir)(struct vnode *, size_t);
    int (*unlink)(struct vnode *, size_t);

    int (*truncate)(struct vnode *, off_t);
    off_t (*get_size)(struct vnode *);

    int (*lookup)(struct vnode *, size_t, struct vnode **, char **);
    int (*delete)(struct vnode *);
    int (*free)(struct vnode *);
};

struct file_ops {
    off_t (*seek)(struct file *, off_t, int);
    ssize_t (*read)(struct file *, char *, size_t);
    ssize_t (*write)(struct file *, char *, size_t);

    ssize_t (*pread)(struct file *, char *, size_t, off_t);
    ssize_t (*pwrite)(struct file *, char *, size_t, off_t);

    int (*open)(struct vnode *, struct file *);
    int (*close)(struct vnode *, struct file *);
};

static inline int __follow_mnt(struct vnode ** v) {
    if (!v) return -ENOENT;
    if (!(*v)->v_mount) return -EACCES;

    *v = (*v)->v_mount;
    return 0;
}

static inline int __valid_node(struct vnode * v) {
    if (!v) return -ENOENT;
    if (!v->v_ops) return -EACCES;
    if (!v->v_fops) return -EACCES;

    return 0;
}

static inline int vop_create(struct vnode * v, const char * p, mode_t i) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->create) return -EPERM;

    return v->v_ops->create(v, p, i);
}

static inline int vop_mkdir (struct vnode * v, const char * p, mode_t i) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->mkdir) return -EPERM;

    return v->v_ops->mkdir(v, p, i);
}

static inline int vop_rmdir (struct vnode * v, size_t idx) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->rmdir) return -EPERM;

    return v->v_ops->rmdir(v, idx);
}

static inline int vop_mknod (struct vnode * v, const char * p, mode_t i, int j) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->mknod) return -EPERM;

    return v->v_ops->mknod(v, p, i, j);
}

static inline int vop_rename(struct vnode * v, size_t i, const char * p) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->rename) return -EPERM;

    return v->v_ops->rename(v, i, p);
}

static inline int vop_truncate(struct vnode * v, off_t x) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->truncate) return -EPERM;

    return v->v_ops->truncate(v, x);
}

static inline int vop_link(struct vnode * v, const char * p, struct vnode * w) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->link) return -EPERM;

    return v->v_ops->link(v, p, w);
}

static inline int vop_symlink(struct vnode * v, const char * p, const char * s) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->symlink) return -EPERM;

    return v->v_ops->symlink(v, p, s);
}

static inline int vop_unlink(struct vnode * v, size_t idx) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->unlink) return -EPERM;

    return v->v_ops->unlink(v, idx);
}

static inline int vop_free(struct vnode * v) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->free) return -EPERM;

    return v->v_ops->free(v);
}

static inline ssize_t vop_get_size(struct vnode * v) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->get_size) return -EPERM;

    return v->v_ops->get_size(v);
}

static inline int vop_delete(struct vnode * v) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_ops->delete) return -EPERM;

    return v->v_ops->delete(v);
}

static inline int vfop_open(struct vnode * v, struct file * f, int oflags) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_fops->open) return -EPERM;

    err = v->v_fops->open(v, f);

    f->f_oflags = oflags;

    return err;
}

static inline int vfop_close(struct vnode * v, struct file * f) {
    int err;
    err = __valid_node(v);
    if (err) return err;

    __follow_mnt(&v);

    if (!v->v_fops->close) return -EPERM;

    err = v->v_fops->close(v, f);

    return err;
}

static inline int vop_lookup(struct vnode * v, size_t idx, struct vnode ** w, char ** name) {
    int err;
    err = __valid_node(v);
    if (err) return -1;

    __follow_mnt(&v);

    if (!v->v_ops->lookup) return -EPERM;

    return v->v_ops->lookup(v, idx, w, name);
}

static inline ssize_t vfop_read(struct file * f, char * b, size_t n) {
    int err;
    err = __valid_node(f->f_vn);
    if (err) return err;

    if (!f->f_vn->v_fops->read) return -EPERM;

    return f->f_vn->v_fops->read(f, b, n);
}

static inline ssize_t vfop_write(struct file * f, char * b, size_t n) {
    int err;
    err = __valid_node(f->f_vn);
    if (err) return err;

    if (!f->f_vn->v_fops->write) return -EPERM;

    return f->f_vn->v_fops->write(f, b, n);
}

static inline ssize_t vfop_pread(struct file * f, char * b, size_t n, off_t o) {
    int err;
    err = __valid_node(f->f_vn);
    if (err) return err;

    if (!f->f_vn->v_fops->pread) return -EPERM;

    return f->f_vn->v_fops->pread(f, b, n, o);
}

static inline ssize_t vfop_pwrite(struct file * f, char * b, size_t n, off_t o) {
    int err;
    err = __valid_node(f->f_vn);
    if (err) return err;

    if (!f->f_vn->v_fops->pwrite) return -EPERM;

    return f->f_vn->v_fops->pwrite(f, b, n, o);
}

static inline off_t vfop_seek(struct file * f, off_t off, int whence) {
    int err;
    err = __valid_node(f->f_vn);
    if (err) return err;

    if (!f->f_vn->v_fops->seek) return -EPERM;

    return f->f_vn->v_fops->seek(f, off, whence);
}

/* read/write permission inlines */
static inline bool vf_can_read(struct file * f) {
    if (f->f_oflags & O_WRONLY)
        return false;
    if (f->f_oflags & O_RDWR)
        return true;
    return true;
}

static inline bool vf_can_write(struct file * f) {
    if ((f->f_oflags & O_RDWR) || (f->f_oflags & O_WRONLY))
        return true;
    return false;
}

/* path walking */
int find_file(struct vnode *, struct vnode **, const char *);
struct vnode * walk_path(const char *);
struct vnode * walk_path_parent(const char *, char **);

void dec_nlink(struct vnode *);
void inc_nlink(struct vnode *);

void dec_refcount(struct vnode *);
void inc_refcount(struct vnode *);

/* fs mounting */
int vfs_mount (struct vnode *, struct vnode *, void *,
                int (*)(struct vnode *, void *));

/* filesystems */
int ramfs_init(struct vnode *, void *);
int devfs_init(struct vnode *, void *);
int ramfs_dir_add_child(struct vnode *, struct vnode *, const char *);
int initrd_load();

/* k*fs functions */
struct file * kopen(char *, int);
void kclose(struct file *);
int kmkdir(char *);

/* pipe */
struct vnode * make_pipe(size_t);

#endif /* __INCLUDE_FS_VFS_H */

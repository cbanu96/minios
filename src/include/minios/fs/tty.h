#ifndef __INCLUDE_FS_TTY_H
#define __INCLUDE_FS_TTY_H

#include <sys/types.h>
#include <ds/rbuf.h>
#include <ds/buffer.h>

#define TTY_KBD_CTRL        0x01
#define TTY_KBD_RSHIFT      0x02
#define TTY_KBD_LSHIFT      0x04
#define TTY_KBD_ALT         0x08
#define TTY_KBD_CAPSLOCK    0x10
#define TTY_KBD_NUMLOCK     0x20
#define TTY_KBD_SCROLLLOCK  0x40

#define TTY_BUFFER_SIZE     8192

typedef struct {
    uint8_t kbd_state;

    uint8_t * kbd_layout;
    size_t kbd_layout_size;

    buffer_t * linebuffer;
    rbuf_t   * unread;

    struct process * fg_process;
} tty_t;

void tty_notify(tty_t *);

#endif /* __INCLUDE_MINIOS_FS_TTY_H */

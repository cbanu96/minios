#ifndef __INCLUDE_ELF_H
#define __INCLUDE_ELF_H

/* ELF constants */
#define ELF_NIDENT 16

#define ELFMAG0 0x7F
#define ELFMAG1 'E'
#define ELFMAG2 'L'
#define ELFMAG3 'F'

/* ELF data types */
typedef uint32_t ELF32_Word;    /* 4-bytes */
typedef uint16_t ELF32_Half;    /* 2-bytes */
typedef int32_t  ELF32_Sword;   /* 4-bytes signed */
typedef uint32_t ELF32_Off;     /* 4-bytes offset */
typedef uint32_t ELF32_Addr;    /* 4-bytes address */

/* ELF Header */
typedef struct {
    uint8_t e_ident[ELF_NIDENT];
    ELF32_Half e_type;
    ELF32_Half e_machine;
    ELF32_Word e_version;

    /* Address of the entry point */
    ELF32_Addr e_entry;

    /* Offset to program headers */
    ELF32_Off  e_phoff;

    /* Offset to section headers */
    ELF32_Off  e_shoff;

    ELF32_Word e_flags;

    ELF32_Half e_ehsize;

    /* Program Headers */

    ELF32_Half e_phentsize;
    ELF32_Half e_phnum;

    /* Section Headers */

    ELF32_Half e_shentsize;
    ELF32_Half e_shnum;

    ELF32_Half e_shstrndx;
} ELF32_Header;

/* e_ident */

#define EI_MAG0         0 /* the magic constant indexes */
#define EI_MAG1         1
#define EI_MAG2         2
#define EI_MAG3         3
#define EI_CLASS        4 /* architecture 32/64 bit */
#define EI_DATA         5 /* byte order (little-endian or big-endian) */ 
#define EI_VERSION      6 /* ELF version */
#define EI_OSABI        7 /* OS-specific - should be 0x00 */
#define EI_ABIVERSION   8 /* OS-specific - I don't think anything should be in here */
#define EI_PAD          9 /* takes 7 bytes, unused at the moment */

/* e_type */

#define ET_NONE         0 /* no file type */
#define ET_REL          1 /* relocatable */
#define ET_EXEC         2 /* executable */
#define ET_DYN          3 /* shared object file */
#define ET_CORE         4 /* core file */

/* e_machine */

#define EM_NONE         0
#define EM_386          3

/* e_version */

#define EV_NONE         0
#define EV_CURRENT      1

/* ELF Program Header */
typedef struct {
    ELF32_Word  p_type;
    ELF32_Off   p_offset; 
    ELF32_Addr  p_vaddr;
    ELF32_Addr  p_paddr;
    ELF32_Word  p_filesz;
    ELF32_Word  p_flags;
    ELF32_Word  p_align;
} ELF32_PHeader;

/* p_type */

#define PT_NULL         0 /* To be skipped... */
#define PT_LOAD         1 /* This we load */
#define PT_DYNAMIC      2 /* Dynamic linking */
#define PT_INTERP       3 /* Pathname to an interpreter */
#define PT_NOTE         4 /* Oh, you mean like comments? */
#define PT_SHLIB        5 /* Reserved */
#define PT_PHDR         6 /* This is the program header table itself */

/* ELF Section Header */
typedef struct {
    ELF32_Word sh_name; /* This is not actually a string, but an index into the string table to a null-terminated
                         * string which is the name of this section. Kinda interesting tbh */
    ELF32_Word sh_type;
    ELF32_Word sh_flags;
    ELF32_Addr sh_addr;
    ELF32_Off  sh_offset;
    ELF32_Word sh_size;
    ELF32_Word sh_link;
    ELF32_Word sh_info;
    ELF32_Word sh_addralign;
    ELF32_Word sh_entsize;
} ELF32_SHeader;


/* sh_type */
#define SHT_NULL        0 /* Nothing */
#define SHT_PROGBITS    1 /* CODE */
#define SHT_SYMTAB      2 /* Symbol table */
#define SHT_STRTAB      3 /* String table */

/* 4..7 are not useful yet. May define them later */

#define SHT_NOBITS      8 /* The .bss or something which I must zero */
#define SHT_REL         9 /* Relocation entries */

/* sh_flags - we don't need those yet */

/* Loader function */
int elf_load(struct process *, struct file *, char **, int, char **);

#endif /* __INCLUDE_ELF_H */

#ifndef __INCLUDE_LOG_H
#define __INCLUDE_LOG_H

#include <minios/fs/vfs.h>

#define LOGLEVEL_DEBUG 0      /* Everything GREATER than this will be logged */
#define LOGLEVEL_QUIET 1

#define KERROR "\x04"
#define KWARN  "\x03"
#define KINFO  "\x02"
#define KDEBUG "\x01"


void klogoutput(struct file *);
void klogearly(bool enable);
void kloglevel(uint8_t level);
void klog(const char* fmt, ...);

#endif /* __INCLUDE_LOG_H */

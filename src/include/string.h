#ifndef __INCLUDE_STRING_H
#define __INCLUDE_STRING_H

#include <sys/types.h>

int      memcmp(const void *, const void *, size_t);
void    *memcpy(void *, const void *, size_t);
void    *memmove(void *, const void *, size_t);
void    *memset(void *, int, size_t);

size_t   strlen(const char *);
char    *strncat(char *, const char *, size_t);
int      strncmp(const char *, const char *, size_t);
char    *strncpy(char *, const char *, size_t);
char    *strcpy(char *, const char *);
int      strcmp(const char *, const char *);

#endif /* __INCLUDE_STRING_H */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

int main(int argc, char * argv[], char * envp[]) {
    /* stdin/stdout/stderr */
    open("/dev/ttyS0", O_RDWR);
    dup(0);
    dup(0);

    int pid = fork();

    if (pid == 0) {
        char * const newargv[] = { "/bin/proctest", NULL };
        char * const newenvp[] = { NULL };

        execve("/bin/proctest", newargv, newenvp);
    }

    if (pid > 0) {
        printf("Init: my id is %d\r\n", getpid());
        printf("Init: testing reading from serial port:\r\n");
        char buf[10];
        int num = 0;
        while (num < 10) {
            num += read(0, buf + num, 10 - num);
        }
        printf("\r\nInit: You wrote %s\r\n", buf);
    }

    return 0;
}

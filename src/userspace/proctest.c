#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char * argv[], char * envp[]) {
    int pid = fork();

    if (pid == 0) {
        printf("Child's child: my id is %d, my parent's pid is %d\r\n", getpid(), getppid());
    } else {
        printf("Child: my id is %d, my child's pid is %d, my parent's pid is %d\r\n", getpid(), pid, getppid());

        printf("Child: becoming /bin/fstest\r\n");

        char * const newargv[] = { "/bin/fstest", NULL };
        char * const newenvp[] = { NULL };

        execve("/bin/fstest", newargv, newenvp);
    }

    return 0;
}


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char * argv[], char * envp[]) {
    printf("Child: Now I'm /bin/fstest. Redirecting output to /dev/tty\r\n");

    close(0);
    close(1);
    close(2);

    open("/dev/tty", O_RDWR);
    dup(0);
    dup(0);

    char buf[256];

    printf("\n\n");

    printf("Child: Testing devfs...\n");
    int fd_zero = open("/dev/zero", O_RDWR);
    int fd_null = open("/dev/null", O_RDWR);
    printf("Child: Reading from /dev/zero (fd=%d) 4 bytes: %d\n", fd_zero, read(fd_zero, buf, 4));
    printf("Child: Reading from /dev/null (fd=%d) 4 bytes: %d\n", fd_null, read(fd_null, buf, 4));
    close(fd_zero);
    close(fd_null);

    printf("Child: Testing ramfs...\n");
    int fd_dummy = open("/etc/test", O_RDONLY), n;
    printf("Child: Reading from dummy file /etc/test (fd = %d) max. 256 bytes: %d\n", fd_dummy, n = read(fd_dummy, buf, 256));
    printf("Child: Dummy file contents: %s\n", buf);
    close(fd_dummy);

    printf("Child: Opening /etc/notfound, should fail: %d\n", open("/etc/notfound", O_RDONLY));

    printf("Child: Creating new file /etc/created: %d\n", fd_dummy = open("/etc/created", O_RDWR | O_CREAT));
    printf("Child: Writing to created file data from dummy: %d\n", write(fd_dummy, buf, n));

    printf("Child: terminal is in echo line buffered mode, type to test keyboard driver.\n");


    return 0;
}
